#ifndef cSplitTag_H
#define cSplitTag_H

#include <QDialog>
#include <QCheckBox>
#include <QList>
#include <QSpinBox>
#include <QComboBox>
#include <QString>
#include <QLineEdit>
#include <QTextEdit>
#include "stdint.h"
#include <QCheckBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QComboBox>
#include <QWidget>

typedef enum{
    splitBoolLineEdit,
    splitBoolSpinBox,
    splitBoolComboBox
}mlsBoolWidgetSplitTag_t;

typedef enum{
    ctTerType     = 0x00,
    ctKerID       = 0x01,
    ctStat        = 0x02,
    ctMerType     = 0x03
}mls_comboType_t;

class cSplitTag : public QDialog
{
public:
    cSplitTag();


    QString text;


    void initializeList(QList<QWidget *> listWid, QList<QCheckBox *> listCheck,
                        QStringList stringComboTerType, QStringList stringComboKerId,
                        QStringList stringStat, mlsBoolWidgetSplitTag_t widgetType[]);

    void updateComboType(mls_comboType_t comboType);

    void UncheckTag();

    void Updatetext(int index, mlsBoolWidgetSplitTag_t Widget, uint8_t *Buffertam, uint8_t Pos, uint8_t Length);

    void UncheckCheckbox(int index, mlsBoolWidgetSplitTag_t Widget);

    void UpdateOtherTLV(uint8_t *Buffertam, uint8_t Pos, uint8_t Sobyte, uint8_t Length);


private:  

    bool m_stateCheckboxLineEdit[25] ;
    bool m_stateCheckboxCombo[5]     ;
    bool m_stateCheckboxSpin[5]      ;

    bool m_stateCheckBox[27]         ;

    QList<QWidget*>              m_listWid;
    QList<QCheckBox*>            m_listCheck;

    QStringList                  m_stringComboTerType;
    QStringList                  m_stringComboKerID;
    QStringList                  m_stringComboState;

    mls_comboType_t              m_comboType;
    mlsBoolWidgetSplitTag_t     m_widgetType[27];


};

#endif // cSplitTag_H
