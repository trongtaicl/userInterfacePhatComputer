#include "cPaypassEntryPoint.h"
#include "ui_cPaypassEntryPoint.h"
#include "cSplitTag.h"

cPaypassEntryPoint::cPaypassEntryPoint(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cPaypassEntryPoint)
{
    ui->setupUi(this);
    this->setWindowTitle("Paypass Entrypoint");
    m_comboTerType << "00" << "11" << "12" << "13" << "14" << "15" << "16" << "21" << "22" << "23"
                 << "24" << "25" <<"26" << "34" << "35" << "36";
   // all these kernelID,
    m_comboKerID << "KERNEL01" << "MasterCard" << "Visa" << "American Express" << "JCB" << "JCB" << "Discover"
                << "C.U.P" << "Interact FMW 3.1";

    for (int i =0x09; i <= 0x40 ;i++){
        m_comboKerID << "KERNEL" + QString::number(i, 16).toUpper();
    }
    m_comboKerID << "Interac FMW 3.2" << "Eftpos FMW 3.2";
    for (int i = 0x43; i<= 0xFF; i++)
    {
        m_comboKerID << "KERNEL" + QString::number(i, 16).toUpper();
    }

    ui->spinBox_MaxNumTorn->setMaximum(255);

    for (int i =0; i< m_comboTerType.count(); i++){
        ui->comboBoxTerType->addItem(m_comboTerType.at(i));
    }

    for(int i =0; i< m_comboKerID.count(); i++){
        ui->comboBox_KerID->addItem(m_comboKerID.at(i));
    }

    InitializePaypass();
    InitializeToolButton();

    AddTer       = new cAdditionalTerCapabilities(this);
    TACWin       = new cTerminalActionCode(this);
    winCardData  = new cCardDataInputCapabilities(this);
    winSecCap    = new cSecurityCapabilities(this);
    winCVMCap    = new cCvmCapabilities(this);
    winKerConfig = new cKernelConfig(this);

    sigMappLineEdit  = new QSignalMapper();

    textFinal = "";
    m_lineEditChoseTAC = 0;
    m_lineEditChoseCVM = 0;


    for (int i =0; i< m_listCheckBoxLine.count(); i++){
        sigMappLineEdit->setMapping(m_listCheckBoxLine[i], i);
       /* connect every checkbox click signal with map() */
        connect(m_listCheckBoxLine[i], SIGNAL(clicked()), sigMappLineEdit, SLOT(map()));
    }
    connect(sigMappLineEdit, SIGNAL(mapped(int)), this, SLOT(checkbox_update_lineEdit(int)));

    connect(this, SIGNAL(ToolButtonCardData_clicked(QString)), winCardData, SLOT(cd_change_text_edit(QString)));

    connect(winCardData, SIGNAL(ButtonOk_CardData_clicked(QString)), this, SLOT(UpdateCardData(QString)));

    connect(this, SIGNAL(ToolButtonSecCap_clicked(QString)), winSecCap, SLOT(scChangeTextEdit(QString)));

    connect(winSecCap, SIGNAL(ButtonOK_SecCap_clicked(QString)), this, SLOT(UpdateSecCap(QString)));

    connect(this, SIGNAL(ToolButtonnCVMCap_clicked(QString)), winCVMCap, SLOT(cvm_change_text_edit(QString)));

    connect(winCVMCap, SIGNAL(ButtonOk_CVMCap_clicked(QString)), this, SLOT(UpdateCVMCap(QString)));

    connect(this, SIGNAL(ToolButtonKerConfig_clicked(QString)), winKerConfig, SLOT(kcChangeTextEdit(QString)));

    connect(winKerConfig, SIGNAL(ButtonOK_KerConfig_clicked(QString)), this, SLOT(UpdateKerConfig(QString)));

    connect(this, SIGNAL(ToolButtonAddTerCap_clicked(QString,mlsWinAdd_t)), AddTer, SLOT(change_text_edit(QString,mlsWinAdd_t)));

    connect(AddTer, SIGNAL(ButtonOk_PayPass_Clicked(QString)), this, SLOT(UpdateAddTerCap(QString)));

    connect(this, SIGNAL(ToolButtonTACDefault_clicked(QString,mlsWindowDef_t)), TACWin, SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    connect(this, SIGNAL(ToolButtonTACDenial_clicked(QString,mlsWindowDef_t)), TACWin, SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    connect(this, SIGNAL(ToolButtonTACOnline_clicked(QString,mlsWindowDef_t)), TACWin, SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    connect(TACWin, SIGNAL(ButtonOK_PayPass_Clicked(QString)), this, SLOT(UpdateTACData(QString)));

}

cPaypassEntryPoint::~cPaypassEntryPoint()
{
    delete ui;
}

void cPaypassEntryPoint::InitializePaypass()
{
    int frameWidth;

    m_listWidget << ui->lineEdit_DefUDOL << ui->comboBox_KerID << ui->lineEdit_AppVerNumMas
                 << ui->lineEdit_CVMCap << ui->lineEdit_CVMCapNo << ui->lineEdit_MesHoldTime
                 << ui->lineEdit_RCTLNo << ui->lineEdit_RCTL << ui->lineEdit_RCVMRL
                 << ui->lineEdit_TerCounCode << ui->lineEdit_MobSupInd
                 << ui->lineEdit_AddTerCap << ui->lineEdit_AppVerNumEMV
                 << ui->lineEdit_CardDataIn << ui->lineEdit_CVMCapCVMReqEMV
                 << ui->lineEdit_CVMCapNoEMV << ui->lineEdit_KerCon
                 << ui->lineEdit_MaxLifeTorn << ui->spinBox_MaxNumTorn
                 << ui->lineEdit_RCFL << ui->lineEdit_SecCap
                 << ui->lineEdit_TACDef << ui->lineEdit_TACDenial
                 << ui->lineEdit_TACOnl << ui->comboBoxTerType
                 << ui->lineEdit_BalReadBefAC << ui->lineEdit_BalReadAfterAC;


    m_listCheckboxWid << ui->checkBox_DefUDOL << ui->checkBox_KerID
                      << ui->checkBox_AppVerNumMas << ui->checkBox_CVMCap
                      << ui->checkBox_CVMCapNo << ui->checkBox_MesHoldTime
                      << ui->checkBox_RCTLNo << ui->checkBox_RCTL << ui->checkBox_RCVMReq
                      << ui->checkBox_TerCounCode << ui->checkBox_MobSupInd
                      << ui->checkBox_AddTerCap << ui->checkBox_AppVerNumEMV
                      << ui->checkBox_CardDataInpEMV << ui->checkBox_CVMCapEMV
                      << ui->checkBox_CVMCapNoEMV << ui->checkBox_KerConf
                      << ui->checkBox_MaxLifeTorn << ui->checkBox_MaxNumTorn
                      << ui->checkBox_RCFL << ui->checkBox_SecCap
                      << ui->checkBox_TACDef << ui->checkBox_TACDen
                      << ui->checkBox_TACOnl << ui->checkBox_TerType
                      << ui->checkBox_BalReadBef << ui->checkBox_BalReadAfter;

    m_widgetType[0] = splitBoolLineEdit; m_widgetType[1] = splitBoolComboBox;
   m_widgetType[2] = splitBoolLineEdit; m_widgetType[3] = splitBoolLineEdit;
   m_widgetType[4] = splitBoolLineEdit; m_widgetType[5] = splitBoolLineEdit;
   m_widgetType[6] = splitBoolLineEdit; m_widgetType[7] = splitBoolLineEdit;
   m_widgetType[8] = splitBoolLineEdit; m_widgetType[9] = splitBoolLineEdit;
   m_widgetType[10] = splitBoolLineEdit; m_widgetType[11] = splitBoolLineEdit;
   m_widgetType[12] = splitBoolLineEdit; m_widgetType[13] = splitBoolLineEdit;
   m_widgetType[14] = splitBoolLineEdit; m_widgetType[15] = splitBoolLineEdit;
   m_widgetType[16] = splitBoolLineEdit; m_widgetType[17] = splitBoolLineEdit;
   m_widgetType[18] = splitBoolSpinBox; m_widgetType[19] = splitBoolLineEdit;
   m_widgetType[20] = splitBoolLineEdit; m_widgetType[21] = splitBoolLineEdit;
   m_widgetType[22] = splitBoolLineEdit; m_widgetType[23] = splitBoolLineEdit;
   m_widgetType[24] = splitBoolComboBox; m_widgetType[25] = splitBoolLineEdit;
   m_widgetType[26] = splitBoolLineEdit;

   m_listCheckBoxLine << ui->checkBox_TerCounCode
                      << ui->checkBox_AddTerCap << ui->checkBox_MobSupInd
                      << ui->checkBox_KerConf << ui->checkBox_MesHoldTime
                      << ui->checkBox_AppVerNumMas << ui->checkBox_DefUDOL
                      << ui->checkBox_CVMCap << ui->checkBox_CVMCapNo
                      << ui->checkBox_AppVerNumEMV << ui->checkBox_SecCap
                      << ui->checkBox_CardDataInpEMV << ui->checkBox_CVMCapEMV
                      << ui->checkBox_CVMCapNoEMV << ui->checkBox_MaxLifeTorn
                      << ui->checkBox_TACDef << ui->checkBox_TACDen << ui->checkBox_TACOnl
                      << ui->checkBox_BalReadBef << ui->checkBox_BalReadAfter
                      << ui->checkBox_RCFL << ui->checkBox_RCTLNo << ui->checkBox_RCTL
                      << ui->checkBox_RCVMReq;

   m_listLineEditCheck << ui->lineEdit_TerCounCode << ui->lineEdit_AddTerCap << ui->lineEdit_MobSupInd
                       << ui->lineEdit_KerCon << ui->lineEdit_MesHoldTime << ui->lineEdit_AppVerNumMas
                       << ui->lineEdit_DefUDOL << ui->lineEdit_CVMCap << ui->lineEdit_CVMCapNo
                       << ui->lineEdit_AppVerNumEMV << ui->lineEdit_SecCap
                       << ui->lineEdit_CardDataIn << ui->lineEdit_CVMCapCVMReqEMV
                       << ui->lineEdit_CVMCapNoEMV << ui->lineEdit_MaxLifeTorn
                       << ui->lineEdit_TACDef << ui->lineEdit_TACDenial << ui->lineEdit_TACOnl
                       << ui->lineEdit_BalReadBefAC << ui->lineEdit_BalReadAfterAC << ui->lineEdit_RCFL
                       << ui->lineEdit_RCTLNo << ui->lineEdit_RCTL << ui->lineEdit_RCVMRL;

    for (int i =0; i< m_listLineEditCheck.count(); i++){
        m_listCheckBoxLine.at(i)->setChecked(false);
        m_listCheckBoxLine.at(i)->setStyleSheet("QCheckBox{color:rgb(200, 200, 200)}");
        m_listLineEditCheck.at(i)->setEnabled(false);
        m_listLineEditCheck.at(i)->setStyleSheet("border: 1px solid rgb(160, 160, 160); border-radius: 3px");
    }

    for (int i = 18; i<m_listLineEditCheck.count(); i++){
        m_listLineEditCheck.at(i)->setInputMask("hhhhhhhhhhhh");
        m_listLineEditCheck.at(i)->setText("000000000000");
    }

    for (int i =15; i<18; i++){
        m_listLineEditCheck.at(i)->setInputMask("hhhhhhhhhh");
        m_listLineEditCheck.at(i)->setText("0000000000");
    }

    ui->lineEdit_MaxLifeTorn->setInputMask("hhhh");
    ui->lineEdit_MaxLifeTorn->setText("0000");
    ui->lineEdit_MaxLifeTorn->setToolTip("Tag DF1C");

    for (int i = 10; i< 14; i++){
       m_listLineEditCheck.at(i)->setInputMask("hh");
       m_listLineEditCheck.at(i)->setText("00");
    }
    ui->checkBox_SecCap->setToolTip("Tag DF03");
    ui->lineEdit_SecCap->setToolTip("Tag DF03");

    ui->checkBox_CardDataInpEMV->setToolTip("Tag DF17");
    ui->lineEdit_CardDataIn->setToolTip("Tag DF17");

    ui->checkBox_CVMCapEMV->setToolTip("Tag DF18");
    ui->lineEdit_CVMCapCVMReqEMV->setToolTip("Tag DF18");

    ui->checkBox_CVMCapNoEMV->setToolTip("Tag DF19");
    ui->lineEdit_CVMCapNoEMV->setToolTip("Tag DF19");

    ui->checkBox_MaxLifeTorn->setToolTip("Tag DF1C in second");
    ui->lineEdit_MaxLifeTorn->setToolTip("Tag DF1C in second");

    ui->checkBox_TACDef->setToolTip("Tag DF20");
    ui->lineEdit_TACDef->setToolTip("Tag DF20");
    frameWidth = ui->lineEdit_TACDef->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_TACDef->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-left: %1px; } ").arg(ui->toolButtonTACDefault->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_TACDef->setAlignment(Qt::AlignLeft);

    ui->checkBox_TACDen->setToolTip("Tag DF21");
    ui->lineEdit_TACDenial->setToolTip("Tag DF21");
    frameWidth = ui->lineEdit_TACDenial->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_TACDenial->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-left: %1px; } ").arg(ui->toolButtonTACDefault->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_TACDenial->setAlignment(Qt::AlignLeft);

    ui->checkBox_TACOnl->setToolTip("Tag DF22");
    ui->lineEdit_TACOnl->setToolTip("Tag DF22");
    frameWidth = ui->lineEdit_TACOnl->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_TACOnl->setStyleSheet(QString("QLineEdit {border: 1px solid black; padding-left: %1px; } ").arg(ui->toolButtonTACDefault->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_TACOnl->setAlignment(Qt::AlignLeft);

    ui->checkBox_BalReadBef->setToolTip("Tag DF04");
    ui->lineEdit_BalReadBefAC->setToolTip("Tag DF04");

    ui->checkBox_BalReadAfter->setToolTip("Tag DF05");
    ui->lineEdit_BalReadAfterAC->setToolTip("Tag DF05");
    for (int i = 20; i< 24; i++){
        m_listLineEditCheck.at(i)->setToolTip("Tag DF" + QString::number(i+3));
        m_listCheckBoxLine.at(i)->setToolTip("Tag DF" + QString::number(i+3));
    }

    ui->lineEdit_AppVerNumEMV->setInputMask("hhhh");
    ui->lineEdit_AppVerNumEMV->setText("0000");
    ui->lineEdit_AppVerNumEMV->setToolTip("Tag 9F09");
    ui->checkBox_AppVerNumEMV->setToolTip("Tag 9F09");

    ui->lineEdit_CVMCapNo->setInputMask("hh");
    ui->lineEdit_CVMCapNo->setText("00");
    ui->lineEdit_CVMCapNo->setToolTip("Tag DF2C");
    ui->checkBox_CVMCapNo->setToolTip("Tag DF2C");

    ui->lineEdit_CVMCap->setInputMask("hh");
    ui->lineEdit_CVMCap->setText("00");
    ui->lineEdit_CVMCap->setToolTip("Tag DF1E");
    ui->checkBox_CVMCap->setToolTip("Tag DF1E");

    QRegExp rx("[A-Fa-f1-9]{0,32}");
    QValidator *validator = new QRegExpValidator(rx, this);
    ui->lineEdit_DefUDOL->setValidator(validator);
    ui->lineEdit_DefUDOL->setToolTip("Tag DF1A");

    ui->checkBox_DefUDOL->setToolTip("Tag DF1A");

    ui->lineEdit_AppVerNumMas->setInputMask("hhhh");
    ui->lineEdit_AppVerNumMas->setText("0000");
    ui->lineEdit_AppVerNumMas->setToolTip("Tag 9F6D");
    ui->checkBox_AppVerNumMas->setToolTip("Tag 9F6D");

    ui->lineEdit_MesHoldTime->setInputMask("hhhhhh");
    ui->lineEdit_MesHoldTime->setText("000000");
    ui->lineEdit_MesHoldTime->setToolTip("Tag DF2D");
    ui->checkBox_MesHoldTime->setToolTip("Tag DF2D");

    ui->lineEdit_KerCon->setInputMask("hh");
    ui->lineEdit_KerCon->setText("00");
    ui->lineEdit_KerCon->setToolTip("Tag DF1B");
    ui->checkBox_KerConf->setToolTip("Tag DF1B");

    ui->lineEdit_MobSupInd->setInputMask("hh");
    ui->lineEdit_MobSupInd->setText("00");
    ui->lineEdit_MobSupInd->setToolTip("Tag 9F7E");
    ui->checkBox_MobSupInd->setToolTip("Tag 9F7E");

    //line edit terminal capabilitie
    frameWidth = ui->lineEdit_AddTerCap->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEdit_AddTerCap->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:1px; padding-left: %1px; } ").arg(ui->toolButtonAddTerCap->sizeHint().width() + frameWidth + 3));
    ui->lineEdit_AddTerCap->setAlignment(Qt::AlignLeft);
    ui->lineEdit_AddTerCap->setInputMask("hhhhhhhhhh");
    ui->lineEdit_AddTerCap->setText("00");
    ui->lineEdit_AddTerCap->setToolTip("Tag 9F40");
    ui->checkBox_AddTerCap->setToolTip("Tag 9F40");

    ui->lineEdit_TerCounCode->setInputMask("hhhh");
    ui->lineEdit_TerCounCode->setText("0000");
    ui->lineEdit_TerCounCode->setToolTip("Tag 9F1A");
    ui->checkBox_TerCounCode->setToolTip("Tag 9F1A");

    ui->checkBox_TerType->setChecked(false);
    ui->checkBox_TerType->setToolTip("Tag 9F35");
    ui->comboBoxTerType->setToolTip("Tag 9F35");

    ui->checkBox_KerID->setChecked(false);
    ui->checkBox_KerID->setToolTip("Tag DF0C");
    ui->comboBox_KerID->setToolTip("Tag DF0C");

    ui->checkBox_MaxNumTorn->setChecked(false);
    ui->checkBox_MaxNumTorn->setToolTip("Tag DF1D");
    ui->spinBox_MaxNumTorn->setToolTip("Tag DF1D");


    m_TLV.initializeList(m_listWidget, m_listCheckboxWid, m_comboTerType,
                         m_comboKerID, QStringList(),m_widgetType);
}

void cPaypassEntryPoint::InitializeToolButton()
{
   // button additional
   ui->toolButtonAddTerCap->setStyleSheet("QToolButton{border:none; padding: 0px; border-radius:1px}");
   ui->toolButtonAddTerCap->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonAddTerCap->setIconSize(QSize(ui->toolButtonAddTerCap->width(), ui->toolButtonAddTerCap->height()));
   ui->toolButtonAddTerCap->setToolTip("Tag 9F40");

   ui->toolButtonTACDefault->setStyleSheet("QToolButton{border:none; padding: 0px; border-radius:1px}");
   ui->toolButtonTACDefault->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonTACDefault->setIconSize(QSize(ui->toolButtonTACDefault->width(), ui->toolButtonTACDefault->height()));
   ui->toolButtonAddTerCap->setToolTip("Tag DF20");

   ui->toolButtonTACDenial->setStyleSheet("QToolButton{border:none; padding: 0px; border-radius:1px}");
   ui->toolButtonTACDenial->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonTACDenial->setIconSize(QSize(ui->toolButtonTACDenial->width(), ui->toolButtonTACDenial->height()));
   ui->toolButtonTACDenial->setToolTip("Tag DF21");

   ui->toolButtonTACOnline->setStyleSheet("QToolButton{border:none; padding: 0px; border-radius:1px}");
   ui->toolButtonTACOnline->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonTACOnline->setIconSize(QSize(ui->toolButtonTACOnline->width(), ui->toolButtonTACOnline->height()));
   ui->toolButtonTACOnline->setToolTip("Tag DF22");

   ui->toolButtonKerCon->setStyleSheet("QToolButton{border:none; padding: 0px; border-radius:1px}");
   ui->toolButtonKerCon->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonKerCon->setIconSize(QSize(ui->toolButtonKerCon->width(), ui->toolButtonKerCon->height()));
   ui->toolButtonKerCon->setToolTip("Tag DF1B");

   ui->toolButtonSecCap->setStyleSheet("QToolButton{border:none; padding: 0px; border-radius:1px}");
   ui->toolButtonSecCap->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonSecCap->setIconSize(QSize(ui->toolButtonSecCap->width(), ui->toolButtonSecCap->height()));
   ui->toolButtonSecCap->setToolTip("Tag DF03");

   ui->toolButtonCardData->setStyleSheet("QToolButton{border:none; padding: 0px;border-radius:1px}");
   ui->toolButtonCardData->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonCardData->setIconSize(QSize(ui->toolButtonCardData->width(), ui->toolButtonCardData->height()));
   ui->toolButtonCardData->setToolTip("Tag DF17");

   ui->toolButtonCVMCap->setStyleSheet("QToolButton{border:none; padding: 0px;border-radius:1px}");
   ui->toolButtonCVMCap->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonCVMCap->setIconSize(QSize(ui->toolButtonCVMCap->width(), ui->toolButtonCVMCap->height()));
   ui->toolButtonCVMCap->setToolTip("Tag DF18");

   ui->toolButtonCVMCapNo->setStyleSheet("QToolButton{border:none; padding: 0px; border-radius:1px}");
   ui->toolButtonCVMCapNo->setIcon(QIcon(":/image_open_save/image/pencil.png"));
   ui->toolButtonCVMCapNo->setIconSize(QSize(ui->toolButtonCVMCapNo->width(), ui->toolButtonCVMCapNo->height()));
   ui->toolButtonCVMCapNo->setToolTip("Tag DF19");
}

/**
 * @brief cPaypassEntryPoint::CheckTagAvai
 * Get every TLV to update to checkbox or other TLV text edit
 * @param Buffertam
 * @param LengTotal
 */
void cPaypassEntryPoint::CheckTagAvai(uint8_t *Buffertam, uint16_t LengTotal)
{
    int Length =0, Sobyte =0;

    if (LengTotal == 0){
        return ;
    }
    for (int i =0; i < LengTotal; i = i+ Sobyte +1 + Length){

        if ((Buffertam[i] & 0x1F) == 0x1F){
            if ((Buffertam[i+1] & 0x80) == 0x80){
                Sobyte = 3;
            }

            else {
                Sobyte = 2;
            }
        }

        else {
            Sobyte = 1;
        }

        if ((Buffertam[i + Sobyte ]== 0x81)){
            Sobyte += 1;
        }

        Length = Buffertam[i + Sobyte];
        UpdateData(i, Sobyte, Buffertam, Length);
    }
}

/**
 * @brief cPaypassEntryPoint::UpdateData
 * update data, if a tag is not in the string lineEdit, the checkbox of this tag will be
 * unchecked, line Edit of this tag will be set to default.
 * @param Index
 * @param Sobyte
 * @param Buffertam
 * @param Length
 */

void cPaypassEntryPoint::UpdateData(uint8_t Index, uint8_t Sobyte, uint8_t *Buffertam, uint8_t Length)
{
    QString textLengthForUDOL;
    if (Sobyte == 2){
        switch (Buffertam[Index]){
        case 0x9F:
            switch (Buffertam[Index+1]){
            case 0x1A:
                if (Length ==2){
                  UpdateTextFinal(Length, Buffertam, "9F1A02", Index);
                  m_TLV.Updatetext(TerCounCode, splitBoolLineEdit, Buffertam, Index+Sobyte+1, Length);
                }
                else{
                    m_TLV.UncheckCheckbox(TerCounCode, splitBoolLineEdit);

                }
            break;

            case 0x35:
                if (Length ==1){
                  m_TLV.updateComboType(ctTerType);
                  UpdateTextFinal(Length, Buffertam, "9F3501", Index);
                  m_TLV.Updatetext(TerType , splitBoolComboBox, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_TLV.UncheckCheckbox(TerType, splitBoolComboBox);
                }
            break;

            case 0x40:
                if (Length ==5){
                    UpdateTextFinal(Length, Buffertam, "9F4005", Index);
                  m_TLV.Updatetext(AddTerCap, splitBoolLineEdit, Buffertam, Index+Sobyte+1, Length);
                }
                else{
                    m_TLV.UncheckCheckbox(AddTerCap, splitBoolLineEdit);
                }
            break;

            case 0x7E:
                if (Length ==1){
                  UpdateTextFinal(Length, Buffertam, "9F7E01", Index);
                  m_TLV.Updatetext(MobSupInd, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else if (Length >1){
                    m_TLV.UncheckCheckbox(MobSupInd, splitBoolLineEdit);
                }
                else {
                    m_listCheckboxWid[MobSupInd]->setCheckState(Qt::PartiallyChecked);
                    m_listCheckboxWid[MobSupInd]->setStyleSheet("QCheckBox{color: black}");
                }
            break;

            case 0x6D:
                if (Length ==2){
                    UpdateTextFinal(Length, Buffertam, "9F6D02", Index);
                    m_TLV.Updatetext(AppVerNumMastripe, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_TLV.UncheckCheckbox(AppVerNumMastripe, splitBoolLineEdit);
                }
            break;

            case 0x09:
                if (Length ==2){
                    UpdateTextFinal(Length, Buffertam, "9F0902", Index);
                   m_TLV.Updatetext(AppVerNumEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{

                    m_TLV.UncheckCheckbox(AppVerNumEMV, splitBoolLineEdit);
                }
            break;

            default:
                m_TLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
             break;
            }
        break;

        case 0xDF:
            switch(Buffertam[Index+1]){

            case 0x0C:
                 if (Length ==1){
                   UpdateTextFinal(Length, Buffertam, "DF0C01", Index);
                   m_TLV.updateComboType(ctKerID);
                   m_TLV.Updatetext(KerId, splitBoolComboBox, Buffertam, Index + Sobyte +1, Length);
                 }

                 else{
                     m_TLV.UncheckCheckbox(KerId, splitBoolComboBox);
                 }
            break;

            case 0x1B:
                if (Length ==1){
                    UpdateTextFinal(Length, Buffertam, "DF1B01", Index);
                    m_TLV.Updatetext(KerCon, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_TLV.UncheckCheckbox(KerCon, splitBoolLineEdit);
                }
            break;

            case 0x2D:
                if (Length ==3){
                   UpdateTextFinal(Length, Buffertam, "DF2D03", Index);
                   m_TLV.Updatetext(MesHoldTime, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                   m_TLV.UncheckCheckbox(MesHoldTime, splitBoolLineEdit);
                }
            break;

             case 0x1A:
                     if (Length < 0x0F)
                     {
                         textLengthForUDOL = "0" + QString::number(Length, 16).toUpper();
                     }
                     else
                     {
                         textLengthForUDOL = QString::number(Length, 16).toUpper();
                     }

                     UpdateTextFinal(Length, Buffertam, "DF1A" + textLengthForUDOL, Index);
                     m_TLV.Updatetext(DefUDOLMastripe, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
             break;

             case 0x1E:
                if (Length ==1){
                    UpdateTextFinal(Length, Buffertam, "DF1E01", Index);
                    m_TLV.Updatetext(CVMCapRequiredMastripe, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                }
                else{
                    m_TLV.UncheckCheckbox(CVMCapRequiredMastripe, splitBoolLineEdit);
                }
                break;

                case 0x2C:
                   if (Length ==1){
                     UpdateTextFinal(Length, Buffertam, "DF2C01", Index);
                     m_TLV.Updatetext(CVMCapNoRequiredMastripe, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(CVMCapNoRequiredMastripe, splitBoolLineEdit);
                   }
                break;

                case 0x03:
                   if (Length == 1){
                     UpdateTextFinal(Length, Buffertam, "DF0301", Index);
                     m_TLV.Updatetext(SecCapEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else {
                       m_TLV.UncheckCheckbox(SecCapEMV, splitBoolLineEdit);
                   }
                break;

                case 0x17:
                   if (Length ==1){
                      UpdateTextFinal(Length, Buffertam, "DF1701", Index);
                      m_TLV.Updatetext(CardDataEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(CardDataEMV, splitBoolLineEdit);
                   }
                break;

                case 0x18:
                   if (Length ==1){
                      UpdateTextFinal(Length, Buffertam, "DF1801", Index);
                      m_TLV.Updatetext(CVMCapRequiredEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(CVMCapRequiredEMV, splitBoolLineEdit);
                   }
                break;

                case 0x19:
                    if (Length ==1){
                      UpdateTextFinal(Length, Buffertam, "DF1901", Index);
                      m_TLV.Updatetext(CVMCapNoRequiredEMVp, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                    }
                    else{
                        m_TLV.UncheckCheckbox(CVMCapNoRequiredEMVp, splitBoolLineEdit);
                    }
                break;

                case 0x1C:
                   if (Length ==2){
                      UpdateTextFinal(Length, Buffertam, "DF1C02", Index);
                      m_TLV.Updatetext(MaxLifeTimeEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(MaxLifeTimeEMV, splitBoolLineEdit);
                   }
                break;

                case 0x1D:
                   if (Length ==1){
                      UpdateTextFinal(Length, Buffertam, "DF1D01", Index);
                      m_TLV.Updatetext(MaxNumTornTrans, splitBoolSpinBox, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(MaxNumTornTrans, splitBoolSpinBox);
                   }
                break;

                case 0x20:
                   if (Length ==5){
                     UpdateTextFinal(Length, Buffertam, "DF2005", Index);
                     m_TLV.Updatetext(TACDefEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(TACDefEMV, splitBoolLineEdit);
                   }
                break;

                case 0x21:
                   if (Length == 5){
                     UpdateTextFinal(Length, Buffertam, "DF2105", Index);
                     m_TLV.Updatetext(TACDenialEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(TACDenialEMV, splitBoolLineEdit);
                   }
                break;

                case 0x22:
                   if (Length ==5){
                     UpdateTextFinal(Length, Buffertam, "DF2205", Index);
                     m_TLV.Updatetext(TACOnlEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(TACOnlEMV, splitBoolLineEdit);
                   }
                break;

                case 0x04:
                   if (Length ==6){
                     UpdateTextFinal(Length, Buffertam, "DF0406", Index);
                     m_TLV.Updatetext(BalReadBefACEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(BalReadBefACEMV, splitBoolLineEdit);
                   }
                break;

                case 0x05:
                   if (Length ==6){
                     UpdateTextFinal(Length, Buffertam, "DF0506", Index);
                     m_TLV.Updatetext(BalReadAftACEMV, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(BalReadAftACEMV, splitBoolLineEdit);
                   }
                break;

                case 0x23:
                  if (Length ==6){
                    UpdateTextFinal(Length, Buffertam, "DF2306", Index);
                    m_TLV.Updatetext(ReadConFloor, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                  }
                  else{
                      m_TLV.UncheckCheckbox(ReadConFloor, splitBoolLineEdit);
                  }
                break;

                case 0x24:
                  if (Length ==6){
                    UpdateTextFinal(Length, Buffertam, "DF2406", Index);
                    m_TLV.Updatetext(RCTLNo, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                  }
                  else{
                      m_TLV.UncheckCheckbox(RCTLNo, splitBoolLineEdit);
                  }
                break;

                case 0x25:
                   if (Length == 6){
                     UpdateTextFinal(Length, Buffertam, "DF2506", Index);
                     m_TLV.Updatetext(RCTL, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                   }
                   else{
                       m_TLV.UncheckCheckbox(RCTL, splitBoolLineEdit);
                   }
                break;

                case 0x26:
                    if (Length ==6){
                      UpdateTextFinal(Length, Buffertam, "DF2606", Index);
                      m_TLV.Updatetext(ReadCVMLimit, splitBoolLineEdit, Buffertam, Index + Sobyte +1, Length);
                    }
                    else{
                        m_TLV.UncheckCheckbox(ReadCVMLimit, splitBoolLineEdit);
                    }
                break;

                default:
                    m_TLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
                break;

            }
            break;

        default:
            m_TLV.UpdateOtherTLV(Buffertam, Index, Sobyte, Length);
            break;
        }
    }
}

/**
 * @brief cPaypassEntryPoint::ChangeStyleSheetLineEdit
 * decide that the text of lineedit in windows appears in the line edit outside.
 * @param index
 * @param text
 * @param length
 * @param widgetType
 * @return
 */
int cPaypassEntryPoint::ChangeStyleSheetLineEdit(int index, QString text,
                                                 int length, mlsBoolWidgetSplitTag_t widgetType,
                                                 QWidget *wid, QList<QCheckBox*> listCheck,
                                                 QString &stringFin)
{
   QString textKerID, textSpinbox;

   if (widgetType == splitBoolLineEdit)
   {
       QLineEdit *line = (QLineEdit*)wid;
       if (listCheck[index]->checkState())
       {      
           if (index == 0)
           {
               if (textFixForDefUDOL != "")
               {
                stringFin += textFixForDefUDOL;
               }
               else
               {
                   stringFin += "DF1A00";
               }
           }
           else
           {
               stringFin += text + "0" + QString::number(length,16) + line->text();
           }
       }
   }
   else if (widgetType == splitBoolComboBox)
   {
       QComboBox *combo = (QComboBox*)wid;
       if (listCheck[index]->checkState())
       {
           if (text == "9F35")
           {
               stringFin += text + +"0" + QString::number(length,16) + combo->currentText();
           }

           else
           {
              for (quint32 i =0; i< m_comboKerID.count(); i++)
              {
                  if (combo->currentText() == m_comboKerID[i])
                  {
                      if ((i+1) < 0x0F)
                      {
                          textKerID.append("0");
                      }
                      textKerID.append(QString::number(i+1,16));
                      break;
                  }
              }
               stringFin += text+ + "0" + QString::number(length,16)+textKerID;
           }

       }
   }
       else
       {
           QSpinBox *spin = (QSpinBox*)wid;
           if (listCheck[index]->checkState())
           {
               if (spin->value()< 0x0F)
               {
                   textSpinbox = "0" + QString::number(spin->value(), 16);
               }
               else
               {
                   textSpinbox = QString::number(spin->value());
               }
               stringFin += text + "0" + QString::number(length,16)+textSpinbox;
            }
       }
   return 0;
}

/**
 * @brief cPaypassEntryPoint::UpdateTextFinal
 * we got textFinal, after doing something in the dialog. the textFinal
 * will be updated to print to line edit Entry Point Configuration.
 * which tag in the buffer appears firstly, it will be saved in the textFinal before.
 * @param length
 * @param Buffertam
 * @param text
 * @param Index
 */
void cPaypassEntryPoint::UpdateTextFinal(int length, uint8_t *Buffertam, QString text, uint8_t Index)
{
    bool   defUDOL= false;
    QString textNeededToAdd;

    textFinal += text;
    if (-1 != text.indexOf("DF1A"))
    {
        textFixForDefUDOL  += text;
        defUDOL = true;
    }

    for (int i =0; i< length; i++){
        if (Buffertam[Index + 2 +1 +i] > 0x0F)
        {
            textNeededToAdd= QString::number(Buffertam[Index +2+1+i], 16).toUpper();

        }
        else
        {
            textNeededToAdd ="0" +QString::number(Buffertam[Index +2 +1+i], 16).toUpper();

        }

        textFinal += textNeededToAdd;

        if (true == defUDOL)
        {
            textFixForDefUDOL += textNeededToAdd;
        }
    }
}

void cPaypassEntryPoint::updateBuffer(uint16_t length, uint8_t bufferTemp[])
{
    for (qint32 i =0; i < 200; i++)
    {
        m_bufferTemp[i] = bufferTemp[i];
    }
    m_length = length;
}

/**
 * @brief cPaypassEntryPoint::UpdateData
 * @param indexUseForOkButton
 */
void cPaypassEntryPoint::UpdateData(qint32 indexUseForOkButton)
{
    QList<QCheckBox*>    listCheckBoxHasButton;
    QList<QToolButton*>  listToolButton;

    listCheckBoxHasButton << ui->checkBox_AddTerCap << ui->checkBox_KerConf << ui->checkBox_SecCap
                          << ui->checkBox_CardDataInpEMV
                          << ui->checkBox_CVMCapEMV << ui->checkBox_CVMCapNoEMV << ui->checkBox_TACDef
                          << ui->checkBox_TACDen << ui->checkBox_TACOnl;

    listToolButton        << ui->toolButtonAddTerCap << ui->toolButtonKerCon
                          << ui->toolButtonSecCap << ui->toolButtonCardData
                          << ui->toolButtonCVMCap << ui->toolButtonCVMCapNo
                          << ui->toolButtonTACDefault << ui->toolButtonTACDenial
                          << ui->toolButtonTACOnline;

    m_indexWidget = indexUseForOkButton;
    textFinal = "";
    CheckTagAvai(m_bufferTemp, m_length);
    ui->textEdit->setText(m_TLV.text);
    m_TLV.UncheckTag();

    for (qint32 i =0; i< listToolButton.count(); i++)
    {
        if (false == listCheckBoxHasButton.at(i)->checkState())
        {
            listToolButton.at(i)->setEnabled(false);
        }
    }

}

void cPaypassEntryPoint::UpdateAddTerCap(const QString &text)
{
    ui->lineEdit_AddTerCap->setText(text);
}

void cPaypassEntryPoint::UpdateTACData(const QString &text)
{
    if (m_lineEditChoseTAC == 1){
        ui->lineEdit_TACDef->setText(text);
    }
    else if (m_lineEditChoseTAC == 2){
        ui->lineEdit_TACDenial->setText(text);
    }
    else ui->lineEdit_TACOnl->setText(text);
}


void cPaypassEntryPoint::Update1ByteLineEdit()
{
    //ui->lineEdit_11->setText(SecCap->scTextLineEdit);
}

void cPaypassEntryPoint::UpdateCardData(const QString &text)
{
    ui->lineEdit_CardDataIn->setText(text);
}

void cPaypassEntryPoint::UpdateSecCap(const QString &text)
{
    ui->lineEdit_SecCap->setText(text);
}

void cPaypassEntryPoint::UpdateCVMCap(const QString &text)
{
    switch(m_lineEditChoseCVM){
    case 1:
        ui->lineEdit_CVMCapCVMReqEMV->setText(text);
        break;
    case 2:
        ui->lineEdit_CVMCapNoEMV->setText(text);
        break;
    }
}

void cPaypassEntryPoint::UpdateKerConfig(const QString &text)
{
    ui->lineEdit_KerCon->setText(text);
}

void cPaypassEntryPoint::checkbox_update_lineEdit(int index)
{
    if (m_listCheckBoxLine[index]->checkState()== Qt::Checked)
    {
        m_listCheckBoxLine[index]->setStyleSheet("QCheckBox{color: black}");
        m_listLineEditCheck[index]->setEnabled(true);
        m_listLineEditCheck[index]->setStyleSheet("border: 1px solid black; border-radius:2px");
        switch (index){
        case 1:
             ui->toolButtonAddTerCap->setEnabled(true);
            break;
        case 3:
             ui->toolButtonKerCon->setEnabled(true);
             break;
        case 10:
             ui->toolButtonSecCap->setEnabled(true);
            break;
        case 11:
             ui->toolButtonCardData->setEnabled(true);
            break;
        case 12:
             ui->toolButtonCVMCap->setEnabled(true);
            break;
        case 13:
             ui->toolButtonCVMCapNo->setEnabled(true);
            break;
        case 15:
             ui->toolButtonTACDefault->setEnabled(true);
            break;
        case 16:
             ui->toolButtonTACDenial->setEnabled(true);
            break;
        case 17:
             ui->toolButtonTACOnline->setEnabled(true);
            break;
        }

    }

    else {
        m_listCheckBoxLine[index]->setStyleSheet("QCheckBox{color: rgb(200, 200, 200)}");
        m_listLineEditCheck[index]->setEnabled(false);
        m_listLineEditCheck[index]->setStyleSheet("border: 1px solid gray; border-radius: 3px;");
        switch (index){
        case 1:
             ui->toolButtonAddTerCap->setEnabled(false);
            break;
        case 3:
             ui->toolButtonKerCon->setEnabled(false);
             break;
        case 10:
             ui->toolButtonSecCap->setEnabled(false);
            break;
        case 11:
             ui->toolButtonCardData->setEnabled(false);
            break;
        case 12:
             ui->toolButtonCVMCap->setEnabled(false);
            break;
        case 13:
             ui->toolButtonCVMCapNo->setEnabled(false);
            break;
        case 15:
             ui->toolButtonTACDefault->setEnabled(false);
            break;
        case 16:
             ui->toolButtonTACDenial->setEnabled(false);
            break;
        case 17:
             ui->toolButtonTACOnline->setEnabled(false);
            break;
        }
    }


}

void cPaypassEntryPoint::button_ok_clicked()
{

}

void cPaypassEntryPoint::on_toolButtonAddTerCap_clicked()
{
    AddTer->show();
    emit ToolButtonAddTerCap_clicked(ui->lineEdit_AddTerCap->text(),  waPayPass);
}

void cPaypassEntryPoint::on_toolButtonTACDefault_clicked()
{
    TACWin->show();
    m_lineEditChoseTAC = 1;
    emit ToolButtonTACDefault_clicked(ui->lineEdit_TACDef->text(), wdPayPass);
}

void cPaypassEntryPoint::on_toolButtonTACDenial_clicked()
{
    TACWin->show();
    m_lineEditChoseTAC = 2;
    emit ToolButtonTACDenial_clicked(ui->lineEdit_TACDenial->text(), wdPayPass);
}

void cPaypassEntryPoint::on_toolButtonTACOnline_clicked()
{
    TACWin->show();
    m_lineEditChoseTAC = 3;
    emit ToolButtonTACOnline_clicked(ui->lineEdit_TACOnl->text(), wdPayPass);
}

void cPaypassEntryPoint::on_toolButtonSecCap_clicked()
{
   winSecCap->show();
   emit ToolButtonSecCap_clicked(ui->lineEdit_SecCap->text());
}

void cPaypassEntryPoint::on_toolButtonCardData_clicked()
{
    winCardData->show();
    emit ToolButtonCardData_clicked(ui->lineEdit_CardDataIn->text());
}

void cPaypassEntryPoint::on_toolButtonCVMCap_clicked()
{
    winCVMCap->show();
    emit ToolButtonnCVMCap_clicked(ui->lineEdit_CVMCapCVMReqEMV->text());
    m_lineEditChoseCVM = 1;
}

void cPaypassEntryPoint::on_toolButtonCVMCapNo_clicked()
{
    winCVMCap->show();
    emit ToolButtonnCVMCap_clicked(ui->lineEdit_CVMCapNoEMV->text());
    m_lineEditChoseCVM = 2;
}

void cPaypassEntryPoint::on_toolButtonKerCon_clicked()
{
    winKerConfig->show();
    emit ToolButtonKerConfig_clicked(ui->lineEdit_KerCon->text());
}

void cPaypassEntryPoint::on_pushButton_clicked()
{
    QString  textOtherTLVTemp;
    QStringList listTags;
    QList<int> lengthTags;

    listTags << "DF1A" << "DF0C" << "9F6D" << "DF1E"
             << "DF2C" << "DF2D" << "DF24" << "DF25" << "DF26"
             << "9F1A" << "9F7E" << "9F40" << "9F09"
             << "DF17" << "DF18" << "DF19" << "DF1B"
             << "DF1C" << "DF1D" << "DF23" << "DF03"
             << "DF20" << "DF21" << "DF22" << "9F35"
             << "DF04" << "DF05";
    lengthTags << 3 << 1 << 2 << 1 << 1 << 3 << 6 << 6
               << 6 << 2 << 1 << 5 << 2 << 1 << 1
               << 1 << 1 << 2 << 1 << 6 << 1
               << 5 << 5 << 5 << 1 << 6 << 6;

    QString textOfAllCheckbox ="";
    for (quint32 i =0; i< listTags.length(); i++)
    {
        ChangeStyleSheetLineEdit(i,listTags[i], lengthTags[i],
                                 m_widgetType[i],m_listWidget[i], m_listCheckboxWid,
                                 textOfAllCheckbox);
    }

    textOtherTLVTemp = ui->textEdit->toPlainText();
    if (0!= (textOtherTLVTemp.length() %2))
    {
        textOtherTLVTemp += "0";
    }

    emit buttonOk_clicked(m_indexWidget, textOfAllCheckbox+textOtherTLVTemp);
    this->close();
}

void cPaypassEntryPoint::on_pushButton_2_clicked()
{
    this->close();
}

void cPaypassEntryPoint::on_textEdit_textChanged()
{
    bool state = false;
    quint8 *bufferTemp = new quint8[256];
    QString      textTemp;

    textTemp   = ui->textEdit->toPlainText();
    state      = m_testTLV.convertStringtoNumAndCheck(textTemp, bufferTemp);

    if (false == state)
    {
        ui->textEdit->setStyleSheet("color: red;border: 1px solid black; border-radius: 3px");
    }
    else
    {
        ui->textEdit->setStyleSheet("color:black;border: 1px solid black;border-radius: 3px");
    }
    delete[] bufferTemp;
}

void cPaypassEntryPoint::on_checkBox_TerType_clicked()
{
    if (ui->checkBox_TerType->checkState()){
        ui->checkBox_TerType->setStyleSheet("QCheckBox{color: black}");
        ui->comboBoxTerType->setEnabled(true);
    }
    else {
        ui->checkBox_TerType->setStyleSheet("QCheckBox{color: rgb(200, 200, 200)}");
        ui->comboBoxTerType->setEnabled(false);
    }
}

void cPaypassEntryPoint::on_checkBox_KerID_clicked()
{
    if (ui->checkBox_KerID->checkState()){
        ui->checkBox_KerID->setStyleSheet("QCheckBox{color: black}");
        ui->comboBox_KerID->setEnabled(true);
    }
    else {
        ui->checkBox_KerID->setStyleSheet("QCheckBox{color: rgb(200, 200, 200)}");
        ui->comboBox_KerID->setEnabled(false);
    }
}

void cPaypassEntryPoint::on_checkBox_MaxNumTorn_clicked()
{
    if (ui->checkBox_MaxNumTorn->checkState()){
        ui->checkBox_MaxNumTorn->setStyleSheet("QCheckBox{color: black}");
        ui->spinBox_MaxNumTorn->setEnabled(true);
    }
    else{
        ui->checkBox_MaxNumTorn->setStyleSheet("QCheckBox{color: rgb(200, 200, 200)}");
        ui->spinBox_MaxNumTorn->setEnabled(false);
    }
}
