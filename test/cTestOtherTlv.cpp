#include "cTestOtherTLV.h"

cTestOtherTLV::cTestOtherTLV()
{

}

bool cTestOtherTLV::convertStringtoNumAndCheck(QString stringInput, quint8 *bufferTemp)
{
    qint16 stringLength = 0;
    qint16 bufferLength = 0;
    bool  state =0;

    stringLength = stringInput.length();
    if(stringLength %2 == 1){
        return false;
    }

    bufferLength = stringLength/2;
    for (int i =0; i< bufferLength; i++){
        bufferTemp[i] = stringInput.mid(2*i, 2).toInt(NULL, 16);
    }

    state = checkTagOtherTLV(bufferTemp, bufferLength);
    return state;
}

bool cTestOtherTLV::checkTagOtherTLV(quint8 *bufferTemp, quint16 lengthTotal)
{
    quint16 numBytes =0, index =0;
    quint16 lengthTemp, lengthTag =0;

    lengthTemp = lengthTotal;

    while (lengthTemp >0){
        if ((bufferTemp[index] & 0x1F) == 0x1F){
            if (((index + 1) < lengthTotal) && bufferTemp[index +1] == 0x80)
            {
                numBytes = 3;
            }
            else if ((index +1) < lengthTotal)
            {
                numBytes = 2;
            }

            else if ((index +1) == lengthTotal)
            {
                return true;
            }
        }

        else
        {
            numBytes = 1;
        }

        if (((numBytes + index) < lengthTotal) && bufferTemp[index + numBytes] == 0x81)
        {
            numBytes++;
        }

        if (index + numBytes == lengthTotal){
            return true;
        }

        lengthTag = bufferTemp[index + numBytes];

        if (lengthTag + 1 + numBytes > lengthTemp){
            return false;
        }

        index += numBytes + lengthTag + 1;
        lengthTemp -=  (numBytes + lengthTag +1);
    }

    return true;

}
