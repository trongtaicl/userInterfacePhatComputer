#include "cGetForAce.h"
#include "ui_cGetForAce.h"
#include <QDebug>
#include <QComboBox>

cGetForACE::cGetForACE(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::cGetForACE)
{
    ui->setupUi(this);
    this->setWindowTitle("Dialog");
}

cGetForACE::~cGetForACE()
{
    delete ui;
}

void cGetForACE::getSignalForACE(mls_tabSignal_t tabSelected)
{
    QString path = QCoreApplication::applicationDirPath();

    QString fileSelected;

    switch (tabSelected) {
    case ts_aceFromProcessing:
        fileSelected = path + "/processing_conf.ini";
        break;
    case ts_aceFromEntryPoint:
        fileSelected = path + "/entrypoint_conf.ini";
        break;
    case ts_aceFromDRL:
        fileSelected = path + "/drlset_conf.ini";
        break;
    }

    QFile file(fileSelected);

    if (!file.open(QIODevice::ReadOnly))
    {
        ui->comboBox_Library->clear();
        ui->comboBox_Conf->clear();
        return;
    }

    ui->comboBox_Library->clear();
    ui->comboBox_Conf->clear();
    m_libAndConf.clear();
    QTextStream out(&file);
    while (!out.atEnd())
    {
        QString data = out.readLine();
        QStringList stringSplitted;
        QStringList stringForConf;
        int       index;
        index = data.lastIndexOf(";");
        data.remove(index, 1);
        stringSplitted = data.split(";");
        if (stringSplitted.length() >= 2)
        {
          for (int i =1; i<stringSplitted.length(); i++)
          {
              stringForConf << stringSplitted.at(i);
              qDebug() << stringSplitted.at(i);
          }
          m_libAndConf << stringForConf;
          ui->comboBox_Library->addItem(stringSplitted.at(0));
        }
    }
}


void cGetForACE::on_pushButton_OK_clicked()
{
    emit buttonOk_clicked();
    this->close();
}

void cGetForACE::on_pushButton_Cancel_clicked()
{
    emit buttonCancel_clicked();
    this->close();
}




void cGetForACE::on_comboBox_Library_currentIndexChanged(int index)
{
        ui->comboBox_Conf->clear();
        qDebug() << "index " << index;
        if (ui->comboBox_Library->count() != 0)
        {
            for (int i =0; i< m_libAndConf.at(index).length(); i++)
            {
                qDebug() << m_libAndConf.at(index).at(i);
                ui->comboBox_Conf->addItem(m_libAndConf.at(index).at(i));
            }
        }
}
