#ifndef CGETFORACE_H
#define CGETFORACE_H

#include <QDialog>
#include <QTextStream>
#include <QFile>
#include <QStringList>
#include <QPair>
#include <QList>
#include <QtCore>

namespace Ui {
class cGetForACE;
}

typedef enum{
    ts_aceFromProcessing = 0x00,
    ts_aceFromEntryPoint = 0x01,
    ts_aceFromDRL        = 0x02
}mls_tabSignal_t;

class cGetForACE : public QDialog
{
    Q_OBJECT

public:
    explicit cGetForACE(QWidget *parent = 0);
    ~cGetForACE();

public slots:
    void getSignalForACE(mls_tabSignal_t tabSelected);

private slots:
    void on_pushButton_OK_clicked();

    void on_pushButton_Cancel_clicked();

    void on_comboBox_Library_currentIndexChanged(int index);

signals:
    void buttonOk_clicked();
    void buttonCancel_clicked();
private:

    Ui::cGetForACE *ui;
    QList<QStringList>  m_libAndConf;
};

#endif // CGETFORACE_H
