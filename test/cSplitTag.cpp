#include "cSplitTag.h"
#include <QDebug>

cSplitTag::cSplitTag()
{
   for (qint32 i =0; i < 25; i++)
   {
       m_stateCheckboxLineEdit[i] = false;
   }

   for (qint32 i =0; i< 5; i++)
   {
       m_stateCheckboxCombo[i] = false;
       m_stateCheckboxSpin[i]  = false;
   }
  for (quint32 i  =0;i < 27; i++)
  {
      m_stateCheckBox[i] = false;
  }

}

void cSplitTag::initializeList(QList<QWidget*> listWid, QList<QCheckBox*> listCheck,
                               QStringList stringComboTerType, QStringList stringComboKerId,
                               QStringList stringStat, mlsBoolWidgetSplitTag_t widgetType[])
{
    m_listWid = listWid;
    m_listCheck = listCheck;
    m_stringComboTerType = stringComboTerType;
    m_stringComboKerID = stringComboKerId;
    m_stringComboState = stringStat;
    for (quint32 i =0; i< 27; i++)
    {
        m_widgetType[i] = widgetType[i];
    }

}

void cSplitTag::updateComboType(mls_comboType_t comboType)
{
    m_comboType = comboType;
}

void cSplitTag::UncheckTag()
{
    text = "";
    for (int i =0; i< m_listCheck.length(); i++)
    {
        if (m_stateCheckBox[i] == false)
        {
                m_listCheck[i]->setChecked(false);
                m_listCheck[i]->setStyleSheet("QCheckBox{color: rgb(170, 170, 170)}");

                if (m_widgetType[i] == splitBoolComboBox)
                {
                    QComboBox *combo = (QComboBox*)m_listWid[i];
                    if (i ==1) combo->setCurrentIndex(1);
                    combo->setEnabled(false);
                }

                if (m_widgetType[i] == splitBoolSpinBox)
                {
                    QSpinBox *spin = (QSpinBox*)m_listWid[i];
                    spin->setValue(0);
                    spin->setEnabled(false);
                }

                if (m_widgetType[i] == splitBoolLineEdit)
                {
                    QLineEdit *line = (QLineEdit*)m_listWid[i];
                    if (i != 0)
                    {
                        line->setText("000000000000");
                    }
                    else
                    {
                        line->setText("000000");
                    }
                    line->setEnabled(false);
                    line->setStyleSheet("border: 1px solid gray; border-radius: 3px;");
                }
              }
             m_stateCheckBox[i] = false;
    }

}

void cSplitTag::Updatetext(int index, mlsBoolWidgetSplitTag_t Widget, uint8_t *Buffertam,
                           uint8_t Pos, uint8_t Length)
{
        QString Text;
        bool flagComboTerType = false;
        bool flagComboKerID   = false;
        bool flagComboStat    = false;

        if ((Widget == splitBoolLineEdit)|| (Widget == splitBoolComboBox))
        {
              for (int i = 0; i < Length; i++)
              {
                  Text += QString("%1").arg(Buffertam[Pos+i], 2, 16, QChar('0')).toUpper();
              }

              if (Widget == splitBoolLineEdit)
              {
                  QLineEdit *lineEdit = (QLineEdit*)m_listWid.at(index);

                  m_stateCheckBox[index] = true;
                  lineEdit->setText(Text);
                  m_listCheck.at(index)->setChecked(true);
                  m_listCheck.at(index)->setStyleSheet("QCheckBox{color: black}");
                  lineEdit->setEnabled(true);
                  lineEdit->setStyleSheet("border: 1px solid black; border-radius: 3px;");
              }

              else if (m_comboType == ctTerType){
                  QComboBox *combo = (QComboBox*)m_listWid.at(index);
                  for (int i =0; i<m_stringComboTerType.count(); i++)
                  {
                      if (Text == m_stringComboTerType[i]){
                          m_stateCheckBox[index] = true;
                          combo->setCurrentText(m_stringComboTerType[i]);
                          flagComboTerType = true;
                          combo->setEnabled(true);
                          m_listCheck.at(index)->setStyleSheet("QCheckBox{color:black}");
                          m_listCheck.at(index)->setChecked(true);
                      }
              }

                  if (!flagComboTerType){
                      combo->setCurrentText("00");
                      combo->setEnabled(false);
                      m_listCheck.at(index)->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
                      m_listCheck.at(index)->setChecked(false);
                  }
              }

              else if (m_comboType == ctKerID){
                  QComboBox *combo = (QComboBox*)m_listWid.at(index);
                  m_stateCheckBox[index] = true;
                  for (int i =0; i< m_stringComboKerID.count(); i++){
                      if ((i+1) == Buffertam[Pos]){

                          combo->setCurrentText(m_stringComboKerID[i]);
                          combo->setEnabled(true);
                          m_listCheck.at(index)->setStyleSheet("QCheckBox{color:black}");
                          m_listCheck.at(index)->setChecked(true);
                          flagComboKerID = true;
                      }
                  }
                  if (!flagComboKerID){
                      combo->setCurrentText("MasterCard");
                      combo->setEnabled(false);
                      m_listCheck.at(index)->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
                      m_listCheck.at(index)->setChecked(false);
                  }
              }
              else if (m_comboType == ctStat){
                  m_stateCheckBox[index] = true;
                  QComboBox *combo = (QComboBox*)m_listWid.at(index);
                  for (int i =0; i< m_stringComboState.count(); i++){

                      if (i == Buffertam[Pos])
                      {
                          combo->setCurrentText(m_stringComboState[i]);
                          combo->setEnabled(true);
                          m_listCheck[index]->setStyleSheet("QCheckBox{color:black}");
                          m_listCheck[index]->setChecked(true);
                          flagComboStat = true;
                      }
                  }
                  if (!flagComboStat){
                      combo->setCurrentText("not allowed (0x00)");
                      combo->setEnabled(false);
                      m_listCheck[index]->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
                      m_listCheck[index]->setChecked(false);
                  }
        }
        }

        if (Widget == splitBoolSpinBox){
            m_stateCheckBox[index] = true;
            QSpinBox *spin = (QSpinBox*)m_listWid.at(index);
            if (Length <2){
                spin->setValue(Buffertam[Pos]);
                m_listCheck[index]->setChecked(true);
                m_listCheck[index]->setStyleSheet("QCheckBox{color:black}");
            }
            else{
                spin->setValue(0);
                spin->setEnabled(false);
                m_listCheck[index]->setChecked(false);
                m_listCheck[index]->setStyleSheet("QCheckBox{color:rgb(170, 170, 170)}");
            }
        }
}

void cSplitTag::UpdateOtherTLV(uint8_t *Buffertam, uint8_t Pos, uint8_t Sobyte, uint8_t Length)
{ 
    for (int i =0; i< Length + Sobyte +1; i++)
    {
        text += QString("%1").arg(Buffertam[Pos+i], 2, 16, QChar('0')).toUpper();
    }
}

void cSplitTag::UncheckCheckbox(int index, mlsBoolWidgetSplitTag_t Widget)
{
    if (Widget == splitBoolLineEdit){
        m_listCheck[index]->setChecked(false);
        m_listCheck[index]->setStyleSheet("QCheckBox{color: rgb(170, 170, 170)}");
        QLineEdit *line = (QLineEdit*)m_listWid.at(index);
        line->setEnabled(false);
        line->setStyleSheet("border: 1px solid gray; border-radius: 3px;");
        if (index !=0)
        {
            line->setText("000000000000");
        }
        else
        {
            line->setText("000000");
        }
    }
    else if (Widget == splitBoolComboBox){
        QComboBox *combo = (QComboBox*)m_listWid.at(index);
        if (m_comboType == ctTerType){
            m_listCheck[index]->setChecked(false);
            m_listCheck[index]->setStyleSheet("QCheckBox{corlor: rgb(170, 170, 170)}");
            combo->setEnabled(false);
            combo->setCurrentText("00");
        }
        else if (m_comboType == ctKerID){
            m_listCheck[index]->setChecked(false);
            m_listCheck[index]->setStyleSheet("QCheckBox{corlor: rgb(170, 170, 170)}");
            combo->setEnabled(false);
            combo->setCurrentText("MasterCard");
        }
        else if (m_comboType == ctStat){
            m_listCheck[index]->setChecked(false);
            m_listCheck[index]->setStyleSheet("QCheckBox{corlor: rgb(170, 170, 170)}");
            combo->setEnabled(false);
            combo->setCurrentText("not allowed (0x00)");
        }
    }
    else {
        QSpinBox *spin = (QSpinBox*)m_listWid.at(index);
        m_listCheck[index]->setChecked(false);
        m_listCheck[index]->setStyleSheet("QCheckBox{corlor: rgb(170, 170, 170)}");
        spin->setValue(0);
        spin->setEnabled(false);
    }

}
