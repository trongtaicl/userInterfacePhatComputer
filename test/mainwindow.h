#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QtWidgets>
#include <QWidget>
#include <QDialog>
#include <QToolButton>
#include <QButtonGroup>
#include <QList>
#include <QResizeEvent>
#include <QRegExp>
#include <QRegExpValidator>
#include "cTerminalCapabilities.h"
#include "cAdditionalTerCapabilities.h"
#include "cTerminalActionCode.h"
#include <QString>
#include <QComboBox>
#include "cPaypassEntryPoint.h"
#include <QSignalMapper>
#include <QList>
#include <QComboBox>
#include <cCreateStaticVariable.h>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QAction>
#include <cAceExit.h>
#include <cHelpInfo.h>
#include <cSplitTag.h>
#include <QByteArray>
#include <QLineEdit>
#include <QPainter>
#include <QImage>
#include <QTabBar>
#include <cValidator.h>
#include <QTextCursor>
#include <cReadXmlFile.h>
#include <cGetForAce.h>
#include "cTestSocketWithoutSignal.h"
#include <QThread>

namespace Ui {
class MainWindow;
class CustomTabStyle;
}

#define BUFFER_SIZE  32768
#define MAX_COL      600
#define MAX_ROW      600

typedef enum{
    mproTACDef      = 0x00,
    mproTACOnl      = 0x01,
    mproTACDen      = 0x02,
    mterTACDef      = 0x03,
    mterTACOnl      = 0x04,
    mterTACDen      = 0x05
}mlsMainTACSelect_t;

typedef enum{
    twLineEdit      = 0x00,
    twSpinBox       = 0x01,
    twCheckBox      = 0x02,
    twComboBox      = 0x03,
    twTextEdit      = 0x04
}mlsTypeWidget_t;

typedef enum{
    ptAcquireId       = 0x00,
    ptAID             = 0x01,
    ptAppSelInd       = 0x02,
    ptAppVerNum       = 0x03,
    ptSkipTAC         = 0x04,
    ptRTS             = 0x05,
    ptVelChecSupp     = 0x06,
    ptFloorLimitCheck = 0x07,
    ptTACSupp         = 0x08,
    ptTACDef          = 0x09,
    ptTACDenial          = 0x0a,
    ptTACOnl          = 0x0b,
    ptFloorLimit      = 0x0c,
    ptTargPer         = 0x0d,
    ptThreshVal       = 0x0e,
    ptMaxTargPer      = 0x0f,
    ptDefDDOL         = 0x10,
    ptDDOL            = 0x11,
    ptDefTDOL         = 0x12,
    ptTDOL            = 0x13,
    ptTranCurCode     = 0x14,
    ptTranCurExpo     = 0x15
}mls_ProcessingTab_t;



class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void initializeTerminalConfiguration();
    void initializeMainWindow();
    void initializeProcessingConfiguration();
    void initializeEntryPoint();
    void initializeDataStorage();
    void initializeSetup();
    void initializeDRLVisa();

    QString updateDir(QString dir);

    uint16_t CheckTag(const QString &text, uint8_t *bufferTmp);

    void UpdateIndexTable();

    bool checkTagAvai(quint8 *bufferTmp, quint16 lengthTotal, QString &error);

    bool updateCheckBoxLineSpin(quint8 index, quint8 numByte, quint8 *bufferTmp,
                                quint8 length, QString &error);

    void updateBufferToSaveTer(quint8 *buffer, quint8 length, quint32 *lengthOfTLV);

    bool checkTLV(quint8 *bufferTemp, quint16 lengthTotal, quint16 ith);

    bool getAndCheckAIDProcessing(quint8 *buffer, quint16 lengthTotal,
                                  QString &err, quint32 *lengthFF, quint8 *numItem);

    void updateCheckBoxLineProcess(quint8 index, quint8 numByte, quint8* bufferTemp,
                                   quint8 length, QString &textDDOL, QString &textTDOL);

    void storeTagToDatabaseProcess(quint8 (*database)[MAX_COL],
                                   quint8 ithRow, quint8 **bufferTmp, quint32 *lengthTLV);

    bool getIndexOfTag4F(quint8 *buffer, quint8 length, quint8 *ith);

    bool checkLengthEachTagProcess(quint8 *buffer, quint32 lengthTotal, quint8 **database,
                                   QString &err);

    void checkWidgetToUpdateDatabaseProcess(quint8 (*database)[MAX_COL], quint8 *lengthBuf, quint8 index);

    void initializeBufferDatabase(quint8 **database);

    void changeLengthLineEdit(QLineEdit *lineEdit, quint8 length);

    bool getAndCheckTagFF35Entry(quint8 *buffer, quint16 lengthTotal, QString &err, quint32 *lengthFF35,
                                 quint8 *numRow);

    bool  checkTLVEntryPoint(quint8 *bufferTemp, quint16 lengthTotal, quint32 *indexDf0e,
                             bool *df0ePresent, QString &stringLineEdit);

    void  insertComboBoxLineEditEntry(QPair<QStringList, QStringList> stringList, quint16 ithRow, quint8 lengthDf0e);

    void  getPathForTerminalProcessingEntrypoint();

    bool loadTerminalFile(const QString &fileName, bool initiOrLoad);

    bool  loadProcessingFile(const QString &fileName, bool initOrLoad);

    bool loadEntryPointFile(const QString &fileName, bool initOrLoad);
protected:
   void resizeEvent(QResizeEvent *event);
   void paintEvent(QPaintEvent *event);
signals:
   void ButtonTerminal_Clicked(const QString &text);
   void ButtonAddition_Clicked(const QString &text, mlsWinAdd_t winSelected);
   void ButtonDefault_Clicked(const QString &text, mlsWindowDef_t winSelected);
   void ButtonOnline_Clicked(const QString &text, mlsWindowDef_t winSelected);
   void Button_7_Clicked(const QString &text, mlsWindowDef_t winSelected);
   void UpdatePaypass(qint32 indexWidget);
   void UpdatePaywave();
   void adminShow(const QString &text);
   void terminalCapShow(const QString &text);

   void sendStringToCheck(QString &text);

   void selectACE(mls_tabSignal_t tab);

   void startToSendTCP();

public slots:
    void GroupButtonClick(int Index);
    void Additional_Update(const QString &text);
    void Terminal_Update(const QString &text);
    void Default_Update(const QString &text);

    void OpenNewWindow(int Index);

    void updateLineEditEntryPointConfig(qint32 index, QString text);

    void listWidgetItemChanged();

    void aceExitClicked();

    void closeAdmin(const QString &text);

    void comboEntryPointChanged(int id);

    void lineEditEntryPointChanged();

    void getResultTDOL(bool a);

    void getResultDDOL(bool a);

    void errorTCP(QString &err, mls_tabSend_t ithTab);

    void writeSuccessfully(mls_tabSend_t ithTab);

private slots:
    void on_toolButtonAddition_clicked();

    void on_toolButtonDefault_clicked();

    void on_toolButtonOnline_clicked();

    void on_toolButton_7_clicked();

    void on_checkBoxDefault_clicked();

    void on_toolButtonTerminal_clicked();

    void on_toolButtonAddProcessing_clicked();

    void on_toolButtonDenialProcessing_clicked();

    void on_toolButtonOnlineProcessing_clicked();

    void on_toolButtonEntryUp_clicked();

    void on_toolButtonEntryDown_clicked();

    void on_toolButtonEntryAdd_clicked();

    void on_toolButtonEntryRemove_clicked();


    void on_ButtonEntrySave_clicked();

    void on_ButtonOpen_clicked();

    void on_ButtonOpenProcessing_clicked();

    void on_ButtonEntryOpen_clicked();

    void on_ButtonSaveAs_clicked();

    void on_ButtonSaveAsProcessing_clicked();

    void on_ButtonEntrySaveAs_clicked();

    void on_checkBoxAutorun_clicked();

    void on_toolButtonRemoveProcessing_clicked();

    void on_toolButtonUpProcessing_clicked();

    void on_toolButtonDownProcessing_clicked();

    void on_checkBox_TACProcess_clicked();

    void on_toolButtonDefaultProcessing_clicked();

    void on_listWidgetProcessing_itemChanged(QListWidgetItem *item);

    void on_listWidgetProcessing_itemClicked(QListWidgetItem *item);

    void on_toolButtonLoadDEK_clicked();

    void on_toolButtonClearDEK_clicked();

    void on_ButtonSave_clicked();

    void on_checkBox_TTQ_clicked();

    void on_checkBox_Referral_clicked();

    void on_toolButton_DRLAdd_clicked();

    void on_toolButton_DRLRemove_clicked();

    void on_toolButton_DRLUp_clicked();

    void on_toolButton_DRLDown_clicked();

    void on_toolButton_DRLOpen_clicked();

    void on_toolButton_DRLSaveAs_clicked();

    void on_ButtonSaveProcessing_clicked();

    void on_toolButton_DRLSave_clicked();

    void on_actionExit_Ctrl_Q_triggered();

    void on_actionExport_triggered();

    void on_actionSend_CA_Keys_triggered();

    void on_actionSend_Revocated_CA_Keys_triggered();

    void on_actionAdministrative_triggered();

    void on_actionAbout_triggered();

    void on_actionLoad_triggered();

    void on_actionSave_triggered();

    void on_actionSave_As_triggered();

    void on_actionLoad_2_triggered();

    void on_actionSave_2_triggered();

    void on_actionSave_As_2_triggered();

    void on_actionLoad_3_triggered();

    void on_actionSave_3_triggered();

    void on_actionSave_As_3_triggered();

    void on_checkBoxAutorun_stateChanged(int arg1);

    void on_checkBoxDefault_stateChanged(int arg1);

    void on_comboBoxTerminal_currentIndexChanged(int index);

    void on_lineEditTerminal_textEdited(const QString &arg1);

    void on_lineEditAddition_textEdited(const QString &arg1);

    void on_lineEditTerCounCode_textEdited(const QString &arg1);

    void on_lineEditPINTimeout_textEdited(const QString &arg1);

    void on_checkBoxEMV1_clicked();

    void on_checkBoxMastripe_clicked();

    void on_comboBoxCDA_currentIndexChanged(int index);

    void on_checkBoxVelocity_clicked();

    void on_checkBoxRTSNot_clicked();

    void on_checkBoxPinBypass_clicked();

    void on_checkBoxPSE_clicked();

    void on_checkBoxCardHolder_clicked();

    void on_lineEditOnline_textEdited(const QString &arg1);

    void on_lineEditDefault_textEdited(const QString &arg1);

    void on_lineEditDenial_textEdited(const QString &arg1);

    void on_checkBoxOnline_clicked();

    void on_checkBoxReferral_clicked();

    void on_checkBoxAdvice_clicked();

    void on_checkBoxEMV2_clicked();

    void on_spinBox_valueChanged(int arg1);

    void on_spinBox_editingFinished();

    void on_lineEditOnline_editingFinished();

    void on_lineEditDefault_editingFinished();

    void on_lineEditDenial_editingFinished();

    void on_lineEditPINTimeout_editingFinished();

    void on_lineEditTerCounCode_editingFinished();

    void on_lineEditAddition_editingFinished();

    void on_lineEditTerminal_editingFinished();

    void on_comboBoxTerminal_editTextChanged(const QString &arg1);

    void on_comboBoxCDA_editTextChanged(const QString &arg1);

    void on_comboBoxTerminal_activated(const QString &arg1);

    void on_comboBoxCDA_activated(const QString &arg1);

    void on_checkBox_TACProcess_stateChanged(int arg1);

    void on_checkBox_SkipTACProcess_clicked();

    void on_lineEditAcquireIDProcessing_textEdited(const QString &arg1);

    void on_checkBox_ASIProcess_clicked();

    void on_lineEditAVNProcessing_textEdited(const QString &arg1);

    void on_lineEditDenialProcessing_textEdited(const QString &arg1);

    void on_lineEditOnlineProcessing_returnPressed();

    void on_lineEditOnlineProcessing_textEdited(const QString &arg1);

    void on_lineEditDefaultProcessing_textEdited(const QString &arg1);

    void on_checkBox_FloorLimitProcess_clicked();

    void on_lineEditProcessingFloor_textEdited(const QString &arg1);

    void on_checkBox_DefaultDDOL_clicked();

    void on_checkBox_DefaultTDOL_clicked();

    void on_checkBox_RTSProcess_clicked();

    void on_checkBox_VelocityProcess_clicked();

    void on_spinBox_TargetProcess_editingFinished();

    void on_spinBox_MaxTargetProcess_editingFinished();

    void on_lineEditProcessingTCC_textEdited(const QString &arg1);

    void on_lineEditProcessingExpo_textEdited(const QString &arg1);

    void on_lineEditProcessingThresh_textEdited(const QString &arg1);

    void on_spinBox_MaxTargetProcess_valueChanged(int arg1);

    void on_tabWidget_2_tabBarClicked(int index);

    void on_textEdit_DDOL_textChanged();

    void on_textEdit_TDOL_textChanged();

    void on_ButtonACEProcessing_clicked();

    void on_ButtonEntryACE_clicked();

    void on_ButtonSend_clicked();

    void on_ButtonSendProcessing_clicked();

    void on_listWidgetProcessing_clicked(const QModelIndex &index);

    void on_ButtonEntrySend_clicked();

    void on_actionSendTerminal_triggered();

    void on_actionSendProcessing_triggered();

    void on_actionSendEntryPoint_triggered();

private:
    Ui::MainWindow *ui;
    QToolButton *ButtonWelcome;
    QToolButton *ButtonConfiguration;
    QToolButton *ButtonTransaction;
    QToolButton *ButtonBitmap;
    QToolButton *ButtonTool;
    QToolButton *ButtonTerminal;
    QToolButton *ButtonSetup;
    QAction     *actionClose;

    QButtonGroup *group;

    QList<QAbstractButton*> *ListButton;
    QList<QWidget*>                   m_listWid;
    QList<QWidget*>                   m_listWidProcess;

    cTerminalCapabilities             *TermCap;
    cAdditionalTerCapabilities        *AddTerm;
    cTerminalActionCode               *Def;
    cPaypassEntryPoint                *PayPass;
    cAceExit                          *aceExit;
    cReadXmlFile                      *m_readXML;
    //cAdministrative                   *admin;
    cHelpInfo                         *helpShow;
    cSplitTag                         m_TLV;
    cGetForACE                        *m_getACE;

    cTestSocketWithoutSignal          *m_socket;

    QThread                           *m_workerThread;

    cValidator                        *mValidTDOL;
    cValidator                        *mValidDDOL;

    QSignalMapper                     *sigMapp;
    QSignalMapper                     *m_sigMappComboKerId;
    QSignalMapper                     *m_sigMappComboAID;
    QSignalMapper                     *m_sigMappComboTrans;
    QSignalMapper                     *m_sigMappLineEdit;

    QList<QToolButton*>               ListToolButton;
    QList<QLineEdit*>                 ListLineEdit;
    QList<QComboBox*>                 listComboKernelID;
    QList<QComboBox*>                 listComboAID;
    QList<QComboBox*>                 listComboTranType;

    QStringList                       m_tagConfiguration;

    int Count =0;
    int numRowTable =0;

    QString m_dirDataStorage;
    QString m_textAdmin;

    QString m_dir1 ;

    mlsMainTACSelect_t LineEditChosed = mproTACDef; // point to which line edit is chosed between Default, Online.

    mlsTypeWidget_t    m_typeWidgetTer[23];
    bool               m_stateTagTer[23];
    bool               m_stateTagProcessing[21];

    mlsTypeWidget_t    m_typeWidgetProcessing[21];

    quint8             m_databaseArray[MAX_ROW][MAX_COL];
    quint32             m_lengthOfEachItemListWidgetProcessing[MAX_COL];
    quint8             m_numItemsProcess;

    quint8             m_databaseEntryPoint[MAX_ROW][MAX_COL];
    quint32            m_lengthOfEachItemEntryPoint[MAX_COL];
    quint8             m_numRows;

    quint8             m_draw= 0;
    quint16            m_x;
    quint16            m_y;

    QString            m_terminalDir;
    QString            m_processingDir;
    QString            m_entryPointDir;
    QString            m_fileXML;

    QString            stringDir;

};



#endif // MAINWINDOW_H
