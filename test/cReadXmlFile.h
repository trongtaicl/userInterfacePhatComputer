#ifndef CREADXMLFILE_H
#define CREADXMLFILE_H
#include <QString>
#include <QXmlStreamWriter>
#include <QFileDialog>
#include <QtCore>
#include <QtGui>
#include <QTranslator>
#include <QXmlStreamReader>
#include <QStringRef>

class cReadXmlFile: public QObject
{
    Q_OBJECT
public:
    cReadXmlFile();

    void writeXMLFile(QString &fileName, QString &terminalPath, QString &processingPath, QString &entryPointPath);

    bool readXMLFile(QString &fileName, QString &terminalPath, QString &processingPath, QString &entryPoint);

    bool check(QString &fileName, QString &terminal, QString &processing, QString &entrypoint);
};

#endif // CREADXMLFILE_H
