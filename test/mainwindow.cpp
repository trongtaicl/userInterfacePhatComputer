#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTabWidget>
#include <QTabBar>
#include <QToolButton>
#include <QDebug>
#include <QPixmap>
#include <QButtonGroup>
#include <QStyle>
#include <QListWidget>
#include <QComboBox>
#include <QLineEdit>
#include "cSha1.h"

/* task:
 * resize window khi them cac tool button
 * */

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("SEPOSS");
    this->resize(1300, 800);
    this->setMinimumSize(1300, 800);

    QImage *image = new QImage();
    image->load(":/image/image/bitmap.png");
    image->save(":/image/image/bitmap1.png");

    ui->tabWidget->resize(1280, 780);

    m_dirDataStorage = "C:/";

    // define resource from class
    TermCap             = new cTerminalCapabilities(this);
    AddTerm             = new cAdditionalTerCapabilities(this);
    Def                 = new cTerminalActionCode(this);
    PayPass             = new cPaypassEntryPoint(this);
    aceExit             = new cAceExit(this);
    m_readXML           = new cReadXmlFile();    // database to save directory
    m_getACE            = new cGetForACE(this);
    m_socket            = new cTestSocketWithoutSignal();

    helpShow            = new cHelpInfo(this);
    mValidTDOL          = new cValidator();
    mValidDDOL          = new cValidator();

    // tool button in toolbar
    ButtonWelcome       = new QToolButton(this);
    ButtonConfiguration = new QToolButton(this);
    ButtonBitmap        = new QToolButton(this);
    ButtonTransaction   = new QToolButton(this);
    ButtonTool          = new QToolButton(this);
    ButtonTerminal      = new QToolButton(this);
    ButtonSetup         = new QToolButton(this);
    actionClose         = new QAction(this);

    /* length of Each item is the length use in processing configuration tab */
    for (quint32 i =0; i<MAX_COL; i++)
    {
        m_lengthOfEachItemListWidgetProcessing[i] = 0;
    }

    /* shorcut to close the app */
    actionClose->setShortcut(Qt::Key_Q | Qt::CTRL);
    connect(actionClose, SIGNAL(triggered()), this, SLOT(close()));
    this->addAction(actionClose);

    // setting tabWidget in west direction
    ui->tabWidget->setTabPosition(QTabWidget::West);
    ui->tabWidget->setStyleSheet("QTabBar::tab{height:70px; width:80px;border: 0px;} QTabWidget::pane{border:none}");
    ui->tabWidget->setCurrentIndex(0);

    // create a button group for all the button in Toolbar to do a loop
    group = new QButtonGroup(this);
    group->addButton(ButtonWelcome, 0);
    group->addButton(ButtonConfiguration, 1);
    group->addButton(ButtonTransaction, 2);
    group->addButton(ButtonBitmap, 3);
    group->addButton(ButtonTool, 4);
    group->addButton(ButtonTerminal, 5);
    group->addButton(ButtonSetup, 6);

    // get number of elements in group
    QList<QAbstractButton*> ListButton = group->buttons();
    Count = ListButton.count();

    /*use label 2 to insert the background image */
    ui->label_Background->resize(1140, 780);
    // image for welcome
    QPixmap p(":/image/image/background1.jpg");
    ui->label_Background->setPixmap(p);
    ui->label_Background->setPixmap(p.scaled(1140, 780, Qt::KeepAspectRatio));

    initializeProcessingConfiguration();
    initializeTerminalConfiguration();
    initializeMainWindow();
    initializeEntryPoint();
    initializeDataStorage();
    initializeSetup();
    initializeDRLVisa();

    /*load the terminal, processing, entryPoint direction in database */
    getPathForTerminalProcessingEntrypoint();

    m_textAdmin = "000000000000000000000000000000"
                  "000000000000000000000000000001";

    // close the app
    connect(aceExit, SIGNAL(buttonOK_clicked()), this, SLOT(aceExitClicked()));

    // which button is clicked, go to that tab
    connect(group, SIGNAL(buttonClicked(int)), this, SLOT(GroupButtonClick(int)));

    //button additional is clicked, open new window
    connect(this, SIGNAL(ButtonAddition_Clicked(QString,mlsWinAdd_t)), AddTerm, SLOT(change_text_edit(QString,mlsWinAdd_t)));

    // button default is clicked, open new window
    connect(this, SIGNAL(ButtonDefault_Clicked(QString,mlsWindowDef_t)), Def,
            SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    // button online is clicked, open new window
    connect(this, SIGNAL(ButtonOnline_Clicked(QString,mlsWindowDef_t)), Def,
            SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    connect(this, SIGNAL(Button_7_Clicked(QString,mlsWindowDef_t)), Def, SLOT(def_change_text_edit(QString,mlsWindowDef_t)));

    // button ok in def dialog is pressed, go to slot default_update
    connect(Def, SIGNAL(ButtonOk_Clicked(QString)), this, SLOT(Default_Update(QString)));

    //connect(this, SIGNAL(adminShow(QString)), admin, SLOT(updateTextLineEdit(QString)));

    //connect(admin, SIGNAL(closeButtonClicked(QString)), this, SLOT(closeAdmin(QString)));

    /* when press ACE button, ACE dialog shows */
    connect(this, SIGNAL(selectACE(mls_tabSignal_t)), m_getACE, SLOT(getSignalForACE(mls_tabSignal_t)));

    /* return the result  */
    connect(m_socket, SIGNAL(writeSuccess(mls_tabSend_t)), this, SLOT(writeSuccessfully(mls_tabSend_t)));

    /* error in connecting to server  */
    connect(m_socket, SIGNAL(errorProcess(QString&,mls_tabSend_t)), this, SLOT(errorTCP(QString&,mls_tabSend_t)));

    connect(this, SIGNAL(startToSendTCP()), m_socket, SLOT(startConnecting()));

}

MainWindow::~MainWindow()
{
    delete mValidDDOL;
    delete mValidTDOL;
    delete m_readXML;
    delete ui;
}


void MainWindow::resizeEvent(QResizeEvent *event)
{
    // initialize window and tabwidget
    ui->label_Background->resize(ui->tabWidget->width(), ui->tabWidget->height());
    QPixmap p(":/image/image/background1.jpg");
    ui->label_Background->setPixmap(p.scaled(ui->label_Background->width(), ui->label_Background->height(), Qt::IgnoreAspectRatio));

}

void MainWindow::paintEvent(QPaintEvent *event)
{

}

/**
 * @brief MainWindow::GroupButtonClick
 * show the appropriate tab with the button.
 * @param Index
 */
void MainWindow::GroupButtonClick(int Index)
{
    ui->tabWidget->setCurrentIndex(Index);
    group->button(Index)->setStyleSheet("QToolButton{border:1px solid gray; background-color: rgb(240, 240, 240)}");
    for (int i =0; i< Count ; i++){
        if (i != Index){
            group->button(i)->setStyleSheet("QToolButton{border:none}");
        }
    }
}

/**
 * @brief MainWindow::Additional_Update
 * update additional lineEdit in terminal configuration tab.
 * @param text
 */
void MainWindow::Additional_Update(const QString &text)
{
    ui->lineEditAddition->setText(text);
}
/**
 * @brief MainWindow::Terminal_Update
 * update terminal capabilities in terminal tab.
 * @param text
 */
void MainWindow::Terminal_Update(const QString &text)
{
    ui->lineEditTerminal->setText(text);
}

/**
 * @brief MainWindow::Default_Update
 * if lineEditChosed is in processing tab, update database, set text for lineEdit in processing tab.
 * if lineEditChosed in terminal tab, set text for lineEdit in terminal tab.
 * @param text
 */
void MainWindow::Default_Update(const QString &text)
{  // use a new window for three buttons: default button, online button,
    quint8 index = ui->listWidgetProcessing->currentRow();
    quint32 length;
    quint8 **database;
    quint32 lengthOfByteLineEditTAC, lengthTagAndLengthOfTAC;
    bool ok;

    // create a two dimensional array
    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    switch (LineEditChosed){
     case mproTACDef:
        ui->lineEditDefaultProcessing->setText(text);

        /* init two dimension database to store length and value of each tag to database */
        initializeBufferDatabase(database);

        /* store all value in the index row of m_databaseArray into database. but follows
         the order of each tag in the table in the document */

        checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                                  database,QString());

        /* update the value in TACDenial row of array database*/
         lengthOfByteLineEditTAC = 5, lengthTagAndLengthOfTAC = 3;
        for (quint32 i =0; i<lengthOfByteLineEditTAC; i++)
        {
            database[ptTACDenial][lengthTagAndLengthOfTAC+i] = (quint8)ui->lineEditDefaultProcessing->text()
                    .mid(2*i,2).toInt(&ok, 16);
        }
        /* store lastest database into the index row of database : m_databaseArray */
        storeTagToDatabaseProcess(m_databaseArray, index,database, &length);

        /*update the length of current row of the m_databaseArray */
        m_lengthOfEachItemListWidgetProcessing[index] = length;

        ui->ButtonSaveProcessing->setEnabled(true);
        break;
        case mproTACOnl:
        ui->lineEditOnlineProcessing->setText(text);

        initializeBufferDatabase(database);

        checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                                  database,QString());
        for (quint32 i =0; i<5; i++)
        {
            database[ptTACDenial][3+i] = (quint8)ui->lineEditOnlineProcessing->text().mid(2*i,2).toInt(&ok, 16);
        }

        storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
        m_lengthOfEachItemListWidgetProcessing[index] = length;
        ui->ButtonSaveProcessing->setEnabled(true);
        break;
     case mproTACDen:
        ui->lineEditDenialProcessing->setText(text);

        initializeBufferDatabase(database);
        checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                                  database,QString());
        for (quint32 i =0; i<5; i++)
        {
            database[ptTACDenial][3+i] = (quint8)ui->lineEditDenialProcessing->text().mid(2*i,2).toInt(&ok, 16);
        }

        storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
        m_lengthOfEachItemListWidgetProcessing[index] = length;
        ui->ButtonSaveProcessing->setEnabled(true);
        break;
     case mterTACDef:
        ui->lineEditDefault->setText(text);
        break;
    case mterTACDen:
        ui->lineEditDenial->setText(text);
        break;
    case mterTACOnl:
        ui->lineEditOnline->setText(text);
        break;
    }

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;


}

/**
 * @brief MainWindow::OpenNewWindow
 * In EntryPoint tab, when click button in the LineEdit.
 * if the Kernel Id is MasterCard, show PayPass.
 * @param Index
 */
void MainWindow::OpenNewWindow(int Index)
{
    QString stringKerID = listComboKernelID[Index]->currentText();
    uint8_t bufferTemp[200];
    uint16_t length =0;

     if (stringKerID == "MasterCard"){
         /* init bufferTemp */
        for (int i =0; i< 200; i++){
            bufferTemp[i] =0;
        }

        /* convert text in LineEdit to array of quint8 bufferTemp */
        length = CheckTag(ListLineEdit.at(Index)->text(), bufferTemp);
        /* update bufferTemp to payPass class */
        PayPass->updateBuffer(length, bufferTemp);
        /* index is the order of lineEdit in EntryPoint tab */
        emit UpdatePaypass(Index);
        PayPass->show();
    }

}

/**
 * @brief MainWindow::updateLineEditEntryPointConfig
 * update the lineEdit of the index position in table widget.
 * @param index
 * @param text
 */
void MainWindow::updateLineEditEntryPointConfig(qint32 index, QString text)
{
    ListLineEdit.at(index)->setText(text);
}

void MainWindow::listWidgetItemChanged()
{

}

/**
 * @brief MainWindow::aceExitClicked
 * close the app
 */
void MainWindow::aceExitClicked()
{
    this->close();
}
/**
 * @brief MainWindow::closeAdmin
 * @param text
 */
void MainWindow::closeAdmin(const QString &text)
{
    m_textAdmin = text;
}

/**
 * @brief MainWindow::comboEntryPointChanged
 * enable entryPoint send button.
 * @param id
 */
void MainWindow::comboEntryPointChanged(int id)
{
    ui->ButtonEntrySave->setEnabled(true);
}

/**
 * @brief MainWindow::lineEditEntryPointChanged
 */
void MainWindow::lineEditEntryPointChanged()
{
    ui->ButtonEntrySave->setEnabled(true);
}

/**
 * @brief MainWindow::getResultTDOL
 * check validator of text in TextEdit, it accepts hex number.
 * @param a
 */
void MainWindow::getResultTDOL(bool a)
{
    /* if check the text in textEdit doesn't follow the hex number */
    if (!a)
    {
        QString tmpString = ui->textEdit_TDOL->toPlainText();
         // delete the character that is just typed.
        tmpString.remove(tmpString.length()-1, 1);

        QTextCursor tmpCursor;

        tmpCursor = ui->textEdit_TDOL->textCursor();
        tmpCursor.movePosition(QTextCursor::Right, QTextCursor::MoveAnchor, tmpString.length());
        ui->textEdit_TDOL->setPlainText(tmpString);
        ui->textEdit_TDOL->setTextCursor(tmpCursor);
    }

    quint8 index = ui->listWidgetProcessing->currentRow();
    quint8 **database;
    quint32 length, numberOfBytes;
    bool ok;

    if (ui->textEdit_TDOL->toPlainText().length() %2 == 0)
    {
          /* create two dimension array */
        database = new quint8*[MAX_ROW];
        for (quint32 i =0; i< MAX_ROW; i++)
        {
            database[i] = new quint8[MAX_COL];
        }

        ui->textEdit_TDOL->setStyleSheet("color: black; border:1px solid black");

          /* get length of the textEdit according to byte */
        length = ui->textEdit_TDOL->toPlainText().length()/2;
        if (length >252) length = 252;

        initializeBufferDatabase(database);

         /* split value of row "index" of m_databaseArray to each tlv and store into database */
        checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());
        /* according to the basic rule in document, */
        if (length > 127)
        {
            database[ptTDOL][2] = 0x81;
            database[ptTDOL][3] = (quint8)length;
            numberOfBytes = 4;
        }
        else
        {
            database[ptTDOL][2] = (quint8)length;
            numberOfBytes = 3;
        }
        /* update "ptTDOL" row of database */
        for (quint32 i =0; i<length; i++)
        {
            database[ptTDOL][numberOfBytes+i] = (quint8)(ui->textEdit_TDOL->toPlainText().mid(2*i,2)
                                                         .toInt(&ok, 16));
        }

        /*store the lastest value to row "index" of m_databaseArray */
        storeTagToDatabaseProcess(m_databaseArray, index,database,&length);

        m_lengthOfEachItemListWidgetProcessing[index] = length;

        for (quint32 i =0; i< MAX_ROW; i++)
        {
            delete[] database[i];
        }
        delete[] database;
        ui->ButtonSaveProcessing->setEnabled(true);
    }
    else
    {
        ui->textEdit_TDOL->setStyleSheet("color:red; border: 1px solid black;");
    }

}

/**
 * @brief MainWindow::getResultDDOL
 * it is kind of setting validator for textEdit.
 * the textEdit allows hex number.
 * @param a
 */
void MainWindow::getResultDDOL(bool a)
{
    if (!a)
    {
        QString tmpString = ui->textEdit_DDOL->toPlainText();
        // delete the character that is just typed.
        tmpString.remove(tmpString.length()-1, 1);

        // move text cursor.
        QTextCursor tmpCursor;
        tmpCursor = ui->textEdit_DDOL->textCursor();
        tmpCursor.movePosition(QTextCursor::Right, QTextCursor::MoveAnchor, tmpString.length());
        ui->textEdit_DDOL->setPlainText(tmpString);
        ui->textEdit_DDOL->setTextCursor(tmpCursor);

     }

    quint8 index = ui->listWidgetProcessing->currentRow();
    quint8 **database;
    quint8 length, numberOfBytes;
    quint32 lengthTLV;
    bool ok;

    if (ui->textEdit_DDOL->toPlainText().length() %2 == 0)
    {
        /* create two dimension array */
        database = new quint8*[MAX_ROW];
        for (quint32 i =0; i< MAX_ROW; i++)
        {
            database[i] = new quint8[MAX_COL];
        }
        ui->textEdit_DDOL->setStyleSheet("color: black; border:1px solid black");

        /* get length of the textEdit according to byte */
        length = ui->textEdit_DDOL->toPlainText().length()/2;
        if (length >252) length = 252;

        /* init database */
        initializeBufferDatabase(database);

        /* split value of row "index" of m_databaseArray to each tlv and store into database */
        checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

        /* update database */
        if (length > 127)
        {
            database[ptDDOL][2] = 0x81;
            database[ptDDOL][3] = (quint8)length;
            numberOfBytes = 4;
        }
        else
        {
            database[ptDDOL][2] = (quint8)length;
            numberOfBytes = 3;
        }

        for (quint32 i =0; i<length; i++)
        {
            database[ptDDOL][numberOfBytes+i] = (quint8)(ui->textEdit_DDOL->toPlainText()
                                                         .mid(2*i,2).toInt(&ok, 16));
        }

        /*store the lastest value to row "index" of m_databaseArray */
        storeTagToDatabaseProcess(m_databaseArray, index,database, &lengthTLV);
        m_lengthOfEachItemListWidgetProcessing[index] = lengthTLV;
        for (quint32 i =0; i< MAX_ROW; i++)
        {
            delete[] database[i];
        }
        delete[] database;
        ui->ButtonSaveProcessing->setEnabled(true);

    }
    else
    {
        ui->textEdit_DDOL->setStyleSheet("color:red; border: 1px solid black;");
    }
}

/**
 * @brief MainWindow::errorTCP
 * if there are some error while sending data to server, show a warning dialog to inform user.
 * @param err
 * @param ithTab
 */
void MainWindow::errorTCP(QString &err, mls_tabSend_t ithTab)
{
    QString tabStr;

    switch (ithTab)
    {
        case tsTabTerminal:
            tabStr = "TERMINAL";
            break;
        case tsTabProcessing:
            tabStr = "PROCESSING";
            break;
        case tsTabEntryPoint:
            tabStr = "ENTRYPOINT";
            break;
    }

    if (ithTab == tsWindowTabCA)
    {
        QMessageBox::warning(this, "CA Keys Upload ", "CA Keys not sent");
    }
    else if (ithTab == tsWindowTabRevocatedCA)
    {
        QMessageBox::warning(this, "Revocated CA Keys Upload", "Revocated CA Keys not sent");
    }
    else
    {
        QMessageBox::warning(this, "Network error", "Failed to send " + tabStr +
                         "configuration to acceptance system");
    }
}

/**
 * @brief MainWindow::writeSuccessfully
 * if data is sent to server successfully, show a dialog to inform user
 * @param ithTab
 */
void MainWindow::writeSuccessfully(mls_tabSend_t ithTab)
{
    QString tabStr;

    switch (ithTab)
    {
        case tsTabTerminal:
            tabStr = "TERMINAL";
            break;
        case tsTabProcessing:
            tabStr = "PROCESSING";
            break;
        case tsTabEntryPoint:
            tabStr = "ENTRYPOINT";
            break;

    }

    if (ithTab == tsWindowTabCA)
    {
        QMessageBox::information(this, "title", "CA key sent");
    }
    else if (ithTab == tsWindowTabRevocatedCA)
    {
        QMessageBox::information(this, "title", "Revocated CA key sent");
    }
    else
    {
        QMessageBox::information(this, "title", "successfully send " + tabStr +" configuration to acceptance system!");
    }
}

/**
 * @brief MainWindow::on_toolButtonAddition_clicked
 * if the button Additional Terminal Capabilities is pressed, AddTerm dialog shows.
 */
void MainWindow::on_toolButtonAddition_clicked()
{
    AddTerm->show();
    emit ButtonAddition_Clicked(ui->lineEditAddition->text(), waMainWindow);
}

/**
 * @brief MainWindow::on_toolButtonDefault_clicked
 * if button default, denial, online in terminal configuration tab is pressed,
 * def shows
 */
void MainWindow::on_toolButtonDefault_clicked()
{
    Def->show();
    LineEditChosed = mterTACDen;
    emit ButtonDefault_Clicked(ui->lineEditDenial->text(), wdMainWindow);
}

void MainWindow::on_toolButtonOnline_clicked()
{
    Def->show();
    LineEditChosed = mterTACDef;
    emit ButtonOnline_Clicked(ui->lineEditDefault->text(), wdMainWindow);
}

void MainWindow::on_toolButton_7_clicked()
{
    Def->show();
    LineEditChosed = mterTACOnl;
    emit Button_7_Clicked(ui->lineEditOnline->text(), wdMainWindow);
}

/**
 * @brief MainWindow::on_checkBoxDefault_clicked
 * checkbox DefaultTAC in Terminal configuration is checked.
 */
void MainWindow::on_checkBoxDefault_clicked()
{
    bool State =false;
    qDebug() << "here";
    State = ui->checkBoxDefault->checkState();
    ui->toolButtonDefault->setEnabled(State);
    ui->lineEditDefault->setEnabled(State);
    ui->toolButtonOnline->setEnabled(State);
    ui->lineEditOnline->setEnabled(State);
    ui->toolButton_7->setEnabled(State);
    ui->lineEditDenial->setEnabled(State);
}
/**
 * @brief MainWindow::on_toolButtonTerminal_clicked
 * terminal capabilities button is clicked.
 */
void MainWindow::on_toolButtonTerminal_clicked()
{
    TermCap->show();
    emit ButtonTerminal_Clicked(ui->lineEditTerminal->text());
}

/**
 * @brief MainWindow::initializeTerminalConfiguration
 * set tooltip, icon, stylesheet for all the button in terminal tab.
 * set tooltip for all the widget in terminal tab.
 *
 */
void MainWindow::initializeTerminalConfiguration()
{
    // add button open
    ui->ButtonOpen->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonOpen->setFixedSize(QSize(65, 80));
    ui->ButtonOpen->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->ButtonOpen->setIconSize(QSize(50, 60));
    ui->ButtonOpen->setText("Open");
    ui->ButtonOpen->setToolTip("Open");
    ui->ButtonOpen->setAutoRaise(true);

    // add button save
    ui->ButtonSave->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSave->setFixedSize(QSize(65, 80));
    ui->ButtonSave->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonSave->setIconSize(QSize(50, 60));
    ui->ButtonSave->setText("Save");
    ui->ButtonSave->setToolTip("Save");
    ui->ButtonSave->setEnabled(false);
    ui->ButtonSave->setAutoRaise(true);

    // add button save as
    ui->ButtonSaveAs->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSaveAs->setFixedSize(QSize(65, 80));
    ui->ButtonSaveAs->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonSaveAs->setIconSize(QSize(50, 60));
    ui->ButtonSaveAs->setText("Save As");
    ui->ButtonSaveAs->setToolTip("Save As");
    ui->ButtonSaveAs->setAutoRaise(true);

    // add button send
    ui->ButtonSend->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSend->setFixedSize(QSize(65, 80));
    ui->ButtonSend->setIcon(QIcon(":/image_open_save/image/uparrow.png"));
    ui->ButtonSend->setIconSize(QSize(50, 60));
    ui->ButtonSend->setText("Send");
    ui->ButtonSend->setToolTip("Send");
    ui->ButtonSend->setAutoRaise(true);

        // combo box of terminal type
    QStringList ListTerminal;

    ListTerminal << "00" << "11" << "12" <<
                    "13" << "14" << "15" << "16" << "21" << "22" << "23" <<
                    "24" << "25" << "26" << "34" << "35" << "36";

    for (int i =0; i <ListTerminal.count(); i++){
        ui->comboBoxTerminal->addItem(ListTerminal.at(i));
    }
    ui->comboBoxTerminal->setStyleSheet("border:1px solid black");

    /* comboBox of CDA Type */
    QList<QString> ListCDA;
    ListCDA <<"UNDEFINED" << "MODE_1" << "MODE_2" << "MODE_3" << "MODE_4";
    for (int i =0; i < ListCDA.count(); i++){
    ui->comboBoxCDA->addItem(ListCDA.at(i));
    }
    ui->comboBoxCDA->setStyleSheet("border:1px solid black");
    // insert button to line edit Terminal Capabilities, Additional Terminal

    // button terminal capabilities
    ui->toolButtonTerminal->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonTerminal->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonTerminal->setIconSize(QSize(ui->toolButtonTerminal->width(), ui->toolButtonTerminal->height()));
    ui->toolButtonTerminal->setToolTip("Tag 9F33");

     int frameWidth;
    //line edit terminal capabilitie
    frameWidth = ui->lineEditTerminal->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditTerminal->setStyleSheet(QString("QLineEdit {border: 1px solid black;"
                                                "border-radius:3px;padding-right: %1px; } ")
                                        .arg(ui->toolButtonTerminal->sizeHint().width() + frameWidth + 3));
    ui->lineEditTerminal->setMaxLength(6);
    ui->lineEditTerminal->setText("000000");
    ui->lineEditTerminal->setInputMask("hhhhhh");
    TermCap->TextLineEdit = "000000";
    ui->lineEditTerminal->setAlignment(Qt::AlignRight);
    ui->lineEditTerminal->setToolTip("Tag 9F33");

    // button additional
    ui->toolButtonAddition->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonAddition->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonAddition->setIconSize(QSize(ui->toolButtonAddition->width(), ui->toolButtonAddition->height()));
    ui->toolButtonAddition->setToolTip("Tag 9F40");

    // line edit additional
    frameWidth = ui->lineEditAddition->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditAddition->setStyleSheet(QString("QLineEdit { border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButtonAddition->sizeHint().width() + frameWidth + 3));
    ui->lineEditAddition->setMaxLength(10);
    ui->lineEditAddition->setText("0000000000");
    ui->lineEditAddition->setInputMask("hhhhhhhhhh");
    ui->lineEditAddition->setAlignment(Qt::AlignRight);
    ui->lineEditAddition->setToolTip("Tag 9F40");

    // button default
    ui->toolButtonDefault->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonDefault->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonDefault->setIconSize(QSize(ui->toolButtonDefault->width(), ui->toolButtonDefault->height()));
    ui->toolButtonDefault->setToolTip("Tag DF74");

    // line edit default
    frameWidth = ui->lineEditDefault->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditDefault->setStyleSheet(QString("QLineEdit { border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButtonDefault->sizeHint().width() + frameWidth + 3));
    ui->lineEditDefault->setMaxLength(10);;
    ui->lineEditDefault->setText("0000000000");
    ui->lineEditDefault->setInputMask("hhhhhhhhhh");
    ui->lineEditDefault->setAlignment(Qt::AlignRight);
    ui->lineEditDefault->setToolTip("Tag DF74");

    // button online
    ui->toolButtonOnline->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonOnline->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonOnline->setIconSize(QSize(ui->toolButtonOnline->width(), ui->toolButtonOnline->height()));
    ui->toolButtonOnline->setToolTip("Tag DF73");

    // line edit online
    frameWidth = ui->lineEditOnline->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditOnline->setStyleSheet(QString("QLineEdit { border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButtonOnline->sizeHint().width() + frameWidth + 3));
    ui->lineEditOnline->setMaxLength(10);
    ui->lineEditOnline->setText("0000000000");
    ui->lineEditOnline->setInputMask("hhhhhhhhhh");
    ui->lineEditOnline->setAlignment(Qt::AlignRight);
    ui->lineEditOnline->setToolTip("Tag DF73");

    // button 7
    ui->toolButton_7->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButton_7->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButton_7->setIconSize(QSize(ui->toolButton_7->width(), ui->toolButton_7->height()));
    ui->toolButton_7->setToolTip("Tag DF75");

    // line edit 7
    frameWidth = ui->lineEditDenial->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditDenial->setStyleSheet(QString("QLineEdit { border: 1px solid black;border-radius:3px;padding-right: %1px; } ").arg(ui->toolButton_7->sizeHint().width() + frameWidth + 3));
    ui->lineEditDenial->setMaxLength(10);
    ui->lineEditDenial->setText("0000000000");
    ui->lineEditDenial->setInputMask("hhhhhhhhhh");
    ui->lineEditDenial->setAlignment(Qt::AlignRight);
    ui->lineEditDenial->setToolTip("Tag DF75");

    // line country code and Pintimeout
    ui->lineEditTerCounCode->setInputMask("hhhh");
    ui->lineEditTerCounCode->setMaxLength(4);
    ui->lineEditTerCounCode->setText("0000");
    ui->lineEditTerCounCode->setToolTip("Tag 9F1A");
    ui->lineEditPINTimeout->setInputMask("99");
    ui->lineEditPINTimeout->setMaxLength(2);
    ui->lineEditPINTimeout->setText("00");
    ui->lineEditPINTimeout->setToolTip("Tag DF27");

    ui->toolButtonDefault->setEnabled(false);
    ui->lineEditDefault->setEnabled(false);
    ui->toolButtonOnline->setEnabled(false);
    ui->lineEditOnline->setEnabled(false);
    ui->toolButton_7->setEnabled(false);
    ui->lineEditDenial->setEnabled(false);

    m_listWid << ui->lineEditTerCounCode << ui->checkBoxCardHolder << ui->comboBoxTerminal << ui->checkBoxEMV1
            << ui->lineEditTerminal << ui->lineEditAddition << ui->checkBoxEMV2 << ui->checkBoxMastripe
            << ui->lineEditPINTimeout << ui->checkBoxOnline << ui->checkBoxAdvice << ui->checkBoxPSE
            << ui->checkBoxAutorun << ui->spinBox << ui->checkBoxPinBypass << ui->checkBoxReferral
            << ui->checkBoxDefault << ui->lineEditDefault << ui->lineEditDenial << ui->lineEditOnline
            << ui->checkBoxRTSNot << ui->checkBoxVelocity << ui->comboBoxCDA;
  /* list all widget in terminal configuration tab */
    m_typeWidgetTer[0] = twLineEdit; m_typeWidgetTer[1] = twCheckBox; m_typeWidgetTer[2] = twComboBox;
    m_typeWidgetTer[3] = twCheckBox; m_typeWidgetTer[4] = twLineEdit; m_typeWidgetTer[5] = twLineEdit;
    m_typeWidgetTer[6] = twCheckBox; m_typeWidgetTer[7] = twCheckBox; m_typeWidgetTer[8] = twLineEdit;
    m_typeWidgetTer[9] = twCheckBox; m_typeWidgetTer[10] = twCheckBox; m_typeWidgetTer[11] = twCheckBox;
    m_typeWidgetTer[12] = twCheckBox; m_typeWidgetTer[13] = twSpinBox; m_typeWidgetTer[14] = twCheckBox;
    m_typeWidgetTer[15] = twCheckBox; m_typeWidgetTer[16] = twCheckBox; m_typeWidgetTer[17] = twLineEdit;
    m_typeWidgetTer[18] = twLineEdit; m_typeWidgetTer[19] = twLineEdit; m_typeWidgetTer[20] = twCheckBox;
    m_typeWidgetTer[21] = twCheckBox; m_typeWidgetTer[22] = twComboBox;

    ui->comboBoxTerminal->setToolTip("Tag 9F35");
    ui->label_TerType->setToolTip("Tag 9F35");
    ui->label_TerCap->setToolTip("Tag 9F33");
    ui->label_AddTerCap->setToolTip("Tag 9F40");
    ui->label_TerCounCode->setToolTip("Tag 9F1A");
    ui->label_PINTime->setToolTip("Tag DF27");
    ui->checkBoxEMV1->setToolTip("Tag DF0A");
    ui->checkBoxDefault->setToolTip("Tag DF09");
    ui->label_Denial_2->setToolTip("Tag DF74");
    ui->label_Def->setToolTip("Tag DF73");
    ui->label_Onl->setToolTip("Tag DF75");
    ui->label_CDAType->setToolTip("Tag DF7C");
    ui->comboBoxCDA->setToolTip("Tag DF7C");
    ui->checkBoxVelocity->setToolTip("Tag DF54");
    ui->checkBoxRTSNot->setToolTip("Tag DF53");
    ui->checkBoxPinBypass->setToolTip("Tag DF7B");
    ui->checkBoxPSE->setToolTip("Tag DF7A");
    ui->checkBoxCardHolder->setToolTip("Tag DF79");
    ui->checkBoxOnline->setToolTip("Tag DF06");
    ui->checkBoxReferral->setToolTip("Tag DF07");
    ui->checkBoxAdvice->setToolTip("Tag DF08");
    ui->checkBoxEMV2->setToolTip("Tag DF55");
    ui->checkBoxAutorun->setToolTip("Tag DF0D");
    ui->label_Predefine->setToolTip("Tag DF10");
    ui->spinBox->setToolTip("Tag DF10");
    ui->spinBox->setEnabled(false);

    /* list of tag in terminal configuration tab */
    m_tagConfiguration << "9F1A" << "DF79" << "9F35" << "DF0A" << "9F33" << "9F40" << "DF55"
                       << "DF0B" << "DF27" << "DF06" << "DF08" << "DF7A" << "DF0D" << "DF10"
                       << "DF7B" << "DF07" << "DF09" << "DF73" << "DF74" << "DF75" << "DF53"
                       << "DF54" << "DF7C";

    // Button Terminal Capabilities is clicked, open new window
    connect(this, SIGNAL(ButtonTerminal_Clicked(QString)), TermCap, SLOT(ChangeTextEdit(QString)));
    // Ok button in new additional window is clicked, update text in additional
    connect(AddTerm, SIGNAL(ButtonOk_MainWindow_Clicked(QString)), this, SLOT(Additional_Update(QString)));

    // Ok button in new terminal window is clicked, update text in terminal capabilities
      connect(TermCap, SIGNAL(ButtonOk_Clicked(QString)), this, SLOT(Terminal_Update(QString)));
}

/**
 * @brief MainWindow::initializeMainWindow
 * set tooltip, stylesheet, icon for all the button in Tool Bar of app.
 */
void MainWindow::initializeMainWindow()
{
    // create a tool button welcome
        ButtonWelcome->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 32));
        ButtonWelcome->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonWelcome->setFixedSize(QSize(80, 80));
        ButtonWelcome->setIcon(QIcon(":/image/image/welcome.png"));
        ButtonWelcome->setIconSize(QSize(50, 70));
        ButtonWelcome->setText("Welcome");
        ButtonWelcome->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonWelcome->setToolTip("Welcome");

        // create a tool button configuration
        ButtonConfiguration->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, ButtonWelcome->height()) + QPoint(0, 32));
        ButtonConfiguration->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonConfiguration->setFixedSize(QSize(80, 80));
        ButtonConfiguration->setIcon(QIcon(":/image/image/configuration.png"));
        ButtonConfiguration->setIconSize(QSize(50, 70));
        ButtonConfiguration->setText("Configuration");
        ButtonConfiguration->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonConfiguration->setToolTip("Configuration");

        // create a tool button transaction
        ButtonTransaction->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 2*ButtonConfiguration->height()) + QPoint(0, 32));
        ButtonTransaction->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonTransaction->setFixedSize(QSize(80, 80));
        ButtonTransaction->setIcon(QIcon(":/image/image/transaction.png"));
        ButtonTransaction->setIconSize(QSize(50, 70));
        ButtonTransaction->setText("Transaction");
        ButtonTransaction->setStyleSheet("QToolButton{border:none} QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonTransaction->setToolTip("Display transaction page");

        // creat a bitmap toolbutton
        ButtonBitmap->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 3*ButtonTransaction->height()) + QPoint(0, 32));
        ButtonBitmap->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonBitmap->setFixedSize(QSize(80, 80));
        ButtonBitmap->setIcon(QIcon(":/image/image/bitmap.png"));
        ButtonBitmap->setIconSize(QSize(50, 70));
        ButtonBitmap->setText("Bitmap");
        ButtonBitmap->setStyleSheet("QToolButton{border: none} QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonBitmap->setToolTip("Display bitmap page");

        // create a tools button
        ButtonTool->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 4*ButtonBitmap->height()) + QPoint(0, 32));
        ButtonTool->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonTool->setFixedSize(QSize(80, 80));
        ButtonTool->setIcon(QIcon(":/image/image/tool.jpg"));
        ButtonTool->setIconSize(QSize(50, 70));
        ButtonTool->setText("Tools");
        ButtonTool->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonTool->setToolTip("Display tool page");

        // create terminal button
        ButtonTerminal->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 5*ButtonTool->height()) + QPoint(0, 32));
        ButtonTerminal->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonTerminal->setFixedSize(QSize(80, 80));
        ButtonTerminal->setIcon(QIcon(":/image/image/terminal.png"));
        ButtonTerminal->setIconSize(QSize(50, 60));
        ButtonTerminal->setText("Terminal");
        ButtonTerminal->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonTerminal->setToolTip("Terminal UI");

        // create setup button
        ButtonSetup->move(ui->tab0->mapFrom(ui->tabWidget, ui->tabWidget->pos()) + QPoint(0, 6*ButtonTerminal->height()) + QPoint(0, 35));
        ButtonSetup->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
        ButtonSetup->setFixedSize(QSize(80, 80));
        ButtonSetup->setIcon(QIcon(":/image/image/setup.jpg"));
        ButtonSetup->setIconSize(QSize(50, 70));
        ButtonSetup->setText("Setup");
        ButtonSetup->setStyleSheet("QToolButton{border: none}QToolButton:hover{background-color: rgb(245,245,245)}");
        ButtonSetup->setToolTip("Display setup page");
}

/**
 * @brief MainWindow::initializeProcessingConfiguration
 * set tooltip, icon, stylesheet for all the button in processing tab.
 * init all the widget in processing tab.
 */
void MainWindow::initializeProcessingConfiguration()
{
    m_numItemsProcess = 0;

    QRegExp r("[0-9]{0,6}");
    QValidator *valid = new QRegExpValidator(r, this);
    ui->lineEditProcessingTCC->setValidator(valid);
    ui->lineEditProcessingExpo->setValidator(valid);
    ui->lineEditProcessingExpo->setMaxLength(3);
    ui->lineEditProcessingTCC->setMaxLength(4);

    /* all the important widget in tab */
    m_listWidProcess << ui->lineEditAcquireIDProcessing << ui->checkBox_ASIProcess
                     << ui->lineEditAVNProcessing << ui->checkBox_SkipTACProcess << ui->checkBox_RTSProcess << ui->checkBox_VelocityProcess
                     << ui->checkBox_FloorLimitProcess << ui->checkBox_TACProcess << ui->lineEditDenialProcessing
                     << ui->lineEditOnlineProcessing << ui->lineEditDefaultProcessing << ui->lineEditProcessingFloor
                     << ui->spinBox_TargetProcess << ui->lineEditProcessingThresh << ui->spinBox_MaxTargetProcess
                     << ui->checkBox_DefaultDDOL << ui->textEdit_DDOL << ui->checkBox_DefaultTDOL
                     << ui->textEdit_TDOL << ui->lineEditProcessingTCC << ui->lineEditProcessingExpo;

    /* init type of each widget, checkbox, lineEdit or textEdit */
    m_typeWidgetProcessing[0] = twLineEdit; m_typeWidgetProcessing[1] = twCheckBox;
    m_typeWidgetProcessing[2] = twLineEdit;
    m_typeWidgetProcessing[3] = twCheckBox; m_typeWidgetProcessing[4] = twCheckBox;
    m_typeWidgetProcessing[5] = twCheckBox ; m_typeWidgetProcessing[6] = twCheckBox;
    m_typeWidgetProcessing[7] = twCheckBox ;m_typeWidgetProcessing[8] = twLineEdit;
    m_typeWidgetProcessing[9] = twLineEdit; m_typeWidgetProcessing[10] = twLineEdit;
    m_typeWidgetProcessing[11] = twLineEdit; m_typeWidgetProcessing[12] = twSpinBox;
    m_typeWidgetProcessing[13] = twLineEdit; m_typeWidgetProcessing[14] = twSpinBox;
    m_typeWidgetProcessing[15] = twCheckBox; m_typeWidgetProcessing[16] = twTextEdit;
    m_typeWidgetProcessing[17] = twCheckBox; m_typeWidgetProcessing[18] = twTextEdit;
    m_typeWidgetProcessing[19] = twLineEdit; m_typeWidgetProcessing[20] = twLineEdit ;

    /* init all the state of tag In processing tab is false,
      all of checkboxes is unchecked, lineEdits are not filled, */
    for (quint32 i =0; i< 21; i++)
    {
        m_stateTagProcessing[i] = false;
    }
    // add button open
    ui->ButtonOpenProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonOpenProcessing->setFixedSize(QSize(65, 80));
    ui->ButtonOpenProcessing->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->ButtonOpenProcessing->setIconSize(QSize(50, 60));
    ui->ButtonOpenProcessing->setText("Open");
    ui->ButtonOpenProcessing->setToolTip("open configuration");
    ui->ButtonOpenProcessing->setAutoRaise(true);
    ui->ButtonOpenProcessing->setStyleSheet(":hover");

    // add button save
    ui->ButtonSaveProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSaveProcessing->setFixedSize(QSize(65, 80));
    ui->ButtonSaveProcessing->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonSaveProcessing->setIconSize(QSize(50, 60));
    ui->ButtonSaveProcessing->setText("Save");
    ui->ButtonSaveProcessing->setToolTip("save configuration");
    ui->ButtonSaveProcessing->setEnabled(false);
    ui->ButtonSaveProcessing->setAutoRaise(true);

    // add button save as
    ui->ButtonSaveAsProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSaveAsProcessing->setFixedSize(QSize(65, 80));
    ui->ButtonSaveAsProcessing->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonSaveAsProcessing->setIconSize(QSize(50, 60));
    ui->ButtonSaveAsProcessing->setText("Save As");
    ui->ButtonSaveAsProcessing->setToolTip("Save As configuration");
    ui->ButtonSaveAsProcessing->setAutoRaise(true);

    // add button send
    ui->ButtonSendProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonSendProcessing->setFixedSize(QSize(65, 80));
    ui->ButtonSendProcessing->setIcon(QIcon(":/image_open_save/image/uparrow.png"));
    ui->ButtonSendProcessing->setIconSize(QSize(50, 60));
    ui->ButtonSendProcessing->setText("Send");
    ui->ButtonSendProcessing->setToolTip("Send configuration");
    ui->ButtonSendProcessing->setAutoRaise(true);

    // add button ACE
    ui->ButtonACEProcessing->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonACEProcessing->setFixedSize(QSize(65, 80));
    ui->ButtonACEProcessing->setIcon(QIcon(":/image_open_save/image/ace_icon.png"));
    ui->ButtonACEProcessing->setIconSize(QSize(50, 60));
    ui->ButtonACEProcessing->setText("ACE");
    ui->ButtonACEProcessing->setToolTip("Get processing from ACE");
    ui->ButtonACEProcessing->setAutoRaise(true);

    // add button plus
    ui->toolButtonAddProcessing->setFixedSize(QSize(31, 31));
    ui->toolButtonAddProcessing->setIcon(QIcon(":/image_open_save/image/plus1.png"));
    ui->toolButtonAddProcessing->setIconSize(QSize(30, 30));

    // add button minus
    ui->toolButtonRemoveProcessing->setFixedSize(QSize(31, 31));
    ui->toolButtonRemoveProcessing->setIcon(QIcon(":/image_open_save/image/minus1.png"));
    ui->toolButtonRemoveProcessing->setIconSize(QSize(30, 30));

    // add button up
    ui->toolButtonUpProcessing->setFixedSize(QSize(31, 31));
    ui->toolButtonUpProcessing->setIcon(QIcon(":/image_open_save/image/up1.png"));
    ui->toolButtonUpProcessing->setIconSize(QSize(30, 30));
    ui->toolButtonUpProcessing->setAutoRaise(true);

    // add button down
    ui->toolButtonDownProcessing->setFixedSize(QSize(31, 31));
    ui->toolButtonDownProcessing->setIcon(QIcon(":/image_open_save/image/down.png"));
    ui->toolButtonDownProcessing->setIconSize(QSize(30, 30));

    // button Denial
    ui->toolButtonDenialProcessing->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonDenialProcessing->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonDenialProcessing->setIconSize(QSize(ui->toolButtonDenialProcessing->width(),
                                                      ui->toolButtonDenialProcessing->height()));
    ui->toolButtonDenialProcessing->setToolTip("Tag DF21");

    // line edit Denial
    int frameWidth = ui->lineEditDenialProcessing->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditDenialProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid gray;"
                                                        "border-radius:3px; padding-right: %1px; } ")
                                                .arg(ui->toolButtonDenialProcessing->sizeHint().width()
                                                     + frameWidth + 3));
    ui->lineEditDenialProcessing->setMaxLength(10);
    ui->lineEditDenialProcessing->setText("0000000000");
    ui->lineEditDenialProcessing->setInputMask("hhhhhhhhhh");
    ui->lineEditDenialProcessing->setAlignment(Qt::AlignRight);

    // button Online
    ui->toolButtonOnlineProcessing->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonOnlineProcessing->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonOnlineProcessing->setIconSize(QSize(ui->toolButtonOnlineProcessing->width(), ui->toolButtonOnlineProcessing->height()));
    ui->toolButtonOnlineProcessing->setToolTip("Tag DF22");

    // line edit Online
    frameWidth = ui->lineEditOnlineProcessing->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditOnlineProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid gray;"
                                                        "border-radius:3px; padding-right: %1px; } ")
                                                .arg(ui->toolButtonDenialProcessing->sizeHint().width() +
                                                     frameWidth + 3));
    ui->lineEditOnlineProcessing->setMaxLength(10);
    ui->lineEditOnlineProcessing->setText("0000000000");
    ui->lineEditOnlineProcessing->setInputMask("hhhhhhhhhh");
    ui->lineEditOnlineProcessing->setAlignment(Qt::AlignRight);

    // button Default
    ui->toolButtonDefaultProcessing->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButtonDefaultProcessing->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButtonDefaultProcessing->setIconSize(QSize(ui->toolButtonDefaultProcessing->width(),
                                                       ui->toolButtonDefaultProcessing->height()));
    ui->toolButtonDefaultProcessing->setToolTip("Tag DF20");

    // line edit Default
    frameWidth = ui->lineEditDefaultProcessing->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    ui->lineEditDefaultProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid gray;"
                                                         "border-radius:3px;padding-right: %1px; } ")
                                                 .arg(ui->toolButtonDefaultProcessing->sizeHint().width() +
                                                      frameWidth + 3));
    ui->lineEditDefaultProcessing->setMaxLength(10);
    ui->lineEditDefaultProcessing->setText("0000000000");
    ui->lineEditDefaultProcessing->setInputMask("hhhhhhhhhh");
    ui->lineEditDefaultProcessing->setAlignment(Qt::AlignRight);

    ui->lineEditAcquireIDProcessing->setText("0000000000");
    ui->lineEditAcquireIDProcessing->setMaxLength(10);
    ui->lineEditAcquireIDProcessing->setInputMask("hhhhhhhhhh");
    ui->lineEditAcquireIDProcessing->setStyleSheet("border:1px solid gray; border-radius:3px");

    ui->lineEditAVNProcessing->setText("0000");
    ui->lineEditAVNProcessing->setInputMask("hhhh");
    ui->lineEditAVNProcessing->setStyleSheet("border:1px solid gray; border-radius:3px");

    ui->lineEditProcessingFloor->setMaxLength(8);
    ui->lineEditProcessingFloor->setStyleSheet("border:1px solid gray; border-radius:3px");
    ui->lineEditMainProcessing->setCursorPosition(0);

    ui->lineEditProcessingTCC->setStyleSheet("border:1px solid gray; border-radius:3px");
    ui->lineEditProcessingExpo->setStyleSheet("border:1px solid gray; border-radius:3px");
    ui->lineEditProcessingThresh->setStyleSheet("border:1px solid gray; border-radius:3px");

    /* set all the widget to be unable */
    for (int i =0; i < m_listWidProcess.length(); i++)
    {
        m_listWidProcess.at(i)->setEnabled(false);
        ui->toolButtonDefaultProcessing->setEnabled(false);
        ui->toolButtonDenialProcessing->setEnabled(false);
        ui->toolButtonOnlineProcessing->setEnabled(false);
        ui->listWidgetProcessing->clear();
        ui->label_Acquire->setEnabled(false);
        ui->label_AVN->setEnabled(false);
        ui->label_Denial->setEnabled(false);
        ui->label_Online->setEnabled(false);
        ui->label_Default->setEnabled(false);
        ui->label_Floor->setEnabled(false);
        ui->label_DDOL->setEnabled(false);
        ui->label_TDOL->setEnabled(false);
        ui->label_Target->setEnabled(false);
        ui->label_MaxTarget->setEnabled(false);
        ui->label_TCC->setEnabled(false);
        ui->label_Expo->setEnabled(false);
        ui->label_Thresh->setEnabled(false);
    }

    ui->lineEditDenialProcessing->setEnabled(false);
    ui->lineEditDefaultProcessing->setEnabled(false);
    ui->lineEditOnlineProcessing->setEnabled(false);
    ui->toolButtonDefaultProcessing->setEnabled(false);
    ui->toolButtonOnlineProcessing->setEnabled(false);
    ui->toolButtonDenialProcessing->setEnabled(false);

    /* set tool tip for all those widgets */
    ui->ButtonOpenProcessing->setToolTip("Open Configuration");
    ui->ButtonSaveProcessing->setToolTip("Save Configuration");
    ui->ButtonSaveAsProcessing->setToolTip("Save Configuration");
    ui->ButtonSendProcessing->setToolTip("Send Configuration");
    ui->lineEditMainProcessing->setToolTip("Tag 4F");
    ui->listWidgetProcessing->setToolTip("Tag 4F");
    ui->label_Acquire->setToolTip("Tag 9F01");
    ui->lineEditAcquireIDProcessing->setToolTip("Tag 9F01");
    ui->checkBox_ASIProcess->setToolTip("Tag DF7F");
    ui->label_AVN->setToolTip("Tag 9F09");
    ui->lineEditAVNProcessing->setToolTip("Tag 9F09");
    ui->checkBox_TACProcess->setToolTip("Tag DF15");
    ui->lineEditDenialProcessing->setToolTip("Tag DF21");
    ui->label_Online->setToolTip("Tag DF22");
    ui->lineEditOnlineProcessing->setToolTip("Tag DF22");
    ui->label_Default->setToolTip("Tag DF20");
    ui->lineEditDefaultProcessing->setToolTip("Tag DF20");
    ui->checkBox_SkipTACProcess->setToolTip("Tag DF11");
    ui->checkBox_FloorLimitProcess->setToolTip("Tag DF14");
    ui->label_Floor->setToolTip("Tag 9F1B");
    ui->lineEditProcessingFloor->setToolTip("Tag 9F1B");
    ui->checkBox_DefaultDDOL->setToolTip("Tag DF01 \n"
                                          "Default Dynamic Data Authentication "
                                          "Data Object List");
    ui->label_DDOL->setToolTip("Tag DF01 \n"
                                          "Default Dynamic Data Authentication "
                                          "Data Object List");
    ui->textEdit_DDOL->setToolTip("Tag DF71");

    ui->checkBox_DefaultTDOL->setToolTip("Tag DF02 \n"
                                         "Transaction Certificate Data Object List");
    ui->label_TDOL->setToolTip("Tag DF02 \n"
                                         "Transaction Certificate Data Object List");
    ui->textEdit_TDOL->setToolTip("Tag DF72");
    ui->checkBox_RTSProcess->setToolTip("Tag DF12 \n"
                                        "Random Transaction Selection");
    ui->checkBox_VelocityProcess->setToolTip("Tag DF13");
    ui->label_Target->setToolTip("Tag DF70");
    ui->spinBox_TargetProcess->setToolTip("Tag DF70");
    ui->label_MaxTarget->setToolTip("Tag DF6F");
    ui->spinBox_MaxTargetProcess->setToolTip("Tag DF6F");
    ui->label_TCC->setToolTip("Tag 5F2A \n"
                              "Transaction Currency Code");
    ui->lineEditProcessingTCC->setToolTip("Tag 5F2A");
    ui->label_Expo->setToolTip("Tag 5F36 \n"
                             "Transaction Exponent");
    ui->lineEditProcessingExpo->setToolTip("Tag 5F36");
    ui->label_Thresh->setToolTip("Tag DF6E");
    ui->lineEditProcessingThresh->setToolTip("Tag DF6E");

    /* check the text in textEdit TDOL or DDOL */
    connect(this, SIGNAL(sendStringToCheck(QString&)), mValidDDOL, SLOT(getStringToCheck(QString&)));

    connect(mValidDDOL, SIGNAL(result(bool)), this, SLOT(getResultDDOL(bool)));

    connect(this, SIGNAL(sendStringToCheck(QString&)), mValidTDOL, SLOT(getStringToCheck(QString&)));

    connect(mValidTDOL, SIGNAL(result(bool)), this, SLOT(getResultTDOL(bool)));
}

/**
 * @brief MainWindow::initializeEntryPoint
 * set tooltip, stylesheet, icon for all buttons in the tab.
 */
void MainWindow::initializeEntryPoint()
{
    int colCount = 4;
    int rowCount = 12;
    numRowTable = rowCount;

    // add button open
    ui->ButtonEntryOpen->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntryOpen->setFixedSize(QSize(65, 80));
    ui->ButtonEntryOpen->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->ButtonEntryOpen->setIconSize(QSize(50, 60));
    ui->ButtonEntryOpen->setText("Open");
    ui->ButtonEntryOpen->setToolTip("Open");
    ui->ButtonEntryOpen->setAutoRaise(true);

    // add button save
    ui->ButtonEntrySave->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntrySave->setFixedSize(QSize(65, 80));
    ui->ButtonEntrySave->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonEntrySave->setIconSize(QSize(50, 60));
    ui->ButtonEntrySave->setText("Save");
    ui->ButtonEntrySave->setToolTip("Save");
    ui->ButtonEntrySave->setEnabled(false);
    ui->ButtonEntrySave->setAutoRaise(true);

    // add button save as
    ui->ButtonEntrySaveAs->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntrySaveAs->setFixedSize(QSize(65, 80));
    ui->ButtonEntrySaveAs->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->ButtonEntrySaveAs->setIconSize(QSize(50, 60));
    ui->ButtonEntrySaveAs->setText("Save As");
    ui->ButtonEntrySaveAs->setToolTip("Save As");
    ui->ButtonEntrySaveAs->setAutoRaise(true);

    // add button send
    ui->ButtonEntrySend->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntrySend->setFixedSize(QSize(65, 80));
    ui->ButtonEntrySend->setIcon(QIcon(":/image_open_save/image/uparrow.png"));
    ui->ButtonEntrySend->setIconSize(QSize(50, 60));
    ui->ButtonEntrySend->setText("Send");
    ui->ButtonEntrySend->setToolTip("Send");
    ui->ButtonEntrySend->setAutoRaise(true);

    // add button ACE
    ui->ButtonEntryACE->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->ButtonEntryACE->setFixedSize(QSize(65, 80));
    ui->ButtonEntryACE->setIcon(QIcon(":/image_open_save/image/ace_icon.png"));
    ui->ButtonEntryACE->setIconSize(QSize(50, 60));
    ui->ButtonEntryACE->setText("ACE");
    ui->ButtonEntryACE->setToolTip("Get ENTRY POINT from ACE");
    ui->ButtonEntryACE->setAutoRaise(true);

    // add button plus
    ui->toolButtonEntryAdd->setFixedSize(QSize(31, 31));
    ui->toolButtonEntryAdd->setIcon(QIcon(":/image_open_save/image/plus1.png"));
    ui->toolButtonEntryAdd->setIconSize(QSize(30, 30));

    // add button minus
    ui->toolButtonEntryRemove->setFixedSize(QSize(31, 31));
    ui->toolButtonEntryRemove->setIcon(QIcon(":/image_open_save/image/minus1.png"));
    ui->toolButtonEntryRemove->setIconSize(QSize(30, 30));

    // add button up
    ui->toolButtonEntryUp->setFixedSize(QSize(31, 31));
    ui->toolButtonEntryUp->setIcon(QIcon(":/image_open_save/image/up1.png"));
    ui->toolButtonEntryUp->setIconSize(QSize(30, 30));

    // add button down
    ui->toolButtonEntryDown->setFixedSize(QSize(31, 31));
    ui->toolButtonEntryDown->setIcon(QIcon(":/image_open_save/image/down.png"));
    ui->toolButtonEntryDown->setIconSize(QSize(30, 30));

    /* table widget has 4 columns in default */
    ui->tableWidget->setColumnCount(colCount);

    /* set header name for each column of table widget */
    QTableWidgetItem *headerKernelID = new QTableWidgetItem("Kernel ID");
    ui->tableWidget->setHorizontalHeaderItem(0, headerKernelID);

    QTableWidgetItem *headerAID = new QTableWidgetItem("AID Id");
    ui->tableWidget->setHorizontalHeaderItem(1, headerAID);

    QTableWidgetItem *headerTranType = new QTableWidgetItem("Transaction Type");
    ui->tableWidget->setHorizontalHeaderItem(2, headerTranType);

    /* set column width for each column */
    ui->tableWidget->setColumnWidth(0, 130);
    ui->tableWidget->setColumnWidth(1, 60);
    ui->tableWidget->setColumnWidth(2, 150);
    ui->tableWidget->setColumnWidth(3, 2000);
    ui->tableWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);

   /* init signal mapper for each row of columns in table widget */
    sigMapp             = new QSignalMapper();
    m_sigMappComboAID   = new QSignalMapper();
    m_sigMappComboKerId = new QSignalMapper();
    m_sigMappComboTrans = new QSignalMapper();
    m_sigMappLineEdit   = new QSignalMapper();

    connect(this, SIGNAL(UpdatePaypass(qint32)), PayPass, SLOT(UpdateData(qint32)));

   connect(PayPass, SIGNAL(buttonOk_clicked(qint32,QString)), this,
           SLOT(updateLineEditEntryPointConfig(qint32,QString)));


}

/**
 * @brief MainWindow::initializeDataStorage
 */
void MainWindow::initializeDataStorage()
{
    // add button load DEK
    ui->toolButtonLoadDEK->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButtonLoadDEK->setFixedSize(QSize(65, 80));
    ui->toolButtonLoadDEK->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->toolButtonLoadDEK->setIconSize(QSize(50, 50));
    ui->toolButtonLoadDEK->setText("Load DEK DET");
    ui->toolButtonLoadDEK->setToolTip("Load DEK DET");
    ui->toolButtonLoadDEK->setAutoRaise(true);

    // add button clear DEK
    ui->toolButtonClearDEK->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButtonClearDEK->setFixedSize(QSize(65, 80));
    ui->toolButtonClearDEK->setIcon(QIcon(":/image_open_save/image/xicon.png"));
    ui->toolButtonClearDEK->setIconSize(QSize(50, 50));
    ui->toolButtonClearDEK->setText("Clear DEK DET");
    ui->toolButtonClearDEK->setToolTip("Clear DEK DET");
    ui->toolButtonClearDEK->setAutoRaise(true);

}

/**
 * @brief MainWindow::initializeSetup
 */
void MainWindow::initializeSetup()
{
    ui->checkBox_SHA1->setChecked(true);
    ui->toolButton_TTQ->setStyleSheet("QToolButton{border:none; padding: 0px}");
    ui->toolButton_TTQ->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    ui->toolButton_TTQ->setIconSize(QSize(ui->toolButton_TTQ->width(), ui->toolButton_TTQ->height()));

    ui->lineEdit_defaultTTQ->setInputMask("hhhhhhhh");
    ui->lineEdit_defaultTTQ->setText("00000000");
    ui->lineEdit_defaultTTQ->setEnabled(false);
    ui->toolButton_TTQ->setEnabled(false);

    ui->checkBox_autoClear->setChecked(true);

    QList<int> ListReferral;
    ListReferral << 3031 << 3032;

    for (int i =0; i <ListReferral.count(); i++){
        ui->comboBox_ManageACE->addItem(QString::number(ListReferral.at(i)));
    }

    ui->comboBox_ManageACE->setEnabled(false);
    ui->lineEdit_defaultPIN->setText("1234");

}

/**
 * @brief MainWindow::initializeDRLVisa
 */
void MainWindow::initializeDRLVisa()
{
    // add button open
    ui->toolButton_DRLOpen->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLOpen->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLOpen->setIcon(QIcon(":/image_open_save/image/export.png"));
    ui->toolButton_DRLOpen->setIconSize(QSize(50, 50));
    ui->toolButton_DRLOpen->setText("Open");
    ui->toolButton_DRLOpen->setToolTip("open configuration");

    // add button save
    ui->toolButton_DRLSave->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLSave->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLSave->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->toolButton_DRLSave->setIconSize(QSize(50, 50));
    ui->toolButton_DRLSave->setText("Save");
    ui->toolButton_DRLSave->setToolTip("save configuration");
    ui->toolButton_DRLSave->setEnabled(false);

    // add button save as
    ui->toolButton_DRLSaveAs->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLSaveAs->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLSaveAs->setIcon(QIcon(":/image_open_save/image/import.png"));
    ui->toolButton_DRLSaveAs->setIconSize(QSize(50, 50));
    ui->toolButton_DRLSaveAs->setText("Save As");
    ui->toolButton_DRLSaveAs->setToolTip("Save As configuration");

    // add button send
    ui->toolButton_DRLSend->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLSend->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLSend->setIcon(QIcon(":/image_open_save/image/uparrow.png"));
    ui->toolButton_DRLSend->setIconSize(QSize(50, 50));
    ui->toolButton_DRLSend->setText("Send");
    ui->toolButton_DRLSend->setToolTip("Send configuration");

    // add button ACE
    ui->toolButton_DRLACE->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->toolButton_DRLACE->setFixedSize(QSize(60, 70));
    ui->toolButton_DRLACE->setIcon(QIcon(":/image_open_save/image/ace_icon.png"));
    ui->toolButton_DRLACE->setIconSize(QSize(50, 50));
    ui->toolButton_DRLACE->setText("ACE");
    ui->toolButton_DRLACE->setToolTip("Get processing from ACE");

    // add button plus
    ui->toolButton_DRLAdd->setFixedSize(QSize(31, 31));
    ui->toolButton_DRLAdd->setIcon(QIcon(":/image_open_save/image/plus1.png"));
    ui->toolButton_DRLAdd->setIconSize(QSize(30, 30));

    // add button minus
    ui->toolButton_DRLRemove->setFixedSize(QSize(31, 31));
    ui->toolButton_DRLRemove->setIcon(QIcon(":/image_open_save/image/minus1.png"));
    ui->toolButton_DRLRemove->setIconSize(QSize(30, 30));

    // add button up
    ui->toolButton_DRLUp->setFixedSize(QSize(31, 31));
    ui->toolButton_DRLUp->setIcon(QIcon(":/image_open_save/image/up1.png"));
    ui->toolButton_DRLUp->setIconSize(QSize(30, 30));

    // add button down
    ui->toolButton_DRLDown->setFixedSize(QSize(31, 31));
    ui->toolButton_DRLDown->setIcon(QIcon(":/image_open_save/image/down.png"));
    ui->toolButton_DRLDown->setIconSize(QSize(30, 30));

    /* set validator for lineEdit, lineEdit allows to type hex number */
    QRegExp rx("[A-Fa-f1-9]{0,256}");
    QValidator *validator = new QRegExpValidator(rx, this);
    ui->lineEdit_DRL->setValidator(validator);
    ui->lineEdit_DRL->setCursorPosition(0);

    ui->lineEdit_DRLFloorLimit->setInputMask("hhhhhhhhhhhh");
    ui->lineEdit_DRLFloorLimit->setText("000000000000");

    ui->lineEdit_DRLTransLimit->setInputMask("hhhhhhhhhhhh");
    ui->lineEdit_DRLTransLimit->setText("000000000000");

    ui->lineEdit_DRLRequiredLimit->setInputMask("hhhhhhhhhhhh");
    ui->lineEdit_DRLRequiredLimit->setText("000000000000");

    QStringList listItem;

    listItem << "not allowed (0x00)" << "online cryptogram request "
             << "not allowed (0x02)";

    for (qint32 i =0; i< listItem.length(); i++)
    {
        ui->comboBox_DRLZero->addItem(listItem.at(i));
    }
}

/**
 * @brief MainWindow::updateDir
 * each time open terminal configuration, processing configuration,
 * entrypoint configuration, update the Direction to binary file
 * @param dir
 * @return
 */
QString MainWindow::updateDir(QString dir)
{
    qint32 index =0;

    index = dir.lastIndexOf("/");

    return dir.remove(index+1, dir.length()-index-1);
}

/**
 * @brief MainWindow::CheckTag
 * convert TLV string to a buffer that contains all tag, length, value.
 * return the length of TLV buffer
 * @param text
 * @param Buffertam
 * @return
 */
uint16_t MainWindow::CheckTag(const QString &text, uint8_t *bufferTmp)
{
    uint16_t lengthTotal =0;
    quint32 numBytes =0, length =0;
    quint32 stringSize =0;

    stringSize = text.size();

    if (stringSize ==0){
        return 0;
    }
    if ((stringSize %2)!= 0){
        stringSize -= 1;
    }
    uint8_t textToHex[BUFFER_SIZE];

    /* convert two charater into number */
    for (quint32 i =0; i< stringSize; i +=2)
    {
        textToHex[i/2] = text.mid(i,2).toInt(NULL, 16);
    }

    /* split every tag, length, value and save in to a buffer */
    for (quint32 i =0 ; i< stringSize/2; i = i+ numBytes + 1 + length)
    {
        /* if there are no more tag in string to check, numBytes = 0 */
        if ((i+1) < stringSize/2)
        {
            /* get number of Bytes of tag, two bytes or three bytes or just one byte */
            if ((textToHex[i] & 0x1F) == 0x1F)
            {
                if ((textToHex[i+1] & 0x80) == 0x80)
                {
                  numBytes = 3;
                }
                else
                {
                    numBytes = 2;
                }
            }

        else
            {
                numBytes = 1;
            }
        }
        else
        {
            numBytes =0;
        }

        if ((numBytes != 0) && ((numBytes+i) < stringSize/2))
        {
            if ((textToHex[i+ numBytes]) == 0x81)
            {
                numBytes += 1;
            }
            if ((numBytes + i) < stringSize/2)
            {
              length = textToHex[numBytes+i];
              if ((numBytes + i + 1 + length) <= stringSize/2)
              {
                  /* save all value in TextToHex buffer to bufferTmp */
                 for (quint32 j =0; j<numBytes+1; j++)
                 {
                     *bufferTmp++ = textToHex[i+j];
                 }

                 for (quint32 k =0; k < length; k++)
                 {
                     *bufferTmp++ = textToHex[k+numBytes+i+1];
                 }
               }

              lengthTotal += length + 1 + numBytes;
           }
      }
}

    return lengthTotal;
}

/**
 * @brief MainWindow::CheckTagAvai
 * use for terminal configuration tab.
 * Check the tag, length, value is appropriate to parse or not.
 * and according to value of each tag, check the Checkbox, fill the lineEdit.
 * @param Buffertam
 * @param LengTotal
 * @param error
 * @return
 */
bool MainWindow::checkTagAvai(quint8 *bufferTmp, quint16 lengthTotal, QString &error)
{
    int length =0, numByte =0;
    QString err;
    bool state;

    /* all the checkboxes are uncheck, all the lineEdits are unfilled */
    for (qint32 i =0; i< 23; i++)
    {
        m_stateTagTer[i] = false;
    }

   /*check the tag, length, value */
    for (int i =0; i < lengthTotal; i = i+ numByte +1 + length)
    {
        /* find number of bytes of tag */
        if ((bufferTmp[i] & 0x1F) == 0x1F)
        {
            if ((bufferTmp[i+1] & 0x80) == 0x80)
            {
                numByte = 3;
            }
            else
            {
                numByte = 2;
            }
        }
        else
        {
            numByte = 1;
        }

        length = bufferTmp[i + numByte];
        /* check length, value of each tag */
        state = updateCheckBoxLineSpin(i, numByte, bufferTmp, length, err);
        if (!state)
        {
            error = err;
            return false;
        }
    }
   /* if the buffer didn't contain any tag, uncheck or do not fill the appropriate lineEdit or checkBox */
    for(qint32 i =0; i< 23; i++)
    {
        if (!m_stateTagTer[i])
        {
            if (m_typeWidgetTer[i]== twCheckBox)
            {
                QCheckBox *checkbox = (QCheckBox*)m_listWid.at(i);
                checkbox->setChecked(false);
            }

            if (m_typeWidgetTer[i] == twLineEdit)
            {
               QLineEdit *lineEd = (QLineEdit*)m_listWid.at(i);
               lineEd->setText("000000000000");
            }

            if (m_typeWidgetTer[i] == twComboBox)
            {
                QComboBox *combo = (QComboBox*)m_listWid.at(i);
                combo->setCurrentIndex(0);
            }
            if (m_typeWidgetTer[i] == twSpinBox)
            {
                QSpinBox *spinBox = (QSpinBox*)m_listWid.at(i);
                spinBox->setValue(0);
            }
        }
    }

    return true;
}

/**
 * @brief MainWindow::updateCheckBoxLineSpin
 * check the length of tag is appropriate or not, after that, check the Checkbox, fill the LineEdit.
 * @param index is the index of tag in buffer
 * @param numByte is number of bytes of Tag
 * @param Buffertam
 * @param Length
 * @param error
 * @return
 */
bool MainWindow::updateCheckBoxLineSpin(quint8 index, quint8 numByte, quint8 *bufferTmp,
                                        quint8 length, QString &error)
{
    QString stringNeeded;
    quint64  valueSpin =0;
    bool    state;

    if (numByte == 2){
        switch (bufferTmp[index]){
        case 0x9F:
            switch (bufferTmp[index+1]){
            case 0x1A:
                stringNeeded ="";
                if (length !=2)  /* in the document, tag 9F1A has a fixed length 2 */
                {
                    error = "9F1A";
                    return false;
                }
                m_stateTagTer[0] = true;
                /*get each byte from buffer and save to a string to set lineEdit */
                for (qint32 i =0; i < length; i++)
                {
                    if (bufferTmp[index+numByte+1+i] <= 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTmp[index+numByte+1+i], 16));
                }
                ui->lineEditTerCounCode->setText(stringNeeded.toUpper());
                break;

            case 0x35:
                stringNeeded ="";
                if (length !=1)
                {
                    error = "9F35";
                    return false;
                }

                m_stateTagTer[2] = true;
                if (bufferTmp[index+numByte+1] <= 0x0F)
                {
                    stringNeeded.append("0");
                }

                stringNeeded += QString::number(bufferTmp[index+numByte+1], 16);
                state = false;
                qDebug() << "string need " << stringNeeded;
                for (qint32 i =0; i< ui->comboBoxTerminal->count(); i++)
                {
                    if (stringNeeded == ui->comboBoxTerminal->itemText(i))
                    {
                        state = true;
                        break;
                    }
                }

                if (!state)
                {
                    error = "9F35";
                    return false;
                }
                ui->comboBoxTerminal->setCurrentText(stringNeeded.toUpper());
                break;

            case 0x33:
                stringNeeded ="";
                if (length !=3)
                {
                    error = "9F33";
                    return false;
                }
                m_stateTagTer[4] = true;
                for (qint32 i =0; i < length; i++)
                {
                    if (bufferTmp[index+numByte+1+i] <= 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTmp[index+numByte+1+i], 16));
                }
                ui->lineEditTerminal->setText(stringNeeded.toUpper());
                break;

            case 0x40:
                stringNeeded = "";
                if (length !=5)
                {
                    error = "9F40";
                    return false;
                }
                m_stateTagTer[5] = true;
                for (qint32 i =0; i < length; i++)
                {
                    if (bufferTmp[index+numByte+1+i] <= 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTmp[index+numByte+1+i], 16));
                }
                ui->lineEditAddition->setText(stringNeeded.toUpper());
                break;
            }
            break;
          case 0xDF:
            switch (bufferTmp[index+1])
            {
              case 0x79:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF79";
                    return false;
                }
                m_stateTagTer[1] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxCardHolder->setChecked(state);
                break;

              case 0x0A:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF0A";
                    return false;
                }
                m_stateTagTer[3] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                qDebug() << "check 2 " << stringNeeded << state;
                ui->checkBoxEMV1->setChecked(state);
                break;

              case 0x55:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF55";
                    return false;
                }
                m_stateTagTer[6] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxEMV2->setChecked(state);
                break;

              case 0x0B:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF0B";
                    return false;
                }
                m_stateTagTer[7] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxMastripe->setChecked(state);
                break;
              case 0x27:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF27";
                    return false;
                }
                m_stateTagTer[8] = true;

                quint32 numTmp;
                numTmp = bufferTmp[index+numByte+1];
                if (numTmp >= 100)
                {
                    numTmp = numTmp/10;
                }

                if (numTmp < 10)
                {
                    stringNeeded += "0";
                }
                stringNeeded += QString::number(numTmp);
                ui->lineEditPINTimeout->setText(stringNeeded);

                break;

              case 0x06:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF06";
                    return false;
                }
                m_stateTagTer[9] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxOnline->setChecked(state);
                break;

              case 0x08:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF08";
                    return false;
                }
                m_stateTagTer[10] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxAdvice->setChecked(state);
                break;

              case 0x7A:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF7A";
                    return false;
                }
                m_stateTagTer[11] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxPSE->setChecked(state);
                break;

              case 0x0D:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF0D";
                    return false;
                }
                m_stateTagTer[12] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxAutorun->setChecked(state);
                break;

              case 0x10:
                if (length !=3)
                {
                    error = "DF10";
                    return false;
                }
                m_stateTagTer[13] = true;
                for (qint32 i =0; i < length; i++)
                {
                    valueSpin += bufferTmp[index+numByte+1+i] << 8*(length-i-1);
                }
                ui->spinBox->setValue(valueSpin);
                break;

              case 0x7B:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF7B";
                    return false;
                }
                m_stateTagTer[14] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxPinBypass->setChecked(state);
                break;

              case 0x07:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF07";
                    return false;
                }
                m_stateTagTer[15] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxReferral->setChecked(state);
                break;

              case 0x09:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF09";
                    return false;
                }
                m_stateTagTer[16] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxDefault->setChecked(state);
                break;

            case 0x73:
                stringNeeded = "";
                if (length !=5)
                {
                    error = "DF73";
                    return false;
                }
                m_stateTagTer[17] = true;
                for (qint32 i =0; i < length; i++)
                {
                    if (bufferTmp[index+numByte+1+i] <= 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTmp[index+numByte+1+i], 16));
                }
                ui->lineEditDefault->setText(stringNeeded.toUpper());
                break;

            case 0x74:
                stringNeeded = "";
                if (length !=5)
                {
                    error = "DF74";
                    return false;
                }
                m_stateTagTer[18] = true;
                for (qint32 i =0; i < length; i++)
                {
                    if (bufferTmp[index+numByte+1+i] <= 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTmp[index+numByte+1+i], 16));
                }
                ui->lineEditDenial->setText(stringNeeded.toUpper());
                break;

            case 0x75:
                stringNeeded = "";
                if (length !=5)
                {
                    error = "DF75";
                    return false;
                }
                m_stateTagTer[19] = true;
                for (qint32 i =0; i < length; i++)
                {
                    if (bufferTmp[index+numByte+1+i] <= 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTmp[index+numByte+1+i], 16));
                }
                ui->lineEditOnline->setText(stringNeeded.toUpper());
                break;

            case 0x53:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF53";
                    return false;
                }
                m_stateTagTer[20] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxRTSNot->setChecked(state);
                break;

            case 0x54:
                stringNeeded = "";
                if (length !=1)
                {
                    error = "DF54";
                    return false;
                }
                m_stateTagTer[21] = true;
                stringNeeded.append(QString::number(bufferTmp[index+numByte+1], 16));
                state = ("0"==stringNeeded)?false:true;
                ui->checkBoxVelocity->setChecked(state);
                break;

            case 0x7C:
                if (length !=1)
                {
                    error = "DF7C";
                    return false;
                }
                m_stateTagTer[22] = true;
                switch (bufferTmp[index+1+numByte])
                {
                 case 0x00:
                    ui->comboBoxCDA->setCurrentText("UNDEFINED");
                    break;
                case 0x01:
                    ui->comboBoxCDA->setCurrentText("MODE_1");
                    break;
                case 0x02:
                    ui->comboBoxCDA->setCurrentText("MODE_2");
                    break;
                case 0x03:
                    ui->comboBoxCDA->setCurrentText("MODE_3");
                    break;
                case 0x04:
                    ui->comboBoxCDA->setCurrentText("MODE_4");
                    break;
                default:
                    ui->comboBoxCDA->setCurrentText("UNDEFINED");
                    break;
               }
               break;

            }
            break;
        }
    }
    return true;
}

/**
 * @brief MainWindow::updateBufferToSaveTer
 * we have the amount of widgets in tab, update buffer followed the order of tag in table in the document.
 * length of total TLV
 * @param buffer
 * @param length
 * @param lengthOfTLV
 */
void MainWindow::updateBufferToSaveTer(quint8 *buffer, quint8 length, quint32 *lengthOfTLV)
{
    bool    ok;
    quint8  tempNum, tempLength;
    quint8  lengthTLVTmp;

    *lengthOfTLV =0;
    for (quint32 i =0; i< length; i++)
    {
       if (m_typeWidgetTer[i] == twSpinBox)
       {
           if (ui->checkBoxAutorun->checkState())
           {
               QSpinBox *spin = (QSpinBox*)m_listWid.at(i);
               *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(0,2).toInt(&ok, 16);
               *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(2,2).toInt(&ok, 16);
               *(buffer++) = (quint8)0x03;
               *(buffer++) = (quint8)(spin->value() >> 16);
               *(buffer++) = (quint8)(spin->value() >> 8);
               *(buffer++) = (quint8)spin->value();

               lengthTLVTmp = 6;
               /* increase the length TLV */
               *lengthOfTLV += lengthTLVTmp;
               qDebug() << "spin box";
           }
           else
           {
               *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(0,2).toInt(&ok,16);
               *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(2,2).toInt(&ok,16);
               *(buffer++) = (quint8)0x03;
               *(buffer++) = (quint8)0x00;
               *(buffer++) = (quint8)0x00;
               *(buffer++) = (quint8)0x00;
               lengthTLVTmp = 6;
               *lengthOfTLV += lengthTLVTmp;
           }
       }

       if (m_typeWidgetTer[i] == twCheckBox)
       {
           QCheckBox *check = (QCheckBox*)m_listWid.at(i);
           tempNum = (quint8)(check->checkState())?true:false;
           *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(0,2).toInt(&ok, 16);
           *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(2,2).toInt(&ok, 16);
           *(buffer++) = (quint8)0x01;
           *(buffer++) = tempNum;
           lengthTLVTmp = 4;
           *lengthOfTLV += lengthTLVTmp;
       }

       if (m_typeWidgetTer[i] == twComboBox)
       {
           QComboBox *combo = (QComboBox*)m_listWid.at(i);
           if (i == 22)
           {
               tempNum = (quint8)combo->currentIndex();
           }
           else
           {
               tempNum = (quint8)combo->currentText().toInt(&ok, 16);
           }

           *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(0,2).toInt(&ok, 16);
           *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(2,2).toInt(&ok, 16);
           *(buffer++) = (quint8)0x01;
           *(buffer++) = tempNum;
           lengthTLVTmp = 4;
           *lengthOfTLV+= lengthTLVTmp;
           qDebug() << "combo bo " << *lengthOfTLV;

       }

       if (m_typeWidgetTer[i] == twLineEdit)
       {
           quint8 tacDefaultOrder = 17, tacDenialOrder = 18, tacOnlineOrder = 19;
           if ((!ui->checkBoxDefault->checkState()) &&((i == tacDefaultOrder)
                                                       | (i == tacDenialOrder) | (i ==tacOnlineOrder)))
           {
               continue;
           }

           quint8 orderOfPinTimeout = 8;
           QLineEdit *line = (QLineEdit*)m_listWid.at(i);
           if (i ==orderOfPinTimeout)
           {
                tempLength = 1;
           }
           else
           {
              tempLength = line->text().length()/2;
           }
           *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(0,2).toInt(&ok, 16);
           *(buffer++) = (quint8)m_tagConfiguration.at(i).mid(2,2).toInt(&ok, 16);
           *(buffer++) = tempLength;

           if (i == orderOfPinTimeout)
           {
               *(buffer++) = (quint8)line->text().toInt();
           }
           else
           {
                for (int j =0; j < line->text().length(); j+=2)
                {
                   *(buffer++) = (quint8)line->text().mid(j, 2).toInt(&ok, 16);
                }
           }

           *lengthOfTLV += 3+ tempLength;

       }

    }
}

/**
 * @brief MainWindow::checkTLV
 * check data is followed the rule of parsing data, TLV or not.
 * @param bufferTemp
 * @param lengthTotal
 * @param ith
 * @return
 */
bool MainWindow::checkTLV(quint8 *bufferTemp, quint16 lengthTotal, quint16 ith)
{
    quint16 numBytes =0;
    quint16 lengthTemp, lengthTag =0;

    lengthTemp = lengthTotal;

    while (lengthTemp >0){

        /* get the number of bytes of tag */
        if ((bufferTemp[ith] & 0x1F) == 0x1F)
        {
            if (((ith + 1) < lengthTotal) && (bufferTemp[ith +1] & 0x80) == 0x80)
            {
                numBytes = 3;
            }
            else if ((ith +1) < lengthTotal)
            {
                numBytes = 2;
            }

            else if ((ith +1) == lengthTotal)
            {
                return true;
            }
        }

        else
        {
            numBytes = 1;
        }

        if (((numBytes + ith) < lengthTotal) && (bufferTemp[ith + numBytes] == 0x81))
        {
            numBytes++;
        }

        if (ith + numBytes == lengthTotal){
            return true;
        }

        lengthTag = bufferTemp[ith + numBytes];
        /* check length of tag is suitable or not */
        if (lengthTag + 1 + numBytes > lengthTemp)
        {
            return false;
        }

        lengthTemp -=  (numBytes + lengthTag +1);
        /* increase the index of tag */
        ith += numBytes + lengthTag + 1;
    }

    return true;
}

/**
 * @brief MainWindow::getAndCheckAIDProcessing
 * check to parse data
 * each ff33 is appropriate with a row in listwidget in processing tab.
 * split all ff33 in the buffer, and save it to a two dimension array.
 * @param buffer
 * @param lengthTotal
 * @param err
 * @param lengthFF
 * @param numItem
 * @return
 */
bool MainWindow::getAndCheckAIDProcessing(quint8 *buffer, quint16 lengthTotal,
                                          QString &err, quint32 *lengthFF, quint8 *numItem)
{
    quint16 lengthTmp;
    bool    state;
    lengthTmp = lengthTotal;
    quint16    index =0, numBytes, lengthTag =0;
    *numItem =0;
    quint8 numberOfSubsequence;

    while(lengthTmp >0)
    {
        lengthTag = 0;
        /* get the number of bytes of tag */
        if ((buffer[index] & 0x1F) == 0x1F){
            if (((index + 1) < lengthTotal) && ((buffer[index +1] & 0x80) == 0x80))
            {
                numBytes = 3;
            }
            else if ((index +1) < lengthTotal)
            {
                numBytes = 2;
            }

            else if ((index +1) == lengthTotal)
            {
                return true;
            }
        }

        else
        {
            numBytes = 1;
        }

        if (index + numBytes == lengthTotal){
            return true;
        }

        /*check length of TLV, the length is higher than 127, higher than 256
         according to the basic rule of TLV */
        if ((buffer[index+numBytes] & 0x80) == 0x80)
        {
            numberOfSubsequence = buffer[index+numBytes] & 0x7f;
            numBytes++;
            for (quint32 i =0; i< numberOfSubsequence; i++)
            {
                lengthTag += (quint8)buffer[index+numBytes+i] << (numberOfSubsequence-i-1)*8;
            }
            /* find the numBytes of tag*/
            numBytes+= numberOfSubsequence-1;
        }
        else
        {
            lengthTag = buffer[index + numBytes];
        }

        if (lengthTag + 1 + numBytes > lengthTmp){
            err = "parse data";
            return false;
        }
        /* according to the document, each item in listWidget <=> each ff33 */
        if ((buffer[index]==0xff)&&(buffer[index+1] == 0x33))
        {
            /* create a buffer to get the data from tag ff33 */
            quint8 *bufferTemp = new quint8[lengthTag];
            for (quint32 j =0; j< lengthTag; j++)
            {
                bufferTemp[j] = buffer[index+numBytes+1+j];
            }
             /* check the TLV of each ff33 */
            state = checkTLV(bufferTemp, lengthTag, 0);
            if (false == state)
            {
                err = "parse data";
                delete[] bufferTemp;
                return false;
            }

            /* create two dimension array */
            quint8 **databaseTmp;
            databaseTmp = new quint8*[lengthTag];

            for (quint32 k =0; k< lengthTag; k++)
            {
                databaseTmp[k] = new quint8[lengthTag];
            }
            /* init tags for databaseTmp followed the table in document */
            initializeBufferDatabase(databaseTmp);
            /* split bufferTemp to every tag. each tag is a row in array databaseTmp */
            state = checkLengthEachTagProcess(bufferTemp, lengthTag, databaseTmp, err);

            if (!state)
            {
                for (quint32 i =0; i< lengthTag; i++)
                {
                    delete[] databaseTmp[i];
                }
                delete[] databaseTmp;
                err = "parse data";
                return false;
            }
            /*store the databaseTmp into the numItem row of m_databaseArray */
            storeTagToDatabaseProcess(m_databaseArray, *numItem, databaseTmp, lengthFF);

            /* increase the address of lengthFF in file */
            lengthFF++;
            *(numItem) +=1;
            for (quint32 i =0; i< lengthTag; i++)
            {
                delete[] databaseTmp[i];
            }
            delete[] databaseTmp;
            delete[] bufferTemp;
        }

        lengthTmp -=  (numBytes + lengthTag +1);
        index += numBytes + lengthTag + 1;
    }

    /* if in entryPoint tab, there are some AID Ids */
    if (m_numItemsProcess ==0)
    {
        QString warningString;
        for (int i =0; i< ui->tableWidget->rowCount(); i++)
        {
            warningString += "modified AID Id on row " +QString::number(i) +
                    " because it exceeds the number of AIDs in PROCESSING configuration \n";
        }
        err = warningString;
        return false;
    }

    return true;
}

/**
 * @brief MainWindow::updateCheckBoxLineProcess
 * update state of the widget in processing tab.
 * @param index
 * @param numByte
 * @param bufferTemp
 * @param length
 * @param textDDOL
 * @param textTDOL
 */
void MainWindow::updateCheckBoxLineProcess(quint8 index,quint8 numByte, quint8* bufferTemp,
                                           quint8 length, QString &textDDOL, QString &textTDOL)
{
    bool state;
    QString stringNeeded;
    quint32 valueSpin;
    quint64  valueTemp;
    float  val =0;
    QString stringFinal;

    if ((numByte ==2) || (numByte == 3))
    {
        switch(bufferTemp[index])
        {
          case 0xDF:
            switch(bufferTemp[index+1])
            {
             case 0x7E:
                m_stateTagProcessing[1] = true;
                state = (0x00 == bufferTemp[index+numByte+1])?false:true;
                ui->checkBox_ASIProcess->setEnabled(true);
                ui->checkBox_ASIProcess->setChecked(state);
                break;

              case 0x11:
                m_stateTagProcessing[3] = true;
                state = (0x00 == bufferTemp[index+numByte+1])?false:true;
                ui->checkBox_SkipTACProcess->setEnabled(true);
                ui->checkBox_SkipTACProcess->setChecked(state);
                break;

              case 0x12:
                 m_stateTagProcessing[4] = true;
                 state = (0x00 == bufferTemp[index+numByte+1])?false:true;
                 ui->checkBox_RTSProcess->setEnabled(true);
                 ui->checkBox_RTSProcess->setChecked(state);
                break;

            case 0x13:
               m_stateTagProcessing[5] = true;
               state = (0x00 == bufferTemp[index+numByte+1])?false:true;
               ui->checkBox_VelocityProcess->setEnabled(true);
               ui->checkBox_VelocityProcess->setChecked(state);
              break;

            case 0x14:
               m_stateTagProcessing[6] = true;
               state = (0x00 == bufferTemp[index+numByte+1])?false:true;
               ui->checkBox_FloorLimitProcess->setEnabled(true);
               ui->checkBox_FloorLimitProcess->setChecked(state);
              break;

            case 0x15:
               m_stateTagProcessing[7] = true;
               state = (0x00 == bufferTemp[index+numByte+1])?false:true;
               ui->checkBox_TACProcess->setEnabled(true);
               ui->checkBox_TACProcess->setChecked(state);
              break;

            case 0x21:
                stringNeeded = "";
                m_stateTagProcessing[8] = true;
                for(quint32 i =0; i< length; i++)
                {
                    if (bufferTemp[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTemp[index+numByte+1+i], 16));
                    qDebug() << "buffertmp " << bufferTemp[index+numByte+1+i];
                }
                qDebug() << "string ned " << stringNeeded;
                ui->lineEditDenialProcessing->setEnabled(true);
                ui->toolButtonDenialProcessing->setEnabled(true);
                ui->lineEditDenialProcessing->setText(stringNeeded.toUpper());
                ui->lineEditDenialProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid black;"
                                                                    "border-radius:3px;padding-right: 30px } "));
                ui->label_Denial->setEnabled(true);
                break;

            case 0x22:
                stringNeeded = "";
                m_stateTagProcessing[9] = true;
                for(quint32 i =0; i< length; i++)
                {
                    if (bufferTemp[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTemp[index+numByte+1+i], 16));
                }
                ui->lineEditOnlineProcessing->setEnabled(true);
                ui->toolButtonOnlineProcessing->setEnabled(true);
                ui->lineEditOnlineProcessing->setText(stringNeeded.toUpper());
                ui->lineEditOnlineProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid black;"
                                                                    "border-radius:3px; padding-right: 30px} "));
                ui->label_Online->setEnabled(true);
                break;

            case 0x20:
                stringNeeded = "";
                m_stateTagProcessing[10] = true;
                for(quint32 i =0; i< length; i++)
                {
                    if (bufferTemp[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTemp[index+numByte+1+i], 16));
                }
                ui->lineEditDefaultProcessing->setEnabled(true);
                ui->toolButtonDefaultProcessing->setEnabled(true);
                ui->lineEditDefaultProcessing->setText(stringNeeded.toUpper());
                ui->lineEditDefaultProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid black;"
                                                                     "border-radius:3px; padding-right: 30px} "));
                ui->label_Default->setEnabled(true);
                break;

            case 0x70:
                m_stateTagProcessing[12] = true;
                valueSpin = bufferTemp[index+numByte +1];
                if (valueSpin >100) valueSpin = 100;
                ui->spinBox_TargetProcess->setEnabled(true);
                ui->spinBox_TargetProcess->setValue(valueSpin);
                ui->label_Target->setEnabled(true);
                break;

            case 0x6E:
                stringNeeded = "";
                valueTemp =0;
                m_stateTagProcessing[13] = true;
                for (qint32 i =0; i< length; i++)
                {
                   valueTemp += bufferTemp[index+numByte+1+i] << 8*(length-1-i);
                }
                val = (float)valueTemp/10.0;
                ui->lineEditProcessingThresh->setEnabled(true);
                ui->lineEditProcessingThresh->setText(QString::number(val));
                ui->lineEditProcessingThresh->setStyleSheet(QString("QLineEdit {border: 1px solid black;"
                                                                    "border-radius:3px; } "));
                ui->label_Thresh->setEnabled(true);
                break;

            case 0x6F:
                m_stateTagProcessing[14] = true;
                valueSpin = bufferTemp[index+numByte +1];
                if(valueSpin >100) valueSpin = 100;
                ui->spinBox_MaxTargetProcess->setEnabled(true);
                ui->spinBox_MaxTargetProcess->setValue(valueSpin);
                ui->label_MaxTarget->setEnabled(true);
                break;

            case 0x01:
                m_stateTagProcessing[15] = true;
                state = (0x00 == bufferTemp[index+numByte+1])?false:true;
                ui->checkBox_DefaultDDOL->setEnabled(true);
                ui->checkBox_DefaultDDOL->setChecked(state);
                break;

            case 0x71:
                stringNeeded = "";
                qDebug() << "m data " << m_databaseArray[0][120];
                m_stateTagProcessing[16] = true;
                qDebug() << "m data1 " << m_databaseArray[0][120];
                for (qint32 i =0; i< length; i++)
                {
                    if (bufferTemp[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTemp[index+numByte+1+i], 16));
                }
                qDebug() << "m data2 " << m_databaseArray[0][120];

                ui->textEdit_DDOL->setEnabled(true);

                qDebug() << "m data3 " << m_databaseArray[0][120];
                ui->textEdit_DDOL->setStyleSheet(QString("border: 1px solid black;border-radius:3px;  "));
                qDebug() << "m data4 " << m_databaseArray[0][121];
                ui->label_DDOL->setEnabled(true);
                stringFinal = stringNeeded.toUpper();
                textDDOL = stringFinal;
                qDebug() << "m data " << m_databaseArray[0][121];
                break;

            case 0x02:
                m_stateTagProcessing[17] = true;
                state = (0x00 == bufferTemp[index+numByte+1])?false:true;
                ui->checkBox_DefaultTDOL->setEnabled(true);
                ui->checkBox_DefaultTDOL->setChecked(state);
                break;

            case 0x72:
                stringNeeded = "";
                m_stateTagProcessing[18] = true;
                for (qint32 i =0; i< length; i++)
                {
                    if (bufferTemp[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTemp[index+numByte+1+i], 16));
                }
                ui->textEdit_TDOL->setEnabled(true);
                ui->textEdit_TDOL->setStyleSheet(QString("border: 1px solid black;border-radius:3px;  "));
                qDebug() << "string df72 " << stringNeeded.toUpper();
                ui->label_TDOL->setEnabled(true);
                textTDOL = stringNeeded.toUpper();
                break;
            }
            break;

        case 0x9F:
            switch (bufferTemp[index+1])
            {
             case 0x1B:
                stringNeeded = "";
                valueTemp =0;
                for (quint32 i =0; i< length; i++)
                {
                    valueTemp += bufferTemp[index+numByte+1+i] << (length-1-i)*8;
                }

                val = (float)valueTemp/10.0;
                m_stateTagProcessing[11] = true;
                ui->lineEditProcessingFloor->setEnabled(true);
                ui->lineEditProcessingFloor->clear();
                ui->lineEditProcessingFloor->setText(QString::number(val));
                ui->lineEditProcessingFloor->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; } "));
                ui->label_Floor->setEnabled(true);
                break;

            case 0x01:
                stringNeeded = "";
                for (quint32 i =0; i< length; i++)
                {
                    if (bufferTemp[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTemp[index+numByte+1+i], 16).toUpper());
                }
                m_stateTagProcessing[0] = true;
                ui->lineEditAcquireIDProcessing->setEnabled(true);
                ui->lineEditAcquireIDProcessing->setText(stringNeeded);
                ui->lineEditAcquireIDProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; } "));
                ui->label_Acquire->setEnabled(true);
                break;

            case 0x09:
                stringNeeded = "";
                for (quint32 i =0; i< length; i++)
                {
                    if (bufferTemp[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTemp[index+numByte+1+i], 16));
                }
                ui->lineEditAVNProcessing->setEnabled(true);
                ui->lineEditAVNProcessing->setText(stringNeeded);
                ui->lineEditAVNProcessing->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; } "));
                m_stateTagProcessing[2] = true;
                ui->label_AVN->setEnabled(true);
                break;
            }
            break;

        case 0x5F:
            switch(bufferTemp[index+1])
            {
            case 0x2A:
                stringNeeded = "";
                for (quint32 i =0; i < length; i++)
                {
                    if (bufferTemp[index+numByte+1+i] < 0x0F)
                    {
                        stringNeeded.append("0");
                    }
                    stringNeeded.append(QString::number(bufferTemp[index+numByte+1+i], 16));
                }
                m_stateTagProcessing[19] = true;
                ui->lineEditProcessingTCC->setEnabled(true);
                ui->lineEditProcessingTCC->setText(stringNeeded);
                ui->lineEditProcessingTCC->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; } "));
                ui->label_TCC->setEnabled(true);
                break;

            case 0x36:
                stringNeeded ="";
                stringNeeded = QString::number(bufferTemp[index+numByte+1]);
                ui->lineEditProcessingExpo->setEnabled(true);
                ui->lineEditProcessingExpo->setText(stringNeeded);
                ui->lineEditProcessingExpo->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; } "));
                m_stateTagProcessing[20] = true;
                ui->label_Expo->setEnabled(true);
                break;
            }
            break;

        }
    }
}

/**
 * @brief MainWindow::storeTagToDatabaseProcess
 * store all value in bufferTemp in a row of database
 * @param ithRow
 * @param bufferTmp
 * @param lengthTLV
 */
void MainWindow::storeTagToDatabaseProcess(quint8 (*database)[MAX_COL], quint8 ithRow,
                                           quint8 **bufferTmp, quint32 *lengthTLV)
{
    quint32 numberOfTags;
    quint32 index =0, lengthTag =0;
    quint8   numberOfBytes;
    *lengthTLV =0;
    numberOfTags = 22;
    for (quint8 i=0; i< numberOfTags; i++)
    {
        if (i != 1)       // different from tag 4f, all the tag has length 2 like df27.
        {
            if ((bufferTmp[i][2] & 0x81) == 0x81)
            {
                lengthTag = bufferTmp[i][3];
                numberOfBytes = 4;
            }
            else
            {
                lengthTag = bufferTmp[i][2];
                numberOfBytes = 3;
            }
            for (quint32 j = 0; j< lengthTag +numberOfBytes; j++)
            {
                database[ithRow][index++] = bufferTmp[i][j];
            }
            *lengthTLV += lengthTag+numberOfBytes;
        }
        else
        {
            lengthTag = bufferTmp[i][1];
            numberOfBytes = 2;
            for (quint32 j =0; j< lengthTag+numberOfBytes; j++)
            {
                database[ithRow][index++] = bufferTmp[i][j];
            }
            *lengthTLV += lengthTag+numberOfBytes;
        }
    }
}

/**
 * @brief MainWindow::getIndexOfTag4F
 * @param buffer
 * @param length
 * @param ith
 * @return
 */
bool MainWindow::getIndexOfTag4F(quint8 *buffer, quint8 length, quint8 *ith)
{
    quint16 numBytes =0, index =0;
    quint16 lengthTemp, lengthTag =0;

    lengthTemp = length;

    while (lengthTemp >0){
        if ((buffer[index] & 0x1F) == 0x1F){
            if (((index + 1) < length) && buffer[index +1] == 0x80)
            {
                numBytes = 3;
            }
            else if ((index +1) < length)
            {
                numBytes = 2;
            }

            else if ((index +1) == length)
            {
                return false;
            }
        }

        else
        {
            numBytes = 1;
        }

        if (((numBytes + index) < length) && buffer[index + numBytes] == 0x81)
        {
            numBytes++;
        }

        if (index + numBytes == length){
            return false;
        }

        if (buffer[index] == 0x4F)
        {
            *ith = index;
            return true;
        }
        lengthTag = buffer[index + numBytes];
        index += numBytes + lengthTag + 1;
        lengthTemp -=  (numBytes + lengthTag +1);
    }

    return false;

}

/**
 * @brief MainWindow::checkLengthEachTagProcess
 * with all of the value in buffer, check and store into array database,
 * each row of database is represent for each tag in the table in the document
 * @param buffer
 * @param lengthTotal
 * @param database
 * @param err
 * @return
 */
bool MainWindow::checkLengthEachTagProcess(quint8 *buffer, quint32 lengthTotal, quint8 **database,
                                          QString &err)
{
    quint32 numByte =0;
    quint32 lengthTag=0;
    QString stringTmp;
    quint32 lengthTLV;

    for (quint32 i =0; i < lengthTotal; i = i+ numByte +1 + lengthTag)
    {
        /* check and get the number of bytes of each tag */
        if ((buffer[i] & 0x1F) == 0x1F)
        {
            if ((buffer[i+1] & 0x80) == 0x80)
            {
                numByte = 3;
            }
            else
            {
                numByte = 2;
            }
        }
        else
        {
            numByte = 1;
        }

        if ((buffer[i+numByte] & 0x81) == 0x81)
        {
            numByte++;

        }
        lengthTag = buffer[i + numByte];

        /* get the TLV in buffer and store into database */
        if (numByte ==2 || numByte == 3)
        {
            switch(buffer[i])
            {
              case 0xDF:
                switch(buffer[i+1])
                {
                  case 0x7E:
                    if (lengthTag != 1)
                    {
                        err = "DF7E";
                        return false;
                    }
                    database[ptAppSelInd][0] = (quint8)0xDF;
                    database[ptAppSelInd][1] = (quint8)0x7E;
                    database[ptAppSelInd][2] = (quint8)0x01;
                    database[ptAppSelInd][3] = (quint8)buffer[i+numByte+1];
                    break;

                  case 0x11:
                    if (lengthTag !=1)
                    {
                        err = "DF11";
                        return false;
                    }
                    database[ptSkipTAC][0] = (quint8)0xDF;
                    database[ptSkipTAC][1] = (quint8)0x11;
                    database[ptSkipTAC][2] = (quint8)0x01;
                    database[ptSkipTAC][3] = (quint8)buffer[i+numByte+1];
                    break;

                  case 0x12:
                     if (lengthTag != 1)
                     {
                         err = "DF12";
                         return false;
                     }
                     database[ptRTS][0] = (quint8)0xDF;
                     database[ptRTS][1] = (quint8)0x12;
                     database[ptRTS][2] = (quint8)0x01;
                     database[ptRTS][3] = (quint8)buffer[i+numByte+1];
                    break;

                case 0x13:
                   if (lengthTag != 1)
                   {
                       err = "DF13";
                       return false;
                   }
                   database[ptVelChecSupp][0] = (quint8)0xDF;
                   database[ptVelChecSupp][1] = (quint8)0x13;
                   database[ptVelChecSupp][2] = (quint8)0x01;
                   database[ptVelChecSupp][3] = (quint8)buffer[i+numByte+1];
                  break;

                case 0x14:
                   if (lengthTag != 1)
                   {
                       err = "DF14";
                       return false;
                   }
                   database[ptFloorLimitCheck][0] = (quint8)0xDF;
                   database[ptFloorLimitCheck][1] = (quint8)0x14;
                   database[ptFloorLimitCheck][2] = (quint8)0x01;
                   database[ptFloorLimitCheck][3] = (quint8)buffer[i+numByte+1];
                  break;

                case 0x15:
                   if (lengthTag != 1)
                   {
                       err = "DF15";
                       return false;
                   }
                   database[ptTACSupp][0] = (quint8)0xDF;
                   database[ptTACSupp][1] = (quint8)0x15;
                   database[ptTACSupp][2] = (quint8)0x01;
                   database[ptTACSupp][3] = (quint8)buffer[i+numByte+1];
                  break;

                case 0x21:
                    if (lengthTag !=5)
                    {
                        err = "DF21";
                        return false;
                    }
                    lengthTLV = 8;
                    for (quint32 j =0; j < lengthTLV; j++)
                    {
                        database[ptTACDenial][j] = (quint8)buffer[i+j];
                    }
                    break;

                case 0x22:
                    if (lengthTag !=5)
                    {
                        err = "DF22";
                        return false;
                    }
                    lengthTLV = 8;
                    for (quint32 j =0; j < lengthTLV; j++)
                    {
                        database[ptTACOnl][j] = (quint8)buffer[i+j];
                    }
                    break;

                case 0x20:
                    if (lengthTag !=5)
                    {
                        err = "DF20";
                        return false;
                    }
                    lengthTLV = 8;
                    for (quint32 j =0; j < lengthTLV; j++)
                    {
                        database[ptTACDef][j] = (quint8)buffer[i+j];
                    }
                    break;

                case 0x70:
                    if (lengthTag !=1)
                    {
                        err = "DF70";
                        return false;
                    }
                    database[ptTargPer][0] = (quint8)0xDF;
                    database[ptTargPer][1] = (quint8)0x70;
                    database[ptTargPer][2] = (quint8)0x01;
                    database[ptTargPer][3] = (quint8)buffer[i+numByte+1];
                    break;

                case 0x6E:
                    if (lengthTag != 3)
                    {
                        err = "DF6E";
                        return false;
                    }
                    lengthTLV = 3+ lengthTag;
                    for (quint32 j =0; j < lengthTLV; j++)
                    {
                        database[ptThreshVal][j] = (quint8)buffer[i+j];
                    }
                    break;

                case 0x6F:
                    if (lengthTag !=1)
                    {
                        err = "DF6F";
                        return false;
                    }
                    database[ptMaxTargPer][0] = (quint8)0xDF;
                    database[ptMaxTargPer][1] = (quint8)0x6F;
                    database[ptMaxTargPer][2] = (quint8)0x01;
                    database[ptMaxTargPer][3] = (quint8)buffer[i+numByte+1];
                    break;

                case 0x01:
                    if (lengthTag != 1)
                    {
                        err = "DF01";
                        return false;
                    }
                    database[ptDefDDOL][0] = (quint8)0xDF;
                    database[ptDefDDOL][1] = (quint8)0x01;
                    database[ptDefDDOL][2] = (quint8)0x01;
                    database[ptDefDDOL][3] = (quint8)buffer[i+numByte+1];
                    break;

                case 0x71:
                    if (lengthTag > 252)
                    {
                        err = "DF71";
                        return false;
                    }
                    if (numByte ==3)
                    {
                        lengthTLV = lengthTag +4;
                    }
                    else
                    {
                        lengthTLV = lengthTag + 3;
                    }

                    for (quint32 j =0; j< lengthTLV; j++)
                    {
                        database[ptDDOL][j] = (quint8)buffer[i+j];
                    }
                    break;

                case 0x02:
                    if (lengthTag != 1)
                    {
                        err = "DF02";
                        return false;
                    }
                    database[ptDefTDOL][0] = (quint8)0xDF;
                    database[ptDefTDOL][1] = (quint8)0x02;
                    database[ptDefTDOL][2] = (quint8)0x01;
                    database[ptDefTDOL][3] = (quint8)buffer[i+numByte+1];
                    break;

                case 0x72:
                    if (lengthTag > 252)
                    {
                        err = "DF72";
                        return false;
                    }
                    if (numByte == 3)
                    {
                        lengthTLV = lengthTag +4;
                    }
                    else
                    {
                        lengthTLV = lengthTag + 3;
                    }
                    for (quint32 j =0; j< lengthTLV; j++)
                    {
                        database[ptTDOL][j] = (quint8)buffer[i+j];
                    }

                    qDebug() << "numByte " << numByte << lengthTag;
                    break;
                }
                break;

            case 0x9F:
                switch (buffer[i+1])
                {
                case 0x1B:
                    if (lengthTag != 4)
                    {
                        err = "9F1B";
                        return false;
                    }
                    lengthTLV = lengthTag + 3;
                    for (quint32 j =0; j< lengthTLV; j++)
                    {
                        database[ptFloorLimit][j] = (quint8)buffer[i+j];
                    }
                    break;

                case 0x01:
                    if (lengthTag != 6)
                    {
                        err = "9F01";
                        return false;
                    }
                    lengthTLV = lengthTag + 3;
                    for (quint32 j =0; j< lengthTLV; j++)
                    {
                        database[ptAcquireId][j] = (quint8)buffer[i+j];
                    }
                    break;

                case 0x09:
                    if (lengthTag !=2)
                    {
                        err = "9F09";
                        return false;
                    }
                    lengthTLV = lengthTag + 3;
                    for (quint32 j =0; j< lengthTLV; j++)
                    {
                        database[ptAppVerNum][j] = (quint8)buffer[i+j];
                    }
                    break;
                }
                break;

            case 0x5F:
                switch(buffer[i+1])
                {
                case 0x2A:
                    if (lengthTag != 2)
                    {
                        err = "5F2A";
                        return false;
                    }
                    stringTmp += QString::number(buffer[i+1+numByte],16);
                    stringTmp += QString::number(buffer[i+1+numByte+1],16);
                    if ((stringTmp.toInt() ==0) && (stringTmp != "00"))
                    {
                        err = "5F2A";
                        return false;
                    }
                    lengthTLV = lengthTag +3;
                    for (quint32 j =0; j< lengthTLV; j++)
                    {
                        database[ptTranCurCode][j] = buffer[i+j];
                        qDebug() << "buffer " << buffer[i+j];
                    }
                    break;

                case 0x36:
                    if (lengthTag != 1)
                    {
                        err = "5F36";
                        return false;
                    }
                    lengthTLV = lengthTag +3;
                    for (quint32 j =0; j< lengthTLV; j++)
                    {
                        database[ptTranCurExpo][j] = (quint8)buffer[i+j];
                    }
                    break;
                }
                break;

            }
        }
        if (numByte == 1)
        {
            if (buffer[i] == 0x4f)
            {
                if (lengthTag > 0x10)
                {
                    err = "4f";
                    return false;
                }
                lengthTLV = lengthTag +2;
                for (quint32 j =0; j< lengthTLV; j++)
                {
                    database[ptAID][j] = (quint8)buffer[i+j];
                }
            }
        }
    }
    return true;

}

//void MainWindow::checkWidgetToUpdateDatabaseProcess(quint8 (*database)[MAX_COL], quint8 *lengthBuf, quint8 index)
//{
//    bool    ok;
//    quint8  tempNum, tempLength;
//    quint8  ith =0;

//    *lengthBuf =0;
//    qDebug() << "list " << m_listWidProcess.length() << "index " << index;
//    for (quint32 i =0; i< m_listWidProcess.length(); i++)
//    {
//       if (m_typeWidgetProcessing[i] == twSpinBox)
//       {
//               QSpinBox *spin = (QSpinBox*)m_listWidProcess.at(i);
//               *(database[index]+ith++) = (quint8)m_tagProcessing.at(i).mid(0,2).toInt(&ok, 16);
//               *(database[index]+ith++) = (quint8)m_tagProcessing.at(i).mid(2,2).toInt(&ok, 16);
//               *(database[index]+ith++) = (quint8)0x01;
//               *(database[index]+ith++) = (quint8)spin->value();
//               *lengthBuf +=4;
//       }

//       if (m_typeWidgetProcessing[i] == twCheckBox)
//       {
//           QCheckBox *check = (QCheckBox*)m_listWidProcess.at(i);
//           tempNum = (quint8)(check->checkState())?true:false;
//           *(database[index]+ith++) = (quint8)m_tagProcessing.at(i).mid(0,2).toInt(&ok, 16);
//           *(database[index]+ith++) = (quint8)m_tagProcessing.at(i).mid(2,2).toInt(&ok, 16);
//           *(database[index]+ith++) = (quint8)0x01;
//           *(database[index]+ith++) = tempNum;
//           *lengthBuf += 4;
//       }

//       if (m_typeWidgetProcessing[i] == twLineEdit)
//       {
//           if ((!ui->checkBox_TACProcess->checkState()) &&((i == 8) | (i == 9) | (i ==10)))
//           {
//               continue;
//           }

//           QLineEdit *line = (QLineEdit*)m_listWidProcess.at(i);
//           tempLength = line->text().length()/2;
//           *(database[index]+ith++) = (quint8)m_tagProcessing.at(i).mid(0,2).toInt(&ok, 16);
//           *(database[index]+ith++) = (quint8)m_tagProcessing.at(i).mid(2,2).toInt(&ok, 16);
//           if (i == 11)
//           {
//               *(database[index]+ith++) = 4;
//               for (quint32 i =0; i< 4; i++)
//               {
//                 *(database[index]+ith++) = (quint8)((line->text().toInt()&(0xff<< 8*(3-i)))>> (8*(3-i)));
//               }
//               *lengthBuf += 7;
//           }

//           else if (i == 13)
//           {
//               *(database[index]+ith++) = 3;
//               for (quint32 i =0; i< 3; i++)
//               {
//                 *(database[index]+ith++) = (quint8)((line->text().toInt()&(0xff<< 8*(2-i)))>> (8*(2-i)));
//               }
//               *lengthBuf += 6;
//           }
//           else
//           {
//                *(database[index]+ith++) = tempLength;

//               for (quint32 j =0; j < tempLength; j++)
//               {
//                   *(database[index]+ith++) = (quint8)line->text().mid(2*j, 2).toInt(&ok, 16);
//               }
//               *lengthBuf += 3+ tempLength;
//           }
//       }

//    }
//}

/**
 * @brief MainWindow::initializeBufferDatabase
 * each row is the tag followed the table in document.
 * init the value of tag in the table in the document.
 * @param database
 */
void MainWindow::initializeBufferDatabase(quint8 **database)
{
    for (quint32 i = 4; i< 9; i++)
    {
        database[i][0] = 0xDF;
        database[i][1] = (quint8)(0x0D+i);
        database[i][2] = 0x01;
        database[i][3] = 0x00;
    }

    database[ptAcquireId][0] = (quint8)0x9F;
    database[ptAcquireId][1] = 0x01;
    database[ptAcquireId][2] = 0x06;
    for (quint32 i =0; i< 6; i++)
    {
        database[ptAcquireId][3+i] = 0x00;
    }

    database[ptAID][0] = (quint8)0x4f;
    database[ptAID][1] = 0x00;
    database[ptAID][2] = 0x00;

    database[ptAppSelInd][0] = (quint8)0xDF;
    database[ptAppSelInd][1] = (quint8)0x7E;
    database[ptAppSelInd][2] = 0x01;
    database[ptAppSelInd][3] = 0x00;

    database[ptAppVerNum][0] = (quint8)0x9F;
    database[ptAppVerNum][1] = (quint8)0x09;
    database[ptAppVerNum][2] = 0x02;
    for (quint32 i =0; i< 2; i++)
    {
        database[ptAppVerNum][3+i] = 0x00;
    }

    database[ptTACDenial][0] = (quint8)0xdf;
    database[ptTACDenial][1] = (quint8)0x21;
    database[ptTACDenial][2] = 0x05;
    database[ptTACDenial][3] = 0x00;
    for (quint32 i =0; i< 5; i++)
    {
        database[ptTACDenial][3+i] = 0x00;
    }

    database[ptTACOnl][0] = (quint8)0xdf;
    database[ptTACOnl][1] = (quint8)0x22;
    database[ptTACOnl][2] = 0x05;
    database[ptTACOnl][3] = 0x00;
    for (quint32 i =0; i< 5; i++)
    {
        database[ptTACOnl][3+i] = 0x00;
    }

    database[ptTACDef][0] = (quint8)0xdf;
    database[ptTACDef][1] = (quint8)0x20;
    database[ptTACDef][2] = 0x05;
    for (quint32 i =0; i< 5; i++)
    {
        database[ptTACDef][3+i] = 0x00;
    }

    database[ptFloorLimit][0] = (quint8)0x9f;
    database[ptFloorLimit][1] = (quint8)0x1b;
    database[ptFloorLimit][2] = 0x04;
    for (quint32 i =0; i< 4; i++)
    {
        database[ptFloorLimit][3+i] = 0x00;
    }

    database[ptTargPer][0] = (quint8)0xdf;
    database[ptTargPer][1] = (quint8)0x70;
    database[ptTargPer][2] = 0x01;
    database[ptTargPer][3] = 0x00;

    database[ptThreshVal][0] = (quint8)0xdf;
    database[ptThreshVal][1] = (quint8)0x6e;
    database[ptThreshVal][2] = 0x03;
    for (quint32 i =0; i< 3; i++)
    {
        database[ptThreshVal][3+i] = 0x00;
    }

    database[ptMaxTargPer][0] = (quint8)0xdf;
    database[ptMaxTargPer][1] = (quint8)0x6f;
    database[ptMaxTargPer][2] = 0x01;
    database[ptMaxTargPer][3] = 0x00;

    database[ptDefDDOL][0] = (quint8)0xdf;
    database[ptDefDDOL][1] = (quint8)0x01;
    database[ptDefDDOL][2] = 0x01;
    database[ptDefDDOL][3] = 0x00;

    database[ptDDOL][0] = (quint8)0xdf;
    database[ptDDOL][1] = (quint8)0x71;
    database[ptDDOL][2] = 0x00;

    database[ptDefTDOL][0] = (quint8)0xdf;
    database[ptDefTDOL][1] = (quint8)0x02;
    database[ptDefTDOL][2] = 0x01;
    database[ptDefTDOL][3] = 0x00;

    database[ptTDOL][0] = (quint8)0xdf;
    database[ptTDOL][1] = (quint8)0x72;
    database[ptTDOL][2] = 0x00;

    database[ptTranCurCode][0] = (quint8)0x5f;
    database[ptTranCurCode][1] = (quint8)0x2a;
    database[ptTranCurCode][2] = 0x02;
    for (quint32 i =0; i< 2; i++)
    {
        database[ptTranCurCode][3+i] = 0x00;
    }

    database[ptTranCurExpo][0] = (quint8)0x5f;
    database[ptTranCurExpo][1] = (quint8)0x36;
    database[ptTranCurExpo][2] = 0x01;
    database[ptTranCurExpo][3] = 0x00;

}

/**
 * @brief MainWindow::changeLengthLineEdit
 * everytime type Backspace to delete the text, it will remain the length of text.
 * @param lineEdit
 * @param length
 */
void MainWindow::changeLengthLineEdit(QLineEdit *lineEdit, quint8 length)
{
  quint8 pos;
  QString stringTmp;
  bool    lineEditDeleted= false;

  pos = lineEdit->cursorPosition();
  stringTmp = lineEdit->text();
  while(stringTmp.length() < length)
  {
      stringTmp.insert(pos, QString("0"));
      lineEditDeleted = true;
      pos++;
  }
  lineEdit->setText(stringTmp);
  if (lineEditDeleted)
  {
      lineEdit->setCursorPosition(pos-1);
      lineEditDeleted = false;
  }
  else
  {
      lineEdit->setCursorPosition(pos);
  }

}

/**
 * @brief MainWindow::getAndCheckTagFF35Entry
 *
 * @param buffer
 * @param lengthTotal
 * @param err
 * @param lengthFF35
 * @param numRow
 * @return
 */
bool MainWindow::getAndCheckTagFF35Entry(quint8 *buffer, quint16 lengthTotal, QString &err,
                                         quint32 *lengthFF35, quint8 *numRow)
{
    quint16 lengthTmp;
    bool    state;
    lengthTmp = lengthTotal;
    quint16    index =0, numBytes, lengthTag =0, ithRow =0, lengthDf0e;
    quint8 numberOfSubsequence;
    bool     df0ePresent = false;
    QString  lineEditString;
    quint32  indexOfdf0E;
    QPair<QStringList, QStringList> stringLineEditAndDf0e;
    QList<quint8> listLengthDf0E;

    *numRow =0;
    *lengthFF35 =0;

    while(lengthTmp >0)
    {
        lengthTag = 0;
        /* find number of bytes of tag */
        if ((buffer[index] & 0x1F) == 0x1F){
            if (((index + 1) < lengthTotal) && ((buffer[index +1] & 0x80) == 0x80))
            {
                numBytes = 3;
            }
            else if ((index +1) < lengthTotal)
            {
                numBytes = 2;
            }

            else if ((index +1) == lengthTotal)
            {
                return true;
            }
        }

        else
        {
            numBytes = 1;
        }

        if (index + numBytes == lengthTotal){
            return true;
        }

        /* check if there is subsequence byte or not */
        if ((buffer[index+numBytes] & 0x80) == 0x80)
        {
            numberOfSubsequence = buffer[index+numBytes] & 0x7f;
            numBytes++;
            for (quint32 i =0; i< numberOfSubsequence; i++)
            {
                lengthTag += (quint8)buffer[index+numBytes+i] << (numberOfSubsequence-i-1)*8;
            }
            numBytes+= numberOfSubsequence-1;
        }
        else
        {
            lengthTag = buffer[index + numBytes];
        }

        if (lengthTag + 1 + numBytes > lengthTmp){
            err = "parse data";
            return false;
        }

        /* split ff35 and its value to check */
        if ((buffer[index]==0xff)&&(buffer[index+1] == 0x35))
        {
            quint8 *bufferTemp = new quint8[lengthTag];
            for (quint32 j =0; j< lengthTag; j++)
            {
                bufferTemp[j] = buffer[index+numBytes+1+j];
            }
            /* check parsing data and check df0e is present in bufferTemp, save tag df0f and its value
             *  into LineEditString  */
            state = checkTLVEntryPoint(bufferTemp, lengthTag,&indexOfdf0E,&df0ePresent, lineEditString); 
            if (false == state)
            {
                err = "parse data";
                delete[] bufferTemp;
                return false;
            }
            /*  add value of df0e into a stringList*/
            stringLineEditAndDf0e.first << lineEditString;

            /* add value of tag df0e into a string */
            if (df0ePresent)
            {
                QString   tmpString ="";
                lengthDf0e = bufferTemp[indexOfdf0E+2];
                listLengthDf0E << lengthDf0e;
                for (quint8 j =0; j < lengthDf0e; j++)
                {
                    if (bufferTemp[indexOfdf0E+3+j] < 0x0f) tmpString.append("0");
                    tmpString.append(QString::number(bufferTemp[indexOfdf0E+3+j],16));
                }
                stringLineEditAndDf0e.second << tmpString;
            }
            else
            {
                stringLineEditAndDf0e.second << "";
                listLengthDf0E << 0;
            }

            ithRow++;
            *lengthFF35++ = lengthTag;
            *(numRow) +=1;
            delete[] bufferTemp;
            lineEditString = "";
        }
        lengthTmp -=  (numBytes + lengthTag +1);
        index += numBytes + lengthTag + 1;
    }
    ListToolButton.clear();;
    ListLineEdit.clear();
    listComboKernelID.clear();
    listComboAID.clear();
    listComboTranType.clear();

    while(ui->tableWidget->rowCount() >0)
    {
        ui->tableWidget->removeRow(0);
    }

    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setRowCount(stringLineEditAndDf0e.first.length());
    for (quint16 i =0; i< stringLineEditAndDf0e.second.length(); i++)
    {
        insertComboBoxLineEditEntry(stringLineEditAndDf0e,i,listLengthDf0E.at(i));
    }

    return true;
}

/**
 * @brief MainWindow::checkTLVEntryPoint
 * check each ff35
 * check df0e is present in bufferTemp or not.
 * split df0f and its value to check parsing data.
 * @param bufferTemp
 * @param lengthTotal
 * @param indexDf0e
 * @param df0ePresent
 * @param stringLineEdit
 * @return
 */
bool MainWindow::checkTLVEntryPoint(quint8 *bufferTemp, quint16 lengthTotal, quint32 *indexDf0e,
                                    bool *df0ePresent, QString &stringLineEdit)
{
    quint16 numBytes = 0;
    quint16 lengthTemp, lengthTag =0;
    quint32 ith =0;
    bool state;
    quint8 numberOfSubsequences;
    *df0ePresent = false;

    lengthTemp = lengthTotal;

    while (lengthTemp >0){

        /* find the number of bytes of tag */
        if ((bufferTemp[ith] & 0x1F) == 0x1F)
        {
            if (((ith + 1) < lengthTotal) && (bufferTemp[ith +1] & 0x80) == 0x80)
            {
                numBytes = 3;
            }
            else if ((ith +1) < lengthTotal)
            {
                numBytes = 2;
            }

            else if ((ith +1) == lengthTotal)
            {
                return true;
            }
        }

        else
        {
            numBytes = 1;
        }

        if (ith + numBytes == lengthTotal){
            return true;
        }

        if ((bufferTemp[ith+numBytes] & 0x80) == 0x80)
        {
            numberOfSubsequences = bufferTemp[ith+numBytes] & 0x7f;
            numBytes++;
            for (quint32 i =0; i< numberOfSubsequences; i++)
            {
                lengthTag += (quint8)bufferTemp[ith+numBytes+i] << (numberOfSubsequences-i-1)*8;
            }
            numBytes+= numberOfSubsequences-1;
        }
        else
        {
            lengthTag = bufferTemp[ith + numBytes];
        }

        lengthTag = bufferTemp[ith + numBytes];

        if (lengthTag + 1 + numBytes > lengthTemp)
        {
            return false;
        }
        /* get index of tag DF0e in bufferTemp to use later */
        if (((bufferTemp[ith] & 0xdf)== 0xdf) && ((bufferTemp[ith+1] ^ 0x0e) == 0x00))
        {
            *indexDf0e = ith;
            *df0ePresent = true;
        }
         /* split tag df0f and its value to check parsing data */
        if (((bufferTemp[ith] & 0xdf) == 0xdf) && ((bufferTemp[ith+1] & 0x0f) == 0x0f))
        {
           quint8 *buffer = new quint8[lengthTag];
           for (quint32 j =0; j< lengthTag; j++)
           {
               buffer[j] = bufferTemp[ith+numBytes+1+j];
               if (buffer[j] < 0x0f) stringLineEdit.append("0");
               stringLineEdit.append(QString::number(buffer[j],16)).toUpper();
           }

           state = checkTLV(buffer, lengthTag, 0);
           if (!state)
           {
               delete[] buffer;
               return false;
           }
           delete[] buffer;
        }

        lengthTemp -=  (numBytes + lengthTag +1);
        ith += numBytes + lengthTag + 1;
    }
    return true;
}

/**
 * @brief MainWindow::insertComboBoxLineEditEntry
 * add string to lineEdit in ithRow of tableWidget
 * select index for Kernel Id combobox.
 * select index for AID Id combobox.
 * select index for Transaction Type.
 * @param stringList
 * @param ithRow
 * @param lengthDf0e
 */
void MainWindow::insertComboBoxLineEditEntry(QPair<QStringList, QStringList> stringList,
                                             quint16 ithRow, quint8 lengthDf0e)
{
    QComboBox *comboBoxKernelID = new QComboBox(ui->tableWidget);
    QStringList kernelIDName;

    /* init stringList of kernelIDName for comboBox kernelID */
    kernelIDName << "KERNEL01" << "MasterCard" << "Visa" << "American Express" << "JCB" << "Discover" << "C.U.P"
                         << "Interac FWM 3.1 " << "KERNEL09" << "KERNEL0A" << "KERNEL0B" << "KERNEL0C" << "KERNEL0D"
                         << "KERNEL0E" <<"KERNEL0F";

    for (int i =16; i<=64; i++){
        kernelIDName <<"KERNEL" + QString::number(i, 16).toUpper();
    }

    kernelIDName << "Interac (FMW 3.2) " << "Eftpos (FMW 3.2)" ;

    for (int i = 67; i<= 255; i++){
        kernelIDName << "KERNEL" + QString::number(i, 16).toUpper();
    }

    for (int i =0; i < kernelIDName.count(); i++){
        comboBoxKernelID->addItem(QString(kernelIDName.at(i)));
    }

    comboBoxKernelID->setStyleSheet("border:1px solid black;");
    comboBoxKernelID->setToolTip("Tag DF0E");
    ui->tableWidget->setCellWidget(ithRow, 0, comboBoxKernelID);

    /* init stringList for Transaction type comboBox*/
    QComboBox *comboBoxTransaction = new QComboBox(ui->tableWidget);
    QStringList transactionName;

    transactionName << "PURCHASE" << "CASH" << "PURCHASE WITH CASHBACK" << "REFUND" << "MANUAL CASH"
                    << "QUASI CASH" << "DEPOSIT" << "INQUIRY" << "PAYMENT" << "TRANSFER" << "ADMINISTRATIVE"
                    << "CLEAN" << "RETRIEVAL" << "UPDATE" << "AUTHENTICATION" << "CASH DISBURSEMENT" << "PRE AUTHORIZATION";

    for (int i =0; i < transactionName.count(); i++){
        comboBoxTransaction->addItem(QString(transactionName.at(i)));
    }
    comboBoxTransaction->setStyleSheet("border:1px solid black");
    comboBoxTransaction->setToolTip("Tag DF0E");
    ui->tableWidget->setCellWidget(ithRow, 2, comboBoxTransaction);

    /* init AID Id comboBox */
    QComboBox *comboBoxAID = new QComboBox(ui->tableWidget);
    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        comboBoxAID->addItem(QString::number(i+1));
    }

    comboBoxAID->setStyleSheet("border:1px solid black");
    comboBoxAID->setToolTip("Tag DF0E");
    ui->tableWidget->setCellWidget(ithRow, 1, comboBoxAID);

    /* init lineEdit */
    QLineEdit *LineEdit = new QLineEdit(ui->tableWidget);
    LineEdit->setToolTip("Tag FF35");
    ui->tableWidget->setCellWidget(ithRow, 3, LineEdit);

    /* add button for lineEdit */
    QToolButton *Button = new QToolButton(LineEdit);

    Button->setVisible(true);
    Button->setFixedSize(30, 20);
    Button->move(QPoint(5,5));
    Button->setStyleSheet("QToolButton{border: none; padding: 0px}");
    Button->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    Button->setIconSize(QSize(Button->width(), Button->height()));
    Button->setToolTip("Tag FF35");

    int frameWidth = LineEdit->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    LineEdit->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px;"
                                    " padding-left: %1px; } ").arg(Button->sizeHint().width() + frameWidth + 3));

    /* according to value of Tag DF0E, select index for every comboBox */
    if (stringList.second.at(ithRow) != "")
    {
        quint8 *bufferDf0e = new quint8[lengthDf0e];
        for (quint32 i =0; i< lengthDf0e; i++)
        {
            bufferDf0e[i] = (quint8)stringList.second.at(ithRow).mid(2*i,2).toInt(NULL, 16);
        }
        if (lengthDf0e == 0x03)
        {
            if (bufferDf0e[0] == 0x00)
            {
                comboBoxKernelID->setCurrentIndex(1);
            }
            else
            {
                comboBoxKernelID->setCurrentIndex(bufferDf0e[0]-1);
            }

            if ((bufferDf0e[1] <= m_numItemsProcess) && (bufferDf0e[1] >0))
            {
                comboBoxAID->setCurrentIndex(bufferDf0e[1]-1);
            }
            else
            {
                comboBoxAID->setCurrentIndex(0);
            }

            if (bufferDf0e[2] < transactionName.length())
            {
                comboBoxTransaction->setCurrentIndex(bufferDf0e[2]);
            }
            else
            {
                comboBoxTransaction->clear();
            }
        }
        else
        {
            comboBoxKernelID->setCurrentIndex(1);
            if (m_numItemsProcess >0) comboBoxAID->setCurrentIndex(0);
            comboBoxTransaction->setCurrentIndex(0);
        }
        delete[] bufferDf0e;
    }
    else
    {
        comboBoxKernelID->clear();
        comboBoxAID->clear();
        comboBoxTransaction->clear();
    }

    LineEdit->setText(stringList.first.at(ithRow).toUpper());

    ListToolButton << Button;
    ListLineEdit << LineEdit;
    listComboKernelID << comboBoxKernelID;
    listComboAID << comboBoxAID;
    listComboTranType << comboBoxTransaction;

    /* using sigMapper to emit signal whenever user clicks or change the value of comboBox */
    sigMapp->setMapping(ListToolButton[ithRow],ithRow);

    m_sigMappComboKerId->setMapping(listComboKernelID[ithRow], ithRow);
    m_sigMappComboAID->setMapping(listComboAID[ithRow], ithRow);
    m_sigMappComboTrans->setMapping(listComboTranType[ithRow], ithRow);
    m_sigMappLineEdit->setMapping(ListLineEdit[ithRow], ithRow);

    connect(listComboKernelID[ithRow], SIGNAL(currentIndexChanged(int)), m_sigMappComboKerId,
            SLOT(map()));
    connect(listComboAID[ithRow], SIGNAL(currentIndexChanged(int)), m_sigMappComboAID,
            SLOT(map()));
    connect(listComboTranType[ithRow], SIGNAL(currentIndexChanged(int)), m_sigMappComboTrans,
            SLOT(map()));
    connect(ListLineEdit[ithRow], SIGNAL(textChanged(QString)), m_sigMappLineEdit,
            SLOT(map()));

    connect(ListToolButton[ithRow],SIGNAL(clicked()), sigMapp, SLOT(map()));

    connect(sigMapp,SIGNAL(mapped(int)), this, SLOT(OpenNewWindow(int)));
    connect(m_sigMappComboKerId, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
    connect(m_sigMappComboAID, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
    connect(m_sigMappComboTrans, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
    connect(m_sigMappLineEdit, SIGNAL(mapped(int)), this, SLOT(lineEditEntryPointChanged()));
}

/**
 * @brief MainWindow::getPathForTerminalProcessingEntrypoint
 * save all the dir of terminal, processing, entrypoint into a xml file.
 * each time open the tab, get terminal, processing, entrypoint dir in that xml file.
 */
void MainWindow::getPathForTerminalProcessingEntrypoint()
{
    m_dir1 = QCoreApplication::applicationDirPath();
    bool resultCheck = false;

    m_fileXML    = m_dir1 + "/database.xml";

    /*check if there is terminal, processing, entrypoint dir in the xml file
     and get dir */
    resultCheck = m_readXML->readXMLFile(m_fileXML, m_terminalDir, m_processingDir, m_entryPointDir);
    if (!resultCheck)
    {
        ui->labelDir->setText("");
        ui->labelDirProcessing->setText("");
        ui->labelDirEntry->setText("");
        return;
    }

    /* read the processing file according to the dir.
       if there is not processing file or there are some errors in reading processing file,
       clear the label */
    resultCheck = loadProcessingFile(m_processingDir, 0);
    if (resultCheck)
    {
        ui->labelDirProcessing->setText(m_processingDir);
        ui->ButtonSaveProcessing->setEnabled(false);
        m_dir1 = updateDir(m_processingDir);
    }
    else
    {
        ui->labelDirProcessing->clear();
    }

    /* read the entryPoint file according to the dir.
       if there is not entryPoint file or there are some errors in reading entryPoint file,
       clear the label */
    resultCheck = loadEntryPointFile(m_entryPointDir, 0);
    if (resultCheck)
    {
        ui->labelDirEntry->setText(m_entryPointDir);
        m_dir1 = updateDir(m_entryPointDir);
    }
    else
    {
        ui->labelDirEntry->clear();
    }

    /* read the terminal file according to the dir.
       if there is not terminal file or there are some errors in reading terminal file,
       clear the label */
    resultCheck = loadTerminalFile(m_terminalDir, 0);
    if (resultCheck)
    {
        ui->labelDir->setText(m_terminalDir);
        ui->ButtonSave->setEnabled(false);
        m_dir1 = updateDir(m_terminalDir);
    }
    else
    {
        ui->labelDir->clear();
    }

}

/**
 * @brief MainWindow::loadTerminalFile
 * check the file opened or not, update directory to terminal binary file.
 * check the content of file.
 * sha check
 * check data is parsed or not
 * update state for all widget in terminal tab
 * @param fileName
 * @param initiOrLoad
 * @return
 */
bool MainWindow::loadTerminalFile(const QString &fileName, bool initiOrLoad)
{
    QString tempString, stringCheck;
    QByteArray dataRead;
    bool    state;
    QString err;

    /* check file */
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        if (fileName != "")
        {
            if (initiOrLoad)
            {
                QMessageBox::warning(this, "warning","Cannnot open file!");
            }
        }
        return false;
    }

    updateDir(fileName);

    dataRead = file.readAll();

    if (dataRead.length() == 0)
    {
        if (initiOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n"
                                                 "size error on 00");
        }
        return false ;
    }
    if ((dataRead.length() < 21)| ((quint8)dataRead.at(0) != 0xaa))
    {
        QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n"
                                                 "size error on 00");
        return false;
    }

    unsigned char* bufferTemp = new unsigned char[dataRead.length()-21];

    for (qint32 i =21; i < dataRead.length(); i++)
    {
       bufferTemp[i-21] = (unsigned char)dataRead.at(i);
    }

    /*check SHA of data */
    tSHA  ct;
    unsigned char resultSHA[20] ={0};
    ShaInit(&ct);
    ShaUpdate(&ct, bufferTemp,dataRead.length()-21);
    ShaFinal(&ct,resultSHA);

    for (qint32 i =0; i< 20; i++)
    {
        stringCheck += QString::number(resultSHA[i], 16);
        tempString  += QString::number((unsigned char)dataRead.at(i+1), 16);
    }

    if (stringCheck != tempString)
    {
        if (initiOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n "
                                                 "wrong sha on 00");
        }
        return false;
    }
   /* check parsing data */
    state = checkTLV(bufferTemp, dataRead.length()-21,0);

    if (!state)
    {
        if (initiOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n"
                                                 "wrong parse data on 00");
        }
        return false;
    }
    /* update state for all widget in terminal */
    state = checkTagAvai(bufferTemp, dataRead.length()-21, err);
    delete[]bufferTemp;
    if (!state)
    {
        if (initiOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use TERMINAL file content:\n"
                                                 "wrong read data " + err + "\n " + err.toLower());
        }
        return false;
    }


    file.close();
    return true;
}

/**
 * @brief MainWindow::loadProcessingFile
 * load file to read data in file
 * check sha key of data
 * save data into two dimension array.
 * @param fileName
 * @param initOrLoad
 * @return
 */
bool MainWindow::loadProcessingFile(const QString &fileName, bool initOrLoad)
{
    QByteArray dataRead;
    QString tempString, stringCheck;
    bool   state;
    QString error;
    quint8  index4F;

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        if ((fileName != "") && (initOrLoad))
        {
            QMessageBox::warning(this, "File error", "Cannot read the file");
        }
        return false;
    }

    dataRead = file.readAll();
    if ((0 == dataRead.length()))
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error","Unable to read or use PROCESSING file content:\n "
                                                                                  "size error on 00");
        }
        return false;
    }

    if ((dataRead.length() < 21) || ((quint8)dataRead.at(0) != 0xaa))
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use PROCESSING file content:\n"
                                                 "size error on 00");
        }
        return false;
    }

    unsigned char* bufferTemp = new unsigned char[dataRead.length()-21];
    for (qint32 i =21; i < dataRead.length(); i++)
    {
       bufferTemp[i-21] = (unsigned char)dataRead.at(i);
    }

    /* check sha key */
    tSHA  ct;
    unsigned char resultSHA[20] ={0};
    ShaInit(&ct);
    ShaUpdate(&ct, bufferTemp, dataRead.length()-21);
    ShaFinal(&ct,resultSHA);

    for (qint32 i =0; i< 20; i++)
    {
        stringCheck += QString::number(resultSHA[i], 16);
        tempString  += QString::number((unsigned char)dataRead.at(i+1), 16);
    }

    if ((stringCheck != tempString))
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use PROCESSING file content:\n "
                                                 "wrong sha on 00");
        }
        delete[] bufferTemp;
        return false;
    }

    for (quint32 i =0; i< MAX_COL; i++)
    {
        m_lengthOfEachItemListWidgetProcessing[i] = 0;
    }

    m_numItemsProcess = 0;
    ui->listWidgetProcessing->clear();

    state = getAndCheckAIDProcessing(bufferTemp, dataRead.length()-21,error,
                                     m_lengthOfEachItemListWidgetProcessing, &m_numItemsProcess);

    if ((!state)&&(error == "parse data"))
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use PROCESSING file content:\n"
                                                 "wrong parse data on 00");
        }
        delete[] bufferTemp;
        return false;
    }

    else if ((!state) && (error != "") && (error != "parse data"))
    {
        QMessageBox::warning(this, "ENTRY_POINT coherence", error);
    }

    for (int i =0; i < m_listWidProcess.length(); i++)
    {
        m_listWidProcess.at(i)->setEnabled(false);
        ui->toolButtonDefaultProcessing->setEnabled(false);
        ui->toolButtonDenialProcessing->setEnabled(false);
        ui->toolButtonOnlineProcessing->setEnabled(false);
        ui->listWidgetProcessing->clear();
        ui->label_Acquire->setEnabled(false);
        ui->label_AVN->setEnabled(false);
        ui->label_Denial->setEnabled(false);
        ui->label_Online->setEnabled(false);
        ui->label_Default->setEnabled(false);
        ui->label_Floor->setEnabled(false);
        ui->label_DDOL->setEnabled(false);
        ui->label_TDOL->setEnabled(false);
        ui->label_Target->setEnabled(false);
        ui->label_MaxTarget->setEnabled(false);
        ui->label_TCC->setEnabled(false);
        ui->label_Expo->setEnabled(false);
        ui->label_Thresh->setEnabled(false);

    }

    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        QString tempString ="";
        getIndexOfTag4F(m_databaseArray[i], m_lengthOfEachItemListWidgetProcessing[i], &index4F);
        for (quint32 j =0; j< m_databaseArray[i][index4F +1]; j++)
        {
            if (m_databaseArray[i][index4F +2+j] < 0x0F)
            {
                tempString.append("0");
            }
            tempString.append(QString::number(m_databaseArray[i][index4F+2+j],16).toUpper());
        }

        QListWidgetItem *item = new QListWidgetItem(tempString);
        ui->listWidgetProcessing->addItem(item);
    }

    delete[] bufferTemp;
    file.close();
    return true;
}

/**
 * @brief MainWindow::loadEntryPointFile
 * open file in ReadOnly mode
 * check sha key
 * get check the FF35 to update all the widget in entry point tab
 * @param fileName
 * @param initOrLoad
 * @return
 */
bool MainWindow::loadEntryPointFile(const QString &fileName, bool initOrLoad)
{
    QByteArray dataRead;
    QString tempString, stringCheck;
    QString error;
    bool state;

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        if (fileName != "")
        {
            if (initOrLoad)
            {
                QMessageBox::warning(this, "File error", "Cannot read the file");
            }
        }
        return false;
    }

    dataRead = file.readAll();
    qDebug() << "length " << dataRead.length();
    if (0 == dataRead.length())
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error","Unable to read or use ENTRYPOINT file content:\n "
                                                                                  "size error on 00");
        }
        return false;
    }
    if ((dataRead.length() < 21) || ((quint8)dataRead.at(0) != 0xaa))
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use ENTRYPOINT file content:\n"
                                                 "size error on 00");
        }
        return false;
    }

    unsigned char* bufferTemp = new unsigned char[dataRead.length()-21];

    for (qint32 i =21; i < dataRead.length(); i++)
    {
       bufferTemp[i-21] = (unsigned char)dataRead.at(i);
    }
    /* check the sha key */
    tSHA  ct;
    unsigned char resultSHA[20] ={0};
    ShaInit(&ct);
    ShaUpdate(&ct, bufferTemp, dataRead.length()-21);
    ShaFinal(&ct,resultSHA);

    for (qint32 i =0; i< 20; i++)
    {
        stringCheck += QString::number(resultSHA[i], 16);
        tempString  += QString::number((unsigned char)dataRead.at(i+1), 16);
    }

    if (stringCheck != tempString)
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use ENTRY POINT file content:\n "
                                                 "wrong sha on 00");
        }
        delete[] bufferTemp;
        return false;
    }

    for (quint32 i =0; i< MAX_COL; i++)
    {
        m_lengthOfEachItemEntryPoint[i] = 0;
    }

    m_numRows = 0;

    /* update comboBox, lineEdit of EntryPoint configuration tab */
    state = getAndCheckTagFF35Entry(bufferTemp,dataRead.length()-21,error,m_lengthOfEachItemEntryPoint,
                                    &m_numRows);

    if ((!state)&&(error == "parse data"))
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error", "Unable to read or use ENTRY POINT file content:\n"
                                                 "wrong parse data on 00");
        }

        delete[] bufferTemp;
        return false;
    }

    else if (!state)
    {
        if (initOrLoad)
        {
            QMessageBox::warning(this, "File error","Unable to read or use ENTRY POINT file content:\n"
                                                "size error on " + error+ "\n" + error.toLower());
        }
        delete[]  bufferTemp;
        return false;
    }

    if (m_numItemsProcess ==0)
    {
        QString warningString;
        for (int i =0; i< ui->tableWidget->rowCount(); i++)
        {
            warningString += "modified AID Id on row " +QString::number(i) +
                    " because it exceeds the number of AIDs in PROCESSING configuration \n";
        }
        QMessageBox::warning(this, "ENTRY_POINT coherence",warningString);
    }

    file.close();
    ui->ButtonEntrySave->setEnabled(true);
    return true;
}

/*
void MainWindow::checkCheckboxAndLineEditTer(QStringList listTagCheck, QString tagTer,
                                             mlsTypeWidget_t typeWidget, QCheckBox *checkBoxWid,
                                             QSpinBox *spinWid, QLineEdit *lineWid, QComboBox *comboWid)
{
    bool widgetPresent;
    for (qint32 i =0; i < listTagCheck.length(); i++)
    {
        if (tagTer == listTagCheck.at(i))
        {
            widgetPresent = true;
            break;
        }
    }

    if (!widgetPresent)
    {
        switch (typeWidget)
        {
            case twCheckBox:
                checkBoxWid->setChecked(false);
            break;
            case twLineEdit:
                 lineWid->setText("0000000000");
            break;
            case twSpinBox:
                 spinWid->setValue(0);
            break;
            case twComboBox:
                 comboWid->setCurrentIndex(0);
            break;

        }
    }
} */
/*
void MainWindow::checkCheckboxAndLineEditTer(QStringList listTagCheck, QStringList listTag,
                                             mlsTypeWidget_t typeWidget, QList<QWidget *> *listWid)
{

    for (qint32 i =0; i < listTag.length(); i++)
    {
        bool tagPresent = false;
        for (qint32 j =0; j < listTagCheck.length(); j++)
        {
            if (listTag.at(i) == listTagCheck.at(j))
            {
                tagPresent = true;
                break;
            }
        }

        if (!tagPresent)
        {
            switch (typeWidget) {
            case twCheckBox:
                 QCheckBox *checkbox = (QCheckBox*)listWid->at(i);
                 checkbox->setChecked(false);
                break;
            case twLineEdit:
                 QLineEdit *lineEd = (QLineEdit*)listWid->at(i);
                 lineEd->setText("0000000000");
                break;
            case twComboBox:
                 QComboBox *combo = (QComboBox*)listWid->at(i);
                 combo->setCurrentIndex(0);
                break;
            case twSpinBox:
                 QSpinBox *spinBox = (QSpinBox*)listWid->at(i);
                 spinBox->setValue(0);
                break;
            default:
                break;
            }
        }
    }
}

*/

/**
 * @brief MainWindow::on_toolButtonAddProcessing_clicked
 */
void MainWindow::on_toolButtonAddProcessing_clicked()
{
    quint8 index ;
    quint8 length =0;
    quint32 lengthTLV;
    bool ok;

    /* get string in the lineEdit to add into listWidget */
    if (ui->lineEditMainProcessing->text() != "")
    {
        QString task = ui->lineEditMainProcessing->text().toUpper();
        if (task.length()%2 != 0)
        {
            task = "";
        }

        QListWidgetItem *item1 = new QListWidgetItem(task);
        ui->listWidgetProcessing->addItem(item1);
        ui->lineEditMainProcessing->clear();
        ui->listWidgetProcessing->setCurrentItem(item1);
        index = ui->listWidgetProcessing->currentRow();

        for (quint32 i =0; i< MAX_COL; i++)
        {
            m_databaseArray[index][i] = 0x00;
        }

        /* plus 1 for the number of item in listWidget */
        m_numItemsProcess ++;

        /* init two dimension array database */
        quint8 **database;
        database = new quint8*[MAX_ROW];
        for (quint32 i =0; i< MAX_ROW; i++)
        {
            database[i] = new quint8[MAX_COL];
        }

        initializeBufferDatabase(database);

        QString stringtmp;
        for (quint32 i =0; i< 9; i++)
        {
            if (database[ptAcquireId][i] < 0x0f) stringtmp.append("0");
            stringtmp.append(QString::number(database[ptAcquireId][i],16));
        }

        /* update the row ptAID of database */
        if (task != "")
        {
          length = task.length()/2;
          database[ptAID][1] = length;

          for (quint32 i =0; i< length; i++)
          {
              database[ptAID][2+i] = (quint8)(task.mid(2*i,2).toInt(&ok,16));
          }
        }
        /* store in to index row of database */
        storeTagToDatabaseProcess(m_databaseArray,index,database,&lengthTLV);

        m_lengthOfEachItemListWidgetProcessing[index] = lengthTLV;

        /* add item for comboBox AID Id in entryPoint */
        quint32 numberOfRowsEntry;
        numberOfRowsEntry = ui->tableWidget->rowCount();
        for (quint32 i=0; i< numberOfRowsEntry; i++)
        {
            QComboBox *combo = (QComboBox*)ui->tableWidget->cellWidget(i,1);
            combo->addItem(QString::number(m_numItemsProcess));
        }
    }
}

/**
 * @brief MainWindow::on_toolButtonDenialProcessing_clicked
 */
void MainWindow::on_toolButtonDenialProcessing_clicked()
{
    Def->show();
    LineEditChosed = mproTACDen;
    emit Button_7_Clicked(ui->lineEditDenialProcessing->text(), wdMainWindow);
}

/**
 * @brief MainWindow::on_toolButtonOnlineProcessing_clicked
 */
void MainWindow::on_toolButtonOnlineProcessing_clicked()
{
    Def->show();
    LineEditChosed = mproTACOnl;
    emit ButtonOnline_Clicked(ui->lineEditOnlineProcessing->text(), wdMainWindow);
}

/**
 * @brief MainWindow::on_toolButtonEntryUp_clicked
 */

void MainWindow::on_toolButtonEntryUp_clicked()
{
    int selectRow = ui->tableWidget->currentRow();
    if (selectRow >0){
        QComboBox *comboKernelID;
        QComboBox *comboKernelIDAbove;
        QComboBox *comboAID;
        QComboBox *comboAIDAbove;
        QComboBox *comboTranType;
        QComboBox *comboTranTypeAbove;
        QLineEdit *lineEdit;
        QLineEdit *lineEditAbove;
        QString   text;
        QString   textAbove;

        comboKernelID = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 0);
        comboAID      = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 1);
        comboTranType = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 2);
        lineEdit      = (QLineEdit*)ui->tableWidget->cellWidget(selectRow, 3);

        comboKernelIDAbove = (QComboBox*)ui->tableWidget->cellWidget(selectRow-1, 0);
        comboAIDAbove      = (QComboBox*)ui->tableWidget->cellWidget(selectRow-1, 1);
        comboTranTypeAbove = (QComboBox*)ui->tableWidget->cellWidget(selectRow-1, 2);
        lineEditAbove      = (QLineEdit*)ui->tableWidget->cellWidget(selectRow-1, 3);

        /*switch value of two row of tableWidget */
        text      = comboKernelID->currentText();
        textAbove = comboKernelIDAbove->currentText();

        comboKernelID->setCurrentText(textAbove);
        comboKernelIDAbove->setCurrentText(text);

        text      = comboAID->currentText();
        textAbove = comboAIDAbove->currentText();

        comboAID->setCurrentText(textAbove);
        comboAIDAbove->setCurrentText(text);

        text      = comboTranType->currentText();
        textAbove = comboTranTypeAbove->currentText();

        comboTranType->setCurrentText(textAbove);
        comboTranTypeAbove->setCurrentText(text);

        text      = lineEdit->text();
        textAbove = lineEditAbove->text();

        lineEdit->setText(textAbove);
        lineEditAbove->setText(text);
        if (m_numItemsProcess ==0)
        {
            QString warningString;
            for (int i =0; i< ui->tableWidget->rowCount(); i++)
            {
                warningString += "modified AID Id on row " +QString::number(i) +
                        " because it exceeds the number of AIDs in PROCESSING configuration \n";
            }
            QMessageBox::warning(this, "ENTRY_POINT coherence",warningString);
        }

    }
}

void MainWindow::on_toolButtonEntryDown_clicked()
{
    int selectRow = ui->tableWidget->currentRow();
    if (selectRow <ui->tableWidget->rowCount()-1){
        QComboBox *comboKernelID;
        QComboBox *comboKernelIDBelow;
        QComboBox *comboAID;
        QComboBox *comboAIDBelow;
        QComboBox *comboTranType;
        QComboBox *comboTranTypeBelow;
        QLineEdit *lineEdit;
        QLineEdit *lineEditBelow;
        QString   text;
        QString   textBelow;

        comboKernelID = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 0);
        comboAID      = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 1);
        comboTranType = (QComboBox*)ui->tableWidget->cellWidget(selectRow, 2);
        lineEdit      = (QLineEdit*)ui->tableWidget->cellWidget(selectRow, 3);

        comboKernelIDBelow = (QComboBox*)ui->tableWidget->cellWidget(selectRow+1, 0);
        comboAIDBelow      = (QComboBox*)ui->tableWidget->cellWidget(selectRow+1, 1);
        comboTranTypeBelow = (QComboBox*)ui->tableWidget->cellWidget(selectRow+1, 2);
        lineEditBelow      = (QLineEdit*)ui->tableWidget->cellWidget(selectRow+1, 3);

        text      = comboKernelID->currentText();
        textBelow = comboKernelIDBelow->currentText();

        comboKernelID->setCurrentText(textBelow);
        comboKernelIDBelow->setCurrentText(text);

        text      = comboAID->currentText();
        textBelow = comboAIDBelow->currentText();

        comboAID->setCurrentText(textBelow);
        comboAIDBelow->setCurrentText(text);

        text      = comboTranType->currentText();
        textBelow = comboTranTypeBelow->currentText();

        comboTranType->setCurrentText(textBelow);
        comboTranTypeBelow->setCurrentText(text);

        text      = lineEdit->text();
        textBelow = lineEditBelow->text();

        lineEdit->setText(textBelow);
        lineEditBelow->setText(text);
        if (m_numItemsProcess ==0)
        {
            QString warningString;
            for (int i =0; i< ui->tableWidget->rowCount(); i++)
            {
                warningString += "modified AID Id on row " +QString::number(i) +
                        " because it exceeds the number of AIDs in PROCESSING configuration \n";
            }
            QMessageBox::warning(this, "ENTRY_POINT coherence",warningString);
        }

    }
}

void MainWindow::on_toolButtonEntryAdd_clicked()
{
    int insertRow = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(insertRow);
    qDebug() <<"row insert " << insertRow;
    QComboBox *comboBoxKernelID = new QComboBox(ui->tableWidget);
    QStringList kernelIDName;
    kernelIDName << "KERNEL01" << "MasterCard" << "Visa" << "American Express" << "JCB" << "Discover" << "C.U.P"
                 << "Interac FWM 3.1 " << "KERNEL09" << "KERNEL0A" << "KERNEL0B" << "KERNEL0C" << "KERNEL0D"
                 << "KERNEL0E" <<"KERNEL0F";

    for (int i =16; i<=64; i++){
        kernelIDName <<"KERNEL" + QString::number(i, 16).toUpper();
    }

    kernelIDName << "Interac (FMW 3.2) " << "Eftpos (FMW 3.2)" ;

    for (int i = 67; i<= 255; i++){
        kernelIDName << "KERNEL" + QString::number(i, 16).toUpper();
    }

    for (int i =0; i < kernelIDName.count(); i++){
        comboBoxKernelID->addItem(QString(kernelIDName.at(i)));
    }

    ui->tableWidget->setCellWidget(insertRow, 0, comboBoxKernelID);
    comboBoxKernelID->setCurrentText("MasterCard");

    QComboBox *comboBoxTransaction = new QComboBox(ui->tableWidget);
    QStringList transactionName;

    transactionName << "PURCHASE" << "CASH" << "PURCHASE ...CASHBACK" << "REFUND" << "MANUAL CASH"
                    << "QUASI CASH" << "DEPOSIT" << "INQUIRY" << "PAYMENT" << "TRANSFER" << "ADMINISTRATIVE"
                    << "CLEAN" << "RETRIEVAL" << "UPDATE" << "AUTHENTICATION" << "CASH DISBURSEMENT" << "PRE AUTHORIZATION";

    for (int i =0; i < transactionName.count(); i++){
        comboBoxTransaction->addItem(QString(transactionName.at(i)));
    }

    ui->tableWidget->setCellWidget(insertRow, 2, comboBoxTransaction);

    QComboBox *comboBoxAID = new QComboBox(ui->tableWidget);
    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        comboBoxAID->addItem(QString::number(i+1));
    }
    if (m_numItemsProcess ==0)
    {
        QString warningString;
        for (int i =0; i< ui->tableWidget->rowCount(); i++)
        {
            warningString += "modified AID Id on row " +QString::number(i) +
                    " because it exceeds the number of AIDs in PROCESSING configuration \n";
        }
        QMessageBox::warning(this, "ENTRY_POINT coherence",warningString);
    }

    ui->tableWidget->setCellWidget(insertRow, 1, comboBoxAID);

    QLineEdit *LineEdit = new QLineEdit(ui->tableWidget);
    LineEdit->setText("");

    ui->tableWidget->setCellWidget(insertRow, 3, LineEdit);

    QToolButton *Button = new QToolButton(LineEdit);
    Button->move(QPoint(5,5));
    Button->setVisible(true);
    Button->setFixedSize(30, 20);
    Button->setStyleSheet("QToolButton{border: none; padding: 0px}");
    Button->setIcon(QIcon(":/image_open_save/image/pencil.png"));
    Button->setIconSize(QSize(Button->width(), Button->height()));
    Button->setToolTip("Tag FF35");

    qDebug() << "position x " << Button->x();
    qDebug() << "position y  " << Button->y();
    int frameWidth = LineEdit->style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    LineEdit->setStyleSheet(QString("QLineEdit {border: 1px solid black;border-radius:3px; padding-left: %1px; } ").arg(Button->sizeHint().width() + frameWidth + 3));

    ListToolButton << Button;
    ListLineEdit   << LineEdit;
    listComboKernelID << comboBoxKernelID;
    listComboAID    << comboBoxAID;
    listComboTranType << comboBoxTransaction;

    sigMapp->setMapping(ListToolButton[insertRow], insertRow);

    m_sigMappComboKerId->setMapping(listComboKernelID[insertRow], insertRow);
    m_sigMappComboAID->setMapping(listComboAID[insertRow], insertRow);
    m_sigMappComboTrans->setMapping(listComboTranType[insertRow], insertRow);
    m_sigMappLineEdit->setMapping(ListLineEdit[insertRow], insertRow);

    connect(ListToolButton[insertRow], SIGNAL(clicked()), sigMapp, SLOT(map()));
    connect(listComboKernelID[insertRow], SIGNAL(currentIndexChanged(int)), m_sigMappComboKerId,
            SLOT(map()));
    connect(listComboAID[insertRow], SIGNAL(currentIndexChanged(int)), m_sigMappComboAID,
            SLOT(map()));
    connect(listComboTranType[insertRow], SIGNAL(currentIndexChanged(int)), m_sigMappComboTrans,
            SLOT(map()));
    connect(ListLineEdit[insertRow], SIGNAL(textChanged(QString)), m_sigMappLineEdit,
            SLOT(map()));

    connect(sigMapp, SIGNAL(mapped(int)), this, SLOT(OpenNewWindow(int)));
    connect(m_sigMappComboKerId, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
    connect(m_sigMappComboAID, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
    connect(m_sigMappComboTrans, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
    connect(m_sigMappLineEdit, SIGNAL(mapped(int)), this, SLOT(lineEditEntryPointChanged()));

}

void MainWindow::on_toolButtonEntryRemove_clicked()
{
    QString         textAsk;
    int currentRow = ui->tableWidget->currentRow();
    QMessageBox::StandardButton reply;

    textAsk = "Do you want to remove the entry point in line ";
    textAsk += QString::number(currentRow +1) + " ?";
    reply = QMessageBox::question(this, "Remove entry point", textAsk, QMessageBox::Yes|
                          QMessageBox::Cancel);

    if (reply == QMessageBox::Yes)
    {
        ui->tableWidget->removeRow(currentRow);

        listComboAID.removeAt(currentRow);
        listComboKernelID.removeAt(currentRow);
        listComboTranType.removeAt(currentRow);
        ListToolButton.removeAt(currentRow);
        ListLineEdit.removeAt(currentRow);

        for (int i =0; i< ListToolButton.length(); i++)
        {
            sigMapp->setMapping(ListToolButton[i],i);
            m_sigMappComboKerId->setMapping(listComboKernelID[i], i);
            m_sigMappComboAID->setMapping(listComboAID[i], i);
            m_sigMappComboTrans->setMapping(listComboTranType[i], i);
            m_sigMappLineEdit->setMapping(ListLineEdit[i], i);

            connect(listComboKernelID[i], SIGNAL(currentIndexChanged(int)), m_sigMappComboKerId,
                    SLOT(map()));
            connect(listComboAID[i], SIGNAL(currentIndexChanged(int)), m_sigMappComboAID,
                    SLOT(map()));
            connect(listComboTranType[i], SIGNAL(currentIndexChanged(int)), m_sigMappComboTrans,
                    SLOT(map()));
            connect(ListLineEdit[i], SIGNAL(textChanged(QString)), m_sigMappLineEdit,
                    SLOT(map()));

            connect(ListToolButton[i],SIGNAL(clicked()), sigMapp, SLOT(map()));
        }

        connect(sigMapp,SIGNAL(mapped(int)), this, SLOT(OpenNewWindow(int)));
        connect(m_sigMappComboKerId, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
        connect(m_sigMappComboAID, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
        connect(m_sigMappComboTrans, SIGNAL(mapped(int)), this, SLOT(comboEntryPointChanged(int)));
        connect(m_sigMappLineEdit, SIGNAL(mapped(int)), this, SLOT(lineEditEntryPointChanged()));

    }
    if (m_numItemsProcess ==0)
    {
        QString warningString;
        for (int i =0; i < ui->tableWidget->rowCount();i++)
        {
            warningString += "modified AID Id on row " +QString::number(i) +
                    " because it exceeds the number of AIDs in PROCESSING configuration \n";
        }
        QMessageBox::warning(this, "ENTRY_POINT coherence",warningString);
    }


}

/**
 * @brief MainWindow::on_ButtonEntrySave_clicked
 * open file in WriteOnly mode
 * according to the value and state of all widget, save value to buffer
 */
void MainWindow::on_ButtonEntrySave_clicked()
{
    QString fileName ;
    quint16 numberOfRows;
    quint8  valueBytes;
    quint16 lengthLine;

    fileName = ui->labelDirEntry->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
       QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

    numberOfRows = ui->tableWidget->rowCount();
    quint16 *lengthList = new quint16[numberOfRows];
    /* create a two dimension array */
    quint8 **tmpBuffer;
    tmpBuffer = new quint8*[numberOfRows];
    for (quint32 i =0; i< numberOfRows; i++)
    {
        tmpBuffer[i] = new quint8[MAX_COL];
    }

    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint32 index =0;
        QComboBox *comboKernel = (QComboBox*)ui->tableWidget->cellWidget(i, 0);
        QComboBox *comboID = (QComboBox*)ui->tableWidget->cellWidget(i,1);
        QComboBox *comboTrans = (QComboBox*)ui->tableWidget->cellWidget(i,2);
        QLineEdit *line = (QLineEdit*)ui->tableWidget->cellWidget(i,3);

        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0e;
        tmpBuffer[i][index++] = (quint8)0x03;
        valueBytes = comboKernel->currentIndex();
        tmpBuffer[i][index++] = (quint8)(valueBytes+1);

        valueBytes = comboID->currentIndex();
        if (comboID->count() ==0)
        {
            tmpBuffer[i][index++] = (quint8)0x01;
        }
        else
        {
            tmpBuffer[i][index++] = (quint8)(valueBytes+1);
        }

        valueBytes = comboTrans->currentIndex();
        tmpBuffer[i][index++] = (quint8)valueBytes;
        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0f;

        /* get the length of text in LineEdit */
        lengthLine = line->text().length()/2;
        if ((lengthLine > 0x7f) && (lengthLine < 0xff))
        {
            tmpBuffer[i][index++] = (quint8)0x81;
            tmpBuffer[i][index++] = lengthLine;
        }
        else if (lengthLine >0xff)
        {
            tmpBuffer[i][index++] = (quint8)0x82;
            tmpBuffer[i][index++] = (lengthLine &0xff00) >> 8;
            tmpBuffer[i][index++] = (quint8)lengthLine;
        }
        else
        {
            tmpBuffer[i][index++] = lengthLine;
        }

        for (quint32 j =0; j< lengthLine; j++)
        {
            tmpBuffer[i][index++] = (quint8)line->text().mid(2*j,2).toInt(NULL, 16);
        }
        /* save length of each row in to an array */
        lengthList[i] = index;
    }

    quint32 lengthTotal =0;

    /* check each element of lengthList to decide number of bytes of tag */
    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint8 numberOfBytes;
        qDebug() << "length list " << QString::number(lengthList[i],16);
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
           numberOfBytes = 4;
        }
        else if (lengthList[i] > 0xff)
        {
            numberOfBytes = 5;
        }
        else
        {
            numberOfBytes = 3;
        }
        qDebug() << "number of bytes " << numberOfBytes;
        lengthTotal += lengthList[i] + numberOfBytes;
    }

    /* create bufferFin to get all value of TLV */
    quint8 *bufferFin = new quint8[lengthTotal];
    quint32 index =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        bufferFin[index++] = (quint8)0xff;
        bufferFin[index++] = (quint8)0x35;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
            bufferFin[index++] = (quint8)0x81;
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else if (lengthList[i] > 0xff)
        {
            bufferFin[index++] = (quint8)0x82;
            bufferFin[index++] = (quint8)((lengthList[i] & 0xff00) >>8);
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else
        {
            bufferFin[index++] = (quint8)lengthList[i];
        }
        for (quint32 j =0; j< lengthList[i]; j++)
        {
            bufferFin[index++] = tmpBuffer[i][j];
        }
    }

    for (quint32 i =0; i< numberOfRows; i++)
    {
        delete[] tmpBuffer[i];
    }
    delete[] tmpBuffer;
    delete[] lengthList;

    /* get sha key */
    tSHA ct;
    quint8 resultSHA[20];
    ShaInit(&ct);
    ShaUpdate(&ct,bufferFin,lengthTotal);
    ShaFinal(&ct,resultSHA);

    QDataStream out(&file);
    out << (quint8)0xaa;
    for (quint32 i =0; i< 20; i++)
    {
        out << resultSHA[i];
    }
    for (quint32 i =0; i< lengthTotal; i++)
    {
        out << bufferFin[i];
    }
    delete[] bufferFin;
    file.flush();
    file.close();
    ui->ButtonEntrySave->setEnabled(false);
}

/**
 * @brief MainWindow::on_ButtonOpen_clicked
 * button Open in terminal tab is pressed
 */
void MainWindow::on_ButtonOpen_clicked()
{
    QString dirOpen;
    bool    resultCheck = false;
    QString fileName;

    dirOpen = m_dir1;
    /* open the terminal binary file */
    fileName = QFileDialog::getOpenFileName(this,
                                              "Open TERMINAL binary file", dirOpen);

    resultCheck = loadTerminalFile(fileName, 1);

    /* update dir into xml database */
    if (resultCheck)
    {
        ui->labelDir->setText(fileName);
        ui->ButtonSave->setEnabled(false);
        m_dir1 = updateDir(fileName);
        m_terminalDir = fileName;
        m_readXML->writeXMLFile(m_fileXML, m_terminalDir,m_processingDir, m_entryPointDir);
    }

}

/**
 * @brief MainWindow::on_ButtonOpenProcessing_clicked
 * open the processing binary file.
 */
void MainWindow::on_ButtonOpenProcessing_clicked()
{
    QString dirOpen;
    QString fileName;
    bool    resultCheck = false;

    dirOpen = m_dir1;
    fileName = QFileDialog::getOpenFileName(this,
                                              "Open PROCESSING binary file", dirOpen);

    resultCheck = loadProcessingFile(fileName,1);
    if (resultCheck)
    {
        ui->labelDirProcessing->setText(fileName);
        ui->ButtonSaveProcessing->setEnabled(false);
        m_processingDir = fileName;
        m_dir1 = updateDir(fileName);
        m_readXML->writeXMLFile(m_fileXML,m_terminalDir,m_processingDir,m_entryPointDir);
    }
}

/**
 * @brief MainWindow::on_ButtonEntryOpen_clicked
 */
void MainWindow::on_ButtonEntryOpen_clicked()
{
    QString dirOpen;
    bool    resultCheck = false;
    dirOpen = m_dir1;
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open ENTRY POINT binary file", dirOpen);

    resultCheck = loadEntryPointFile(fileName,1);
    if (resultCheck)
    {
        ui->labelDirEntry->setText(fileName);
        m_entryPointDir = fileName;
        m_dir1 = updateDir(fileName);
        m_readXML->writeXMLFile(m_fileXML,m_terminalDir,m_processingDir,m_entryPointDir);
    }
}

/**
 * @brief MainWindow::on_ButtonSaveAs_clicked
 *
 */
void MainWindow::on_ButtonSaveAs_clicked()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save TERMINAL binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this, "File error", "Unable to open file " + fileName);
        return;
    }

    QDataStream in(&file);
    quint8 buffer[200] = {0};
    quint8 outSHA[20]  = {0};
    quint32 lengthTotal;
    tSHA  ct;

    updateBufferToSaveTer(buffer, 23, &lengthTotal);

    ShaInit(&ct);
    ShaUpdate(&ct, buffer, lengthTotal);
    ShaFinal(&ct, outSHA);

    in << (quint8)0xaa;
    for (quint8 i =0; i< 20; i++)
    {
        in << outSHA[i];
    }
    for (quint8 i =0; i < lengthTotal; i++)
    {
        in << buffer[i];
    }
    file.close();
}

void MainWindow::on_ButtonSaveAsProcessing_clicked()
{
    QString dir, fileName;
    quint32 lengthTotalByteTags =0;
    quint32 lengthBuf =0, valTmp =0;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save PROCESSING binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this, "File error", "Cannot write PROCESSING File");
        return;
    }

    quint8 *numberOfBytes = new quint8[m_numItemsProcess];

    for(quint32 i =0; i< m_numItemsProcess; i++)
    {
         lengthBuf += m_lengthOfEachItemListWidgetProcessing[i];
         if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
         {
             numberOfBytes[i] = 4;
         }
         else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
         {
             numberOfBytes[i] = 5;
         }
         else
         {
             numberOfBytes[i] = 3;
         }
         lengthTotalByteTags += numberOfBytes[i];
    }

    unsigned char *buffer = new unsigned char[lengthBuf + lengthTotalByteTags];

    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        buffer[valTmp++] = (unsigned char)0xff;
        buffer[valTmp++] = (unsigned char)0x33;

        if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
        {
            buffer[valTmp++] = (unsigned char)0x81;
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
        {
            buffer[valTmp++] = (unsigned char)0x82;
            buffer[valTmp++] = (unsigned char)((m_lengthOfEachItemListWidgetProcessing[i] & 0xff00) >> 8);
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else
        {
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }

        for (quint32 j =0; j< m_lengthOfEachItemListWidgetProcessing[i]; j++)
        {
          buffer[valTmp++] = (unsigned char)m_databaseArray[i][j];
        }
    }

    QDataStream in(&file);
    unsigned char outSHA[20]  = {0};
    tSHA   ct;
    ShaInit(&ct);
    ShaUpdate(&ct,buffer, lengthBuf+lengthTotalByteTags);
    ShaFinal(&ct,outSHA);

    in << (quint8)0xaa;
    for (quint32 i =0; i< 20; i++)
    {
        in << (quint8)outSHA[i];
    }
    for (quint32 i =0; i < lengthBuf+ lengthTotalByteTags; i++)
    {
        in << (quint8)buffer[i];
    }

    delete[] buffer;
    delete[] numberOfBytes;
    file.close();
    ui->ButtonSaveProcessing->setEnabled(false);



}

void MainWindow::on_ButtonEntrySaveAs_clicked()
{
    QString dir, fileName;
    quint16 numberOfRows;
    quint8  valueBytes;
    quint16 lengthLine;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save ENTRY POINT binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        return;
    }
    numberOfRows = ui->tableWidget->rowCount();
    quint16 *lengthList = new quint16[numberOfRows];
    quint8 **tmpBuffer;
    tmpBuffer = new quint8*[numberOfRows];
    for (quint32 i =0; i< numberOfRows; i++)
    {
        tmpBuffer[i] = new quint8[MAX_COL];
    }

    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint32 index =0;
        QComboBox *comboKernel = (QComboBox*)ui->tableWidget->cellWidget(i, 0);
        QComboBox *comboID = (QComboBox*)ui->tableWidget->cellWidget(i,1);
        QComboBox *comboTrans = (QComboBox*)ui->tableWidget->cellWidget(i,2);
        QLineEdit *line = (QLineEdit*)ui->tableWidget->cellWidget(i,3);

        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0e;
        tmpBuffer[i][index++] = (quint8)0x03;
        valueBytes = comboKernel->currentIndex();
        tmpBuffer[i][index++] = (quint8)(valueBytes+1);

        valueBytes = comboID->currentIndex();
        if (comboID->count() ==0)
        {
            tmpBuffer[i][index++] = (quint8)0x01;
        }
        else
        {
            tmpBuffer[i][index++] = (quint8)(valueBytes+1);
        }

        valueBytes = comboTrans->currentIndex();
        tmpBuffer[i][index++] = (quint8)valueBytes;
        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0f;

        lengthLine = line->text().length()/2;
        if ((lengthLine > 0x7f) && (lengthLine < 0xff))
        {
            tmpBuffer[i][index++] = (quint8)0x81;
            tmpBuffer[i][index++] = lengthLine;
        }
        else if (lengthLine >0xff)
        {
            tmpBuffer[i][index++] = (quint8)0x82;
            tmpBuffer[i][index++] = (lengthLine &0xff00) >> 8;
            tmpBuffer[i][index++] = (quint8)lengthLine;
        }
        else
        {
            tmpBuffer[i][index++] = lengthLine;
        }
        qDebug() << "text " << line->text();
        for (quint32 j =0; j< lengthLine; j++)
        {
            tmpBuffer[i][index] = (quint8)line->text().mid(2*j,2).toInt(NULL, 16);
            qDebug() << "string " << line->text().mid(2*j,2) << tmpBuffer[i][index];
            index += 1;
        }
        lengthList[i] = index;
    }

    quint32 lengthTotal =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint8 numberOfBytes;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
           numberOfBytes = 4;
        }
        else if (lengthList[i] > 0xff)
        {
            numberOfBytes = 5;
        }
        else
        {
            numberOfBytes = 3;
        }
        lengthTotal += lengthList[i] + numberOfBytes;
    }

    quint8 *bufferFin = new quint8[lengthTotal];
    quint32 index =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        bufferFin[index++] = (quint8)0xff;
        bufferFin[index++] = (quint8)0x35;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
            bufferFin[index++] = (quint8)0x81;
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else if (lengthList[i] > 0xff)
        {
            bufferFin[index++] = (quint8)0x82;
            bufferFin[index++] = (quint8)((lengthList[i] & 0xff00) >>8);
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else
        {
            bufferFin[index++] = (quint8)lengthList[i];
        }
        for (quint32 j =0; j< lengthList[i]; j++)
        {
            bufferFin[index++] = tmpBuffer[i][j];
        }
    }
    QString stringt;
    for(quint32 i =0; i< lengthTotal; i++)
    {
        if (bufferFin[i] <=0x0f) stringt.append("0");
        stringt.append(QString::number(bufferFin[i],16));
    }
    qDebug() << stringt;

    for (quint32 i =0; i< numberOfRows; i++)
    {
        delete[] tmpBuffer[i];
    }
    delete[] tmpBuffer;
    delete[] lengthList;

    tSHA ct;
    quint8 resultSHA[20];
    ShaInit(&ct);
    ShaUpdate(&ct,bufferFin,lengthTotal);
    ShaFinal(&ct,resultSHA);

    QDataStream out(&file);
    out << (quint8)0xaa;
    for (quint32 i =0; i< 20; i++)
    {
        out << resultSHA[i];
    }
    for (quint32 i =0; i< lengthTotal; i++)
    {
        out << bufferFin[i];
    }
    delete[] bufferFin;
    file.flush();
    file.close();
    ui->ButtonEntrySave->setEnabled(false);
}

void MainWindow::on_checkBoxAutorun_clicked()
{
    if (1 == ui->checkBoxAutorun->isChecked())
    {
        ui->spinBox->setEnabled(true);
    }
    else
    {
        ui->spinBox->setEnabled(false);
    }
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_toolButtonRemoveProcessing_clicked()
{
    quint32 currIndex = ui->listWidgetProcessing->currentRow();
    if (currIndex >=0)
    {
        QListWidgetItem* item = ui->listWidgetProcessing->currentItem();
        delete ui->listWidgetProcessing->takeItem(ui->listWidgetProcessing->row(item));
        for (int j = currIndex; j < m_numItemsProcess-1; j++)
        {
            for (quint32 i =0; i < MAX_COL; i++)
            {
                m_databaseArray[j][i] = m_databaseArray[j+1][i];
            }
            m_lengthOfEachItemListWidgetProcessing[j] = m_lengthOfEachItemListWidgetProcessing[j+1];
        }

        m_numItemsProcess--;
        m_lengthOfEachItemListWidgetProcessing[m_numItemsProcess] = 0;

        if (0 == m_numItemsProcess)
        {
            for (quint32 i =0; i < 21; i++)
            {
                m_listWidProcess.at(i)->setEnabled(false);
                ui->toolButtonDefaultProcessing->setEnabled(false);
                ui->toolButtonDenialProcessing->setEnabled(false);
                ui->toolButtonOnlineProcessing->setEnabled(false);
                ui->listWidgetProcessing->clear();
                ui->label_Acquire->setEnabled(false);
                ui->label_AVN->setEnabled(false);
                ui->label_Denial->setEnabled(false);
                ui->label_Online->setEnabled(false);
                ui->label_Default->setEnabled(false);
                ui->label_Floor->setEnabled(false);
                ui->label_DDOL->setEnabled(false);
                ui->label_TDOL->setEnabled(false);
                ui->label_Target->setEnabled(false);
                ui->label_MaxTarget->setEnabled(false);
                ui->label_TCC->setEnabled(false);
                ui->label_Expo->setEnabled(false);
                ui->label_Thresh->setEnabled(false);

            }
        }

        quint32 numberOfRows;
        numberOfRows = ui->tableWidget->rowCount();
        QString warningString;
        for (quint32 i =0; i< numberOfRows; i++)
        {
            QComboBox *combo = (QComboBox*)ui->tableWidget->cellWidget(i,1);
            combo->removeItem(m_numItemsProcess);
            if (m_numItemsProcess ==0)
            {
                warningString += "modified AID on row " + QString::number(i) + " because it exceeds the"
                                                                               "number of AIDs in PROCESSING"
                                                                               " configuration\n";
            }
        }
        if (warningString != "")
        {
            QMessageBox::warning(this, "ENTRY POINT coherence", warningString);
        }

    }
}

void MainWindow::on_toolButtonUpProcessing_clicked()
{
    QListWidgetItem* itemCurrent = ui->listWidgetProcessing->currentItem();
    int currentIndex             = ui->listWidgetProcessing->row(itemCurrent);
    quint8 *bufferTmp            = new quint8[MAX_COL];
    quint32 valTmp =0;

    if (currentIndex >0)
    {
        QListWidgetItem* itemTemp = ui->listWidgetProcessing->takeItem(currentIndex -1);
        ui->listWidgetProcessing->insertItem(currentIndex -1, itemCurrent);
        ui->listWidgetProcessing->insertItem(currentIndex, itemTemp);
        for (quint32 i =0; i< MAX_COL; i++)
        {
            bufferTmp[i] = m_databaseArray[currentIndex][i];
            m_databaseArray[currentIndex][i] = m_databaseArray[currentIndex-1][i];
            m_databaseArray[currentIndex-1][i] = bufferTmp[i];
        }
        valTmp = m_lengthOfEachItemListWidgetProcessing[currentIndex];
        m_lengthOfEachItemListWidgetProcessing[currentIndex] = m_lengthOfEachItemListWidgetProcessing[currentIndex-1];
        m_lengthOfEachItemListWidgetProcessing[currentIndex-1] = valTmp;
    }
    delete[] bufferTmp;
}

void MainWindow::on_toolButtonDownProcessing_clicked()
{
    int numRows = ui->listWidgetProcessing->count();
    QListWidgetItem *itemCurrent = ui->listWidgetProcessing->currentItem();
    int currentIndex = ui->listWidgetProcessing->row(itemCurrent);
    quint8 *bufferTmp = new quint8[MAX_COL];
    quint8 valTmp =0;

    if (currentIndex < numRows-1)
    {
        QListWidgetItem *itemAfter = ui->listWidgetProcessing->takeItem(currentIndex +1);

        ui->listWidgetProcessing->insertItem(currentIndex +1, itemCurrent);
        ui->listWidgetProcessing->insertItem(currentIndex, itemAfter);
        for (quint32 i =0; i< MAX_COL; i++)
        {
            bufferTmp[i] = m_databaseArray[currentIndex][i];
            m_databaseArray[currentIndex][i] = m_databaseArray[currentIndex+1][i];
            m_databaseArray[currentIndex+1][i] = bufferTmp[i];
        }

        valTmp = m_lengthOfEachItemListWidgetProcessing[currentIndex];
        m_lengthOfEachItemListWidgetProcessing[currentIndex] = m_lengthOfEachItemListWidgetProcessing[currentIndex+1];
        m_lengthOfEachItemListWidgetProcessing[currentIndex+1] = valTmp;
    }
    delete[] bufferTmp;
}

void MainWindow::on_checkBox_TACProcess_clicked()
{
    quint32 length;
    quint8 **database;
    bool   state;
    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }
    state = (ui->checkBox_TACProcess->checkState())?true:false;
    ui->lineEditDenialProcessing->setEnabled(state);
    ui->lineEditDefaultProcessing->setEnabled(state);
    ui->lineEditOnlineProcessing->setEnabled(state);
    ui->toolButtonDefaultProcessing->setEnabled(state);
    ui->toolButtonOnlineProcessing->setEnabled(state);
    ui->toolButtonDenialProcessing->setEnabled(state);


    quint8 index = ui->listWidgetProcessing->currentRow();
    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptTACSupp][3] = state;
    qDebug() << "tac supp " << database[ptTACSupp][3];
    storeTagToDatabaseProcess(m_databaseArray, index,database,&length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_COL; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_toolButtonDefaultProcessing_clicked()
{
    Def->show();
    LineEditChosed = mproTACDef;
    emit ButtonDefault_Clicked(ui->lineEditDefaultProcessing->text(), wdMainWindow);
}

void MainWindow::on_listWidgetProcessing_itemChanged(QListWidgetItem *item)
{
   (void)&item;
}

void MainWindow::on_listWidgetProcessing_itemClicked(QListWidgetItem *item)
{
    quint8 index ;
    quint32 numBytes =0, lengthTag =0;
    QString textDDOL, textTDOL;

    index= ui->listWidgetProcessing->row(item);
    for (int i =0; i< m_listWidProcess.length(); i++)
    {
        m_stateTagProcessing[i] = false;
    }

    ui->toolButtonDefaultProcessing->setEnabled(false);
    ui->toolButtonDenialProcessing->setEnabled(false);
    ui->toolButtonOnlineProcessing->setEnabled(false);
    ui->lineEditOnlineProcessing->setEnabled(false);
    ui->lineEditDefaultProcessing->setEnabled(false);
    ui->lineEditDenialProcessing->setEnabled(false);

    qDebug() << "m length item " << m_lengthOfEachItemListWidgetProcessing[index];

    for (quint32 i =0; i< m_lengthOfEachItemListWidgetProcessing[index]; i+= numBytes+1+lengthTag)
    {
        numBytes = 0;
        if ((m_databaseArray[index][i] & 0x1F) == 0x1F)
        {
            if ((m_databaseArray[index][i+1] & 0x80) == 0x80)
            {
                numBytes = 3;
            }
            else
            {
                numBytes = 2;
            }
        }
        else
        {
            numBytes = 1;
        }

        if ((m_databaseArray[index][i+numBytes] & 0x81) == 0x81)
        {
            numBytes++;
        }
        lengthTag = m_databaseArray[index][i+numBytes];

        // test String
        QString stringTest;
        for (quint32 k =0; k < 200; k++)
        {
            if (m_databaseArray[index][k] < 0x0f)
            {
                stringTest += "0";
            }
            stringTest += QString::number(m_databaseArray[index][k],16);
        }
        qDebug() << "string " << stringTest;
        updateCheckBoxLineProcess(i,numBytes,m_databaseArray[index],lengthTag, textDDOL, textTDOL);
    }

    ui->textEdit_DDOL->setText(textDDOL);
    ui->textEdit_TDOL->setText(textTDOL);

    for(qint32 i =0; i< m_listWidProcess.length(); i++)
    {
        if (!m_stateTagProcessing[i])
        {
            if (m_typeWidgetProcessing[i]== twCheckBox)
            {
                QCheckBox *checkbox = (QCheckBox*)m_listWidProcess.at(i);
                checkbox->setChecked(false);
            }

            if (m_typeWidgetProcessing[i] == twLineEdit)
            {
               QLineEdit *lineEd = (QLineEdit*)m_listWidProcess.at(i);
               if (i ==11 || i == 13 || i == 19)
               {
                 lineEd->setText("0");
               }
               else if (i == 18)
               {
                   lineEd->setText("0000");
               }
               else
               {
                  lineEd->setText("0000000000");
               }
            }

            if (m_typeWidgetProcessing[i] == twSpinBox)
            {
                QSpinBox *spinBox = (QSpinBox*)m_listWidProcess.at(i);
                spinBox->setValue(0);
            }

            if (m_typeWidgetProcessing[i] == twTextEdit)
            {
                QTextEdit *textEdit = (QTextEdit*)m_listWidProcess.at(i);
                textEdit->clear();
            }
        }
    }
    ui->ButtonSaveProcessing->setEnabled(true);

}

void MainWindow::on_toolButtonLoadDEK_clicked()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToDataStorage();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open DEK DET xml file", dirOpen, "Files(*.xml)");
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->labelXMLFile->setText(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    ui->textEditData->setText(text);
    file.close();
}

void MainWindow::on_toolButtonClearDEK_clicked()
{
    ui->textEditData->clear();
    ui->labelXMLFile->clear();
}

/**
 * @brief MainWindow::on_ButtonSave_clicked
 * open file to save in WriteOnly mode.
 * get sha check of data to save into file.
 */
void MainWindow::on_ButtonSave_clicked()
{
    QString fileName ;

    fileName = ui->labelDir->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this, "Warning", "Cannot open " + fileName);
        return;
    }

    QDataStream in(&file);
    quint8 buffer[200] = {0};
    quint8 outSHA[20]  = {0};
    quint32 lengthTotal;
    tSHA  ct;

    /* according to state of all widget in terminal tab, get the length, value of each tag. save
       it to buffer */
    updateBufferToSaveTer(buffer, 23, &lengthTotal);
    qDebug() << "length save " << lengthTotal;
    /* get the sha key of data */
    ShaInit(&ct);
    ShaUpdate(&ct, buffer, lengthTotal);
    ShaFinal(&ct, outSHA);

    //string test
    QString stringTest;
    for (quint32 i =0; i < 20; i++)
    {
        if (outSHA[i] <= 0x0f)
        {
            stringTest += "0";
        }
        stringTest += QString::number(outSHA[i],16);
    }
    qDebug() << "test save " << stringTest;

    in << (quint8)0xaa;
    for (quint8 i =0; i< 20; i++)
    {
        in << outSHA[i];
    }

    for (quint8 i =0; i < lengthTotal; i++)
    {
        in << buffer[i];
    }
    file.close();
    ui->ButtonSave->setEnabled(false);
}

void MainWindow::on_checkBox_TTQ_clicked()
{
    if (ui->checkBox_TTQ->checkState())
    {
        ui->lineEdit_defaultTTQ->setEnabled(true);
        ui->toolButton_TTQ->setEnabled(true);
    }
    else
    {
        ui->lineEdit_defaultTTQ->setEnabled(false);
        ui->toolButton_TTQ->setEnabled(false);
    }
}

void MainWindow::on_checkBox_Referral_clicked()
{
    if (ui->checkBox_Referral->checkState())
    {
        ui->comboBox_ManageACE->setEnabled(true);
    }
    else
    {
        ui->comboBox_ManageACE->setEnabled(false);
    }
}

void MainWindow::on_toolButton_DRLAdd_clicked()
{
    QString Task = ui->lineEdit_DRL->text().toUpper();
    if (!QString(Task).isEmpty()){
    QListWidgetItem *item1 = new QListWidgetItem(Task);
    ui->listWidget_DRL->addItem(item1);
    ui->lineEdit_DRL->clear();
    }
}

void MainWindow::on_toolButton_DRLRemove_clicked()
{
    QListWidgetItem* item = ui->listWidget_DRL->currentItem();
    delete ui->listWidget_DRL->takeItem(ui->listWidget_DRL->row(item));
}

void MainWindow::on_toolButton_DRLUp_clicked()
{
    QListWidgetItem* itemCurrent = ui->listWidget_DRL->currentItem();
    int currentIndex             = ui->listWidget_DRL->row(itemCurrent);

    if (currentIndex >0)
    {
        QListWidgetItem* itemTemp = ui->listWidget_DRL->takeItem(currentIndex -1);
        ui->listWidget_DRL->insertItem(currentIndex -1, itemCurrent);
        ui->listWidget_DRL->insertItem(currentIndex, itemTemp);
    }
}

void MainWindow::on_toolButton_DRLDown_clicked()
{
    int numRows = ui->listWidget_DRL->count();
    QListWidgetItem *itemCurrent = ui->listWidget_DRL->currentItem();
    int currentIndex = ui->listWidget_DRL->row(itemCurrent);

    if (currentIndex < numRows-1)
    {
        QListWidgetItem *itemAfter = ui->listWidget_DRL->takeItem(currentIndex +1);

        ui->listWidget_DRL->insertItem(currentIndex +1, itemCurrent);
        ui->listWidget_DRL->insertItem(currentIndex, itemAfter);
    }
}

void MainWindow::on_toolButton_DRLOpen_clicked()
{
    QString dirOpen;

    dirOpen = stringDir.getDirToOpenProcessing();
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open DRL set file", dirOpen);
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        return;
    }

    ui->label_DRLDir->setText(fileName);
    stringDir.updateValueDirTerProcess(fileName);

    QTextStream in(&file);
    QString text = in.readAll();
    file.close();

}

void MainWindow::on_toolButton_DRLSaveAs_clicked()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save DRL set file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        return;
    }
    file.close();
}

/**
 * @brief MainWindow::on_ButtonSaveProcessing_clicked
 * open file in WriteOnly mode.
 *
 */
void MainWindow::on_ButtonSaveProcessing_clicked()
{
    QString fileName ;
    quint32 lengthBuf =0;
    quint32 valTmp =0;
    quint32 lengthTotalByteTags =0;

    fileName = ui->labelDirProcessing->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

    quint8 *numberOfBytes = new quint8[m_numItemsProcess];

    for(quint32 i =0; i< m_numItemsProcess; i++)
    {
        /* update length of all data */
         lengthBuf += m_lengthOfEachItemListWidgetProcessing[i];

         /* if there are subsequence in length of tag */
         if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
         {
             numberOfBytes[i] = 4;
         }
         else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
         {
             numberOfBytes[i] = 5;
         }
         else
         {
             numberOfBytes[i] = 3;
         }
         lengthTotalByteTags += numberOfBytes[i];
    }

    unsigned char *buffer = new unsigned char[lengthBuf + lengthTotalByteTags];
    QString stringTmp;

    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        /* there are m_numItemsProcess ff33 in file */
        buffer[valTmp++] = (unsigned char)0xff;
        buffer[valTmp++] = (unsigned char)0x33;

        if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
        {
            buffer[valTmp++] = (unsigned char)0x81;
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
        {
            buffer[valTmp++] = (unsigned char)0x82;
            buffer[valTmp++] = (unsigned char)((m_lengthOfEachItemListWidgetProcessing[i] & 0xff00) >> 8);
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else
        {
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        /* add data of ith row in m_databaseArray into buffer */
        for (quint32 j =0; j< m_lengthOfEachItemListWidgetProcessing[i]; j++)
        {
          buffer[valTmp++] = (unsigned char)m_databaseArray[i][j];
        }
    }

    /* get sha key of buffer */
    QDataStream in(&file);
    unsigned char outSHA[20]  = {0};
    tSHA   ct;
    ShaInit(&ct);
    ShaUpdate(&ct,buffer, lengthBuf+lengthTotalByteTags);
    ShaFinal(&ct,outSHA);

    in << (quint8)0xaa;
    for (quint32 i =0; i< 20; i++)
    {
        in << (quint8)outSHA[i];
    }
    /*write the data into file*/
    for (quint32 i =0; i < lengthBuf+ lengthTotalByteTags; i++)
    {
        in << (quint8)buffer[i];
    }

    delete[] buffer;
    delete[] numberOfBytes;
    file.close();
    ui->ButtonSaveProcessing->setEnabled(false);
}

void MainWindow::on_toolButton_DRLSave_clicked()
{
    QString fileName ;

    fileName = ui->label_DRLDir->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

//    QTextStream out(&file);
//    out << ;
    file.flush();
    file.close();
}

void MainWindow::on_actionExit_Ctrl_Q_triggered()
{
   aceExit->show();
}

void MainWindow::on_actionExport_triggered()
{

}

void MainWindow::on_actionSend_CA_Keys_triggered()
{

    QString dirOpen;
    QByteArray dataRead;
    bool    resultCheck = false;
    QString fileName;
    quint32 lengthTotal;
    QString stringToConvertToByteArray;
    QByteArray  byteArrayPrepend, byteTemp;

    dirOpen = m_dir1;
    /* open the terminal binary file */
    fileName = QFileDialog::getOpenFileName(this,
                                              "CA Keys Download", dirOpen);

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        if (fileName != "")
        {
            QMessageBox::warning(this, "warning","Cannnot open file!");
        }
        return;
    }

    updateDir(fileName);

    dataRead = file.readAll();

    quint32 tag70h;

    tag70h = 1;
    lengthTotal = dataRead.length() +1;
    qDebug() << "length total " << lengthTotal;

    quint8 *buffer = new quint8[dataRead.length()];
    QString stringTest;
    for (quint32 i =0; i< dataRead.length(); i++)
    {
        buffer[i] = (quint8)dataRead.at(i);
        if (buffer[i] <= 0x0f)
            stringTest += "0";
        stringTest += QString::number(buffer[i],16);
    }
    qDebug() << "test " << stringTest;

    if (((quint8)(lengthTotal >> 8) & 0xff) <= 0x0f)
    {
        stringToConvertToByteArray += "0";
    }
    stringToConvertToByteArray += QString::number((quint8)(lengthTotal >> 8),16);
    qDebug() << "string 1 " << stringToConvertToByteArray;

    if (((quint8)lengthTotal &0xff) <= 0x0f)
    {
        stringToConvertToByteArray += "0";
    }
    stringToConvertToByteArray += QString::number((quint8)lengthTotal,16);

    stringToConvertToByteArray += "70";
    for (quint32 i =0; i< dataRead.length(); i++)
    {
        if (buffer[i] <= 0x0f)
        {
            stringToConvertToByteArray += "0";
        }
        stringToConvertToByteArray += QString::number(buffer[i],16);
    }

    byteTemp = stringToConvertToByteArray.toLatin1();
    byteArrayPrepend = QByteArray::fromHex(byteTemp);
    dataRead.prepend(byteArrayPrepend);

    QString ipAdd  = ui->lineEdit_AddressSystem->text();
    QString portNum = ui->lineEdit_PortSystem->text();
    m_socket->getData(byteArrayPrepend,ipAdd,portNum.toInt(),tsWindowTabCA);
    emit startToSendTCP();
    delete[] buffer;
    file.close();

}

void MainWindow::on_actionSend_Revocated_CA_Keys_triggered()
{
    QString dirOpen;
    QByteArray dataRead;
    bool    resultCheck = false;
    QString fileName;
    quint32 lengthTotal;
    QString stringToConvertToByteArray;
    QByteArray  byteArrayPrepend, byteTemp;

    dirOpen = m_dir1;
    /* open the terminal binary file */
    fileName = QFileDialog::getOpenFileName(this,
                                              "Revocated CA Keys Download", dirOpen);

    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
    {
        if (fileName != "")
        {
            QMessageBox::warning(this, "warning","Cannnot open file!");
        }
        return;
    }

    updateDir(fileName);

    dataRead = file.readAll();

    quint32 tag6Bh;

    tag70h = 1;
    lengthTotal = dataRead.length() +1;
    qDebug() << "length total " << lengthTotal;

    quint8 *buffer = new quint8[dataRead.length()];
    QString stringTest;
    for (quint32 i =0; i< dataRead.length(); i++)
    {
        buffer[i] = (quint8)dataRead.at(i);
        if (buffer[i] <= 0x0f)
            stringTest += "0";
        stringTest += QString::number(buffer[i],16);
    }
    qDebug() << "test " << stringTest;

    if (((quint8)(lengthTotal >> 8) & 0xff) <= 0x0f)
    {
        stringToConvertToByteArray += "0";
    }
    stringToConvertToByteArray += QString::number((quint8)(lengthTotal >> 8),16);
    qDebug() << "string 1 " << stringToConvertToByteArray;

    if (((quint8)lengthTotal &0xff) <= 0x0f)
    {
        stringToConvertToByteArray += "0";
    }
    stringToConvertToByteArray += QString::number((quint8)lengthTotal,16);

    stringToConvertToByteArray += "6B";
    for (quint32 i =0; i< dataRead.length(); i++)
    {
        if (buffer[i] <= 0x0f)
        {
            stringToConvertToByteArray += "0";
        }
        stringToConvertToByteArray += QString::number(buffer[i],16);
    }

    byteTemp = stringToConvertToByteArray.toLatin1();
    byteArrayPrepend = QByteArray::fromHex(byteTemp);
    dataRead.prepend(byteArrayPrepend);

    QString ipAdd  = ui->lineEdit_AddressSystem->text();
    QString portNum = ui->lineEdit_PortSystem->text();
    m_socket->getData(byteArrayPrepend,ipAdd,portNum.toInt(),tsWindowTabRevocatedCA);
    emit startToSendTCP();
    delete[] buffer;
    file.close();
}

void MainWindow::on_actionAdministrative_triggered()
{
    emit adminShow(m_textAdmin);
    //admin->show();
}

void MainWindow::on_actionAbout_triggered()
{
    helpShow->show();
}

void MainWindow::on_actionLoad_triggered()
{
    QString dirOpen;
    bool    resultCheck = false;
    QString fileName;

    dirOpen = m_dir1;
    fileName = QFileDialog::getOpenFileName(this,
                                              "Open TERMINAL binary file", dirOpen);

    resultCheck = loadTerminalFile(fileName, 1);
    if (resultCheck)
    {
        ui->labelDir->setText(fileName);
        ui->ButtonSave->setEnabled(false);
        m_dir1 = updateDir(fileName);
        m_terminalDir = fileName;
        m_readXML->writeXMLFile(m_fileXML, m_terminalDir,m_processingDir, m_entryPointDir);
    }

}

void MainWindow::on_actionSave_triggered()
{
    QString fileName ;

    fileName = ui->labelDir->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this, "Warning", "Cannot open " + fileName);
        return;
    }

    QDataStream in(&file);
    quint8 buffer[200] = {0};
    quint8 outSHA[20]  = {0};
    quint32 lengthTotal;
    tSHA  ct;

    updateBufferToSaveTer(buffer, 23, &lengthTotal);

    ShaInit(&ct);
    ShaUpdate(&ct, buffer, lengthTotal);
    ShaFinal(&ct, outSHA);

    in << (quint8)0xaa;
    for (quint8 i =0; i< 20; i++)
    {
        in << outSHA[i];
    }
    qDebug() << "kength" << lengthTotal;
    for (quint8 i =0; i < lengthTotal; i++)
    {
        in << buffer[i];
    }
    file.close();
    ui->ButtonSave->setEnabled(false);
}

void MainWindow::on_actionSave_As_triggered()
{
    QString dir, fileName;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save TERMINAL binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this, "File error", "Unable to open file " + fileName);
        return;
    }

    QDataStream in(&file);
    quint8 buffer[200] = {0};
    quint8 outSHA[20]  = {0};
    quint32 lengthTotal;
    tSHA  ct;

    updateBufferToSaveTer(buffer, 23, &lengthTotal);

    ShaInit(&ct);
    ShaUpdate(&ct, buffer, lengthTotal);
    ShaFinal(&ct, outSHA);

    in << (quint8)0xaa;
    for (quint8 i =0; i< 20; i++)
    {
        in << outSHA[i];
    }
    for (quint8 i =0; i < lengthTotal; i++)
    {
        in << buffer[i];
    }
    file.close();
}

void MainWindow::on_actionLoad_2_triggered()
{
    QString dirOpen;
    QString fileName;
    bool    resultCheck = false;

    dirOpen = m_dir1;
    fileName = QFileDialog::getOpenFileName(this,
                                              "Open PROCESSING binary file", dirOpen);

    resultCheck = loadProcessingFile(fileName,1);
    if (resultCheck)
    {
        ui->labelDirProcessing->setText(fileName);
        ui->ButtonSaveProcessing->setEnabled(false);
        m_processingDir = fileName;
        m_dir1 = updateDir(fileName);
        m_readXML->writeXMLFile(m_fileXML,m_terminalDir,m_processingDir,m_entryPointDir);
    }


}

void MainWindow::on_actionSave_2_triggered()
{
    QString fileName ;
    quint32 lengthBuf =0;
    quint32 valTmp =0;
    quint32 lengthTotalByteTags =0;

    fileName = ui->labelDirProcessing->text();
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

    fileName = ui->labelDirProcessing->text();
    quint8 *numberOfBytes = new quint8[m_numItemsProcess];

    for(quint32 i =0; i< m_numItemsProcess; i++)
    {
         lengthBuf += m_lengthOfEachItemListWidgetProcessing[i];
         if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
         {
             numberOfBytes[i] = 4;
         }
         else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
         {
             numberOfBytes[i] = 5;
         }
         else
         {
             numberOfBytes[i] = 3;
         }
         lengthTotalByteTags += numberOfBytes[i];
    }

    unsigned char *buffer = new unsigned char[lengthBuf + lengthTotalByteTags];
    QString stringTmp;

    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        buffer[valTmp++] = (unsigned char)0xff;
        buffer[valTmp++] = (unsigned char)0x33;

        if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
        {
            buffer[valTmp++] = (unsigned char)0x81;
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
        {
            buffer[valTmp++] = (unsigned char)0x82;
            buffer[valTmp++] = (unsigned char)((m_lengthOfEachItemListWidgetProcessing[i] & 0xff00) >> 8);
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else
        {
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }

        for (quint32 j =0; j< m_lengthOfEachItemListWidgetProcessing[i]; j++)
        {
          buffer[valTmp++] = (unsigned char)m_databaseArray[i][j];
        }
    }

    QDataStream in(&file);
    unsigned char outSHA[20]  = {0};
    tSHA   ct;
    ShaInit(&ct);
    ShaUpdate(&ct,buffer, lengthBuf+lengthTotalByteTags);
    ShaFinal(&ct,outSHA);

    in << (quint8)0xaa;
    for (quint32 i =0; i< 20; i++)
    {
        in << (quint8)outSHA[i];
    }
    for (quint32 i =0; i < lengthBuf+ lengthTotalByteTags; i++)
    {
        in << (quint8)buffer[i];
    }

    delete[] buffer;
    delete[] numberOfBytes;
    file.close();
    ui->ButtonSaveProcessing->setEnabled(false);
}

void MainWindow::on_actionSave_As_2_triggered()
{
    QString dir, fileName;
    quint32 lengthBuf =0;
    quint32 valTmp =0;
    quint32 lengthTotalByteTags =0;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save PROCESSING binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        return;
    }

    quint8 *numberOfBytes = new quint8[m_numItemsProcess];

    for(quint32 i =0; i< m_numItemsProcess; i++)
    {
         lengthBuf += m_lengthOfEachItemListWidgetProcessing[i];
         if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
         {
             numberOfBytes[i] = 4;
         }
         else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
         {
             numberOfBytes[i] = 5;
         }
         else
         {
             numberOfBytes[i] = 3;
         }
         lengthTotalByteTags += numberOfBytes[i];
    }

    unsigned char *buffer = new unsigned char[lengthBuf + lengthTotalByteTags];

    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        buffer[valTmp++] = (unsigned char)0xff;
        buffer[valTmp++] = (unsigned char)0x33;

        if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
        {
            buffer[valTmp++] = (unsigned char)0x81;
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
        {
            buffer[valTmp++] = (unsigned char)0x82;
            buffer[valTmp++] = (unsigned char)((m_lengthOfEachItemListWidgetProcessing[i] & 0xff00) >> 8);
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else
        {
            buffer[valTmp++] = (unsigned char)m_lengthOfEachItemListWidgetProcessing[i];
        }

        for (quint32 j =0; j< m_lengthOfEachItemListWidgetProcessing[i]; j++)
        {
          buffer[valTmp++] = (unsigned char)m_databaseArray[i][j];
        }
    }

    QDataStream in(&file);
    unsigned char outSHA[20]  = {0};
    tSHA   ct;
    ShaInit(&ct);
    ShaUpdate(&ct,buffer, lengthBuf+lengthTotalByteTags);
    ShaFinal(&ct,outSHA);

    in << (quint8)0xaa;
    for (quint32 i =0; i< 20; i++)
    {
        in << (quint8)outSHA[i];
    }
    for (quint32 i =0; i < lengthBuf+ lengthTotalByteTags; i++)
    {
        in << (quint8)buffer[i];
    }

    delete[] buffer;
    delete[] numberOfBytes;
    file.close();
}

void MainWindow::on_actionLoad_3_triggered()
{
    QString dirOpen;
    bool    resultCheck = false;
    dirOpen = m_dir1;
    QString fileName = QFileDialog::getOpenFileName(this,
                                              "Open ENTRY POINT binary file", dirOpen);

    resultCheck = loadEntryPointFile(fileName,1);
    if (resultCheck)
    {
        ui->labelDirEntry->setText(fileName);
        m_entryPointDir = fileName;
        m_dir1 = updateDir(fileName);
        m_readXML->writeXMLFile(m_fileXML,m_terminalDir,m_processingDir,m_entryPointDir);
    }
}

void MainWindow::on_actionSave_3_triggered()
{
    QString fileName ;
    quint16 numberOfRows;
    quint8  valueBytes;
    quint16 lengthLine;

    fileName = ui->labelDirEntry->text();

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
       QMessageBox::warning(this, "Warning", "Cannot save file");
        return;
    }

    numberOfRows = ui->tableWidget->rowCount();
    quint16 *lengthList = new quint16[numberOfRows];
    quint8 **tmpBuffer;
    tmpBuffer = new quint8*[numberOfRows];
    for (quint32 i =0; i< numberOfRows; i++)
    {
        tmpBuffer[i] = new quint8[MAX_COL];
    }

    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint32 index =0;
        QComboBox *comboKernel = (QComboBox*)ui->tableWidget->cellWidget(i, 0);
        QComboBox *comboID = (QComboBox*)ui->tableWidget->cellWidget(i,1);
        QComboBox *comboTrans = (QComboBox*)ui->tableWidget->cellWidget(i,2);
        QLineEdit *line = (QLineEdit*)ui->tableWidget->cellWidget(i,3);

        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0e;
        tmpBuffer[i][index++] = (quint8)0x03;
        valueBytes = comboKernel->currentIndex();
        tmpBuffer[i][index++] = (quint8)(valueBytes+1);

        valueBytes = comboID->currentIndex();
        if (comboID->count() ==0)
        {
            tmpBuffer[i][index++] = (quint8)0x01;
        }
        else
        {
            tmpBuffer[i][index++] = (quint8)(valueBytes+1);
        }

        valueBytes = comboTrans->currentIndex();
        tmpBuffer[i][index++] = (quint8)valueBytes;
        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0f;

        lengthLine = line->text().length()/2;
        if ((lengthLine > 0x7f) && (lengthLine < 0xff))
        {
            tmpBuffer[i][index++] = (quint8)0x81;
            tmpBuffer[i][index++] = lengthLine;
        }
        else if (lengthLine >0xff)
        {
            tmpBuffer[i][index++] = (quint8)0x82;
            tmpBuffer[i][index++] = (lengthLine &0xff00) >> 8;
            tmpBuffer[i][index++] = (quint8)lengthLine;
        }
        else
        {
            tmpBuffer[i][index++] = lengthLine;
        }
        qDebug() << "text " << line->text();
        for (quint32 j =0; j< lengthLine; j++)
        {
            tmpBuffer[i][index] = (quint8)line->text().mid(2*j,2).toInt(NULL, 16);
            qDebug() << "string " << line->text().mid(2*j,2) << tmpBuffer[i][index];
            index += 1;
        }
        lengthList[i] = index;
    }

    quint32 lengthTotal =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint8 numberOfBytes;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
           numberOfBytes = 4;
        }
        else if (lengthList[i] > 0xff)
        {
            numberOfBytes = 5;
        }
        else
        {
            numberOfBytes = 3;
        }
        lengthTotal += lengthList[i] + numberOfBytes;
    }

    quint8 *bufferFin = new quint8[lengthTotal];
    quint32 index =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        bufferFin[index++] = (quint8)0xff;
        bufferFin[index++] = (quint8)0x35;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
            bufferFin[index++] = (quint8)0x81;
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else if (lengthList[i] > 0xff)
        {
            bufferFin[index++] = (quint8)0x82;
            bufferFin[index++] = (quint8)((lengthList[i] & 0xff00) >>8);
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else
        {
            bufferFin[index++] = (quint8)lengthList[i];
        }
        for (quint32 j =0; j< lengthList[i]; j++)
        {
            bufferFin[index++] = tmpBuffer[i][j];
        }
    }
    QString stringt;
    for(quint32 i =0; i< lengthTotal; i++)
    {
        if (bufferFin[i] <=0x0f) stringt.append("0");
        stringt.append(QString::number(bufferFin[i],16));
    }
    qDebug() << stringt;

    for (quint32 i =0; i< numberOfRows; i++)
    {
        delete[] tmpBuffer[i];
    }
    delete[] tmpBuffer;
    delete[] lengthList;

    tSHA ct;
    quint8 resultSHA[20];
    ShaInit(&ct);
    ShaUpdate(&ct,bufferFin,lengthTotal);
    ShaFinal(&ct,resultSHA);

    QDataStream out(&file);
    out << (quint8)0xaa;
    for (quint32 i =0; i< 20; i++)
    {
        out << resultSHA[i];
    }
    for (quint32 i =0; i< lengthTotal; i++)
    {
        out << bufferFin[i];
    }
    delete[] bufferFin;
    file.flush();
    file.close();
    ui->ButtonEntrySave->setEnabled(false);
}

void MainWindow::on_actionSave_As_3_triggered()
{
    QString dir, fileName;
    quint16 numberOfRows;
    quint8  valueBytes;
    quint16 lengthLine;

    dir = stringDir.getDirToOpenProcessing();
    fileName = QFileDialog::getSaveFileName(this, "Save ENTRY POINT binary file", dir);

    QFile file(fileName);
    if (!file.open(QFile::WriteOnly))
    {
        return;
    }
    numberOfRows = ui->tableWidget->rowCount();
    quint16 *lengthList = new quint16[numberOfRows];
    quint8 **tmpBuffer;
    tmpBuffer = new quint8*[numberOfRows];
    for (quint32 i =0; i< numberOfRows; i++)
    {
        tmpBuffer[i] = new quint8[MAX_COL];
    }

    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint32 index =0;
        QComboBox *comboKernel = (QComboBox*)ui->tableWidget->cellWidget(i, 0);
        QComboBox *comboID = (QComboBox*)ui->tableWidget->cellWidget(i,1);
        QComboBox *comboTrans = (QComboBox*)ui->tableWidget->cellWidget(i,2);
        QLineEdit *line = (QLineEdit*)ui->tableWidget->cellWidget(i,3);

        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0e;
        tmpBuffer[i][index++] = (quint8)0x03;
        valueBytes = comboKernel->currentIndex();
        tmpBuffer[i][index++] = (quint8)(valueBytes+1);

        valueBytes = comboID->currentIndex();
        if (comboID->count() ==0)
        {
            tmpBuffer[i][index++] = (quint8)0x01;
        }
        else
        {
            tmpBuffer[i][index++] = (quint8)(valueBytes+1);
        }

        valueBytes = comboTrans->currentIndex();
        tmpBuffer[i][index++] = (quint8)valueBytes;
        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0f;

        lengthLine = line->text().length()/2;
        if ((lengthLine > 0x7f) && (lengthLine < 0xff))
        {
            tmpBuffer[i][index++] = (quint8)0x81;
            tmpBuffer[i][index++] = lengthLine;
        }
        else if (lengthLine >0xff)
        {
            tmpBuffer[i][index++] = (quint8)0x82;
            tmpBuffer[i][index++] = (lengthLine &0xff00) >> 8;
            tmpBuffer[i][index++] = (quint8)lengthLine;
        }
        else
        {
            tmpBuffer[i][index++] = lengthLine;
        }
        qDebug() << "text " << line->text();
        for (quint32 j =0; j< lengthLine; j++)
        {
            tmpBuffer[i][index] = (quint8)line->text().mid(2*j,2).toInt(NULL, 16);
            qDebug() << "string " << line->text().mid(2*j,2) << tmpBuffer[i][index];
            index += 1;
        }
        lengthList[i] = index;
    }

    quint32 lengthTotal =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint8 numberOfBytes;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
           numberOfBytes = 4;
        }
        else if (lengthList[i] > 0xff)
        {
            numberOfBytes = 5;
        }
        else
        {
            numberOfBytes = 3;
        }
        lengthTotal += lengthList[i] + numberOfBytes;
    }

    quint8 *bufferFin = new quint8[lengthTotal];
    quint32 index =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        bufferFin[index++] = (quint8)0xff;
        bufferFin[index++] = (quint8)0x35;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
            bufferFin[index++] = (quint8)0x81;
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else if (lengthList[i] > 0xff)
        {
            bufferFin[index++] = (quint8)0x82;
            bufferFin[index++] = (quint8)((lengthList[i] & 0xff00) >>8);
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else
        {
            bufferFin[index++] = (quint8)lengthList[i];
        }
        for (quint32 j =0; j< lengthList[i]; j++)
        {
            bufferFin[index++] = tmpBuffer[i][j];
        }
    }
    QString stringt;
    for(quint32 i =0; i< lengthTotal; i++)
    {
        if (bufferFin[i] <=0x0f) stringt.append("0");
        stringt.append(QString::number(bufferFin[i],16));
    }
    qDebug() << stringt;

    for (quint32 i =0; i< numberOfRows; i++)
    {
        delete[] tmpBuffer[i];
    }
    delete[] tmpBuffer;
    delete[] lengthList;

    tSHA ct;
    quint8 resultSHA[20];
    ShaInit(&ct);
    ShaUpdate(&ct,bufferFin,lengthTotal);
    ShaFinal(&ct,resultSHA);

    QDataStream out(&file);
    out << (quint8)0xaa;
    for (quint32 i =0; i< 20; i++)
    {
        out << resultSHA[i];
    }
    for (quint32 i =0; i< lengthTotal; i++)
    {
        out << bufferFin[i];
    }
    delete[] bufferFin;
    file.flush();
    file.close();
}


void MainWindow::on_checkBoxAutorun_stateChanged(int arg1)
{
    (void)&arg1;
    bool state =false;
    state = (ui->checkBoxAutorun->checkState())?true:false;
    ui->spinBox->setEnabled(state);
}

void MainWindow::on_checkBoxDefault_stateChanged(int arg1)
{
    (void)&arg1;
    bool state ;
    state = (ui->checkBoxDefault->checkState())?true:false;
    ui->lineEditDenial->setEnabled(state);
    ui->toolButtonDefault->setEnabled(state);
    ui->lineEditDefault->setEnabled(state);
    ui->toolButtonOnline->setEnabled(state);
    ui->lineEditOnline->setEnabled(state);
    ui->toolButton_7->setEnabled(state);
}

void MainWindow::on_comboBoxTerminal_currentIndexChanged(int index)
{
    (void)&index;
    //ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditTerminal_textEdited(const QString &arg1)
{
    (void)&arg1;
    changeLengthLineEdit(ui->lineEditTerminal,6);
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditAddition_textEdited(const QString &arg1)
{
    (void)&arg1;
    changeLengthLineEdit(ui->lineEditAddition,10);
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditTerCounCode_textEdited(const QString &arg1)
{
    (void)&arg1;
    changeLengthLineEdit(ui->lineEditTerCounCode,4);
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditPINTimeout_textEdited(const QString &arg1)
{
    (void)&arg1;
    changeLengthLineEdit(ui->lineEditPINTimeout,2);
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxEMV1_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxMastripe_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_comboBoxCDA_currentIndexChanged(int index)
{
    (void)&index;

}

void MainWindow::on_checkBoxVelocity_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxRTSNot_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxPinBypass_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxPSE_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxCardHolder_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditOnline_textEdited(const QString &arg1)
{
    (void)&arg1;
    changeLengthLineEdit(ui->lineEditOnline,10);
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditDefault_textEdited(const QString &arg1)
{
    (void)&arg1;
    changeLengthLineEdit(ui->lineEditDefault,10);
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditDenial_textEdited(const QString &arg1)
{
    (void)&arg1;
    changeLengthLineEdit(ui->lineEditDenial,10);
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxOnline_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxReferral_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxAdvice_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBoxEMV2_clicked()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
  (void)&arg1;
}

void MainWindow::on_spinBox_editingFinished()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditOnline_editingFinished()
{

}

void MainWindow::on_lineEditDefault_editingFinished()
{
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_lineEditDenial_editingFinished()
{

}

void MainWindow::on_lineEditPINTimeout_editingFinished()
{

}

void MainWindow::on_lineEditTerCounCode_editingFinished()
{

}

void MainWindow::on_lineEditAddition_editingFinished()
{

}

void MainWindow::on_lineEditTerminal_editingFinished()
{

}

void MainWindow::on_comboBoxTerminal_editTextChanged(const QString &arg1)
{
   (void)&arg1;
}

void MainWindow::on_comboBoxCDA_editTextChanged(const QString &arg1)
{
 (void)&arg1;
}

void MainWindow::on_comboBoxTerminal_activated(const QString &arg1)
{
    (void)&arg1;
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_comboBoxCDA_activated(const QString &arg1)
{
    (void)&arg1;
    ui->ButtonSave->setEnabled(true);
}

void MainWindow::on_checkBox_TACProcess_stateChanged(int arg1)
{
    (void)&arg1;
    bool state;
    state = (ui->checkBox_TACProcess->checkState())?true:false;
    ui->lineEditDenialProcessing->setEnabled(state);
    ui->lineEditOnlineProcessing->setEnabled(state);
    ui->lineEditDefaultProcessing->setEnabled(state);
    ui->toolButtonDefaultProcessing->setEnabled(state);
    ui->toolButtonDenialProcessing->setEnabled(state);
    ui->toolButtonOnlineProcessing->setEnabled(state);

}

void MainWindow::on_checkBox_SkipTACProcess_clicked()
{
    quint32 index;
    quint32 length;
    quint8 **database;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    index = ui->listWidgetProcessing->currentRow();
    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptSkipTAC][3] = (ui->checkBox_SkipTACProcess->checkState())?true:false;
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_lineEditAcquireIDProcessing_textEdited(const QString &arg1)
{
    quint8 **database;
    quint32 length;
    quint8 index = ui->listWidgetProcessing->currentRow();
    bool   ok;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    changeLengthLineEdit(ui->lineEditAcquireIDProcessing, 12);
    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    for (quint32 i =0; i<6; i++)
    {
        database[ptAcquireId][3+i] = (quint8)ui->lineEditAcquireIDProcessing->text().mid(2*i,2).toInt(&ok, 16);
    }
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_checkBox_ASIProcess_clicked()
{
    quint8 index = ui->listWidgetProcessing->currentRow();
    quint32 length;
    quint8 **database;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptAppSelInd][3] = (ui->checkBox_ASIProcess->checkState())?true:false;
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_lineEditAVNProcessing_textEdited(const QString &arg1)
{
    quint8 index = ui->listWidgetProcessing->currentRow();
    quint32 length;
    quint8 **database;
    bool   ok;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    changeLengthLineEdit(ui->lineEditAVNProcessing,4);
    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());
    for (quint32 i =0; i<2; i++)
    {
        database[ptAppVerNum][3+i] = (quint8)arg1.mid(2*i,2).toInt(&ok, 16);
    }
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_lineEditDenialProcessing_textEdited(const QString &arg1)
{
    quint8 index = ui->listWidgetProcessing->currentRow();
    quint32 length;
    quint8 **database;
    bool ok;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    changeLengthLineEdit(ui->lineEditDenialProcessing,10);
    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());
    for (quint32 i =0; i<5; i++)
    {
        database[ptTACDenial][3+i] = (quint8)arg1.mid(2*i,2).toInt(&ok, 16);
    }

    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_lineEditOnlineProcessing_returnPressed()
{

}

void MainWindow::on_lineEditOnlineProcessing_textEdited(const QString &arg1)
{
    quint8 index = ui->listWidgetProcessing->currentRow();
    quint32 length;
    quint8 **database;
    bool ok;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    changeLengthLineEdit(ui->lineEditOnlineProcessing, 10);
    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());
    for (quint32 i =0; i<5; i++)
    {
        database[ptTACOnl][3+i] = (quint8)arg1.mid(2*i,2).toInt(&ok, 16);
    }
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_lineEditDefaultProcessing_textEdited(const QString &arg1)
{
    quint8 index = ui->listWidgetProcessing->currentRow();
    quint32 length;
    quint8 **database;
    bool  ok;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    changeLengthLineEdit(ui->lineEditDefaultProcessing,10);
    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());
    for (quint32 i =0; i<5; i++)
    {
        database[ptTACDef][3+i] = (quint8)arg1.mid(2*i,2).toInt(&ok, 16);
    }
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_checkBox_FloorLimitProcess_clicked()
{
    quint8 **database;
    quint32 length;
    quint8 index = ui->listWidgetProcessing->currentRow();

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptFloorLimitCheck][3] = (ui->checkBox_FloorLimitProcess->checkState())?true:false;
    storeTagToDatabaseProcess(m_databaseArray, index,database,&length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_lineEditProcessingFloor_textEdited(const QString &arg1)
{
    quint8 index = ui->listWidgetProcessing->currentRow();
    quint8 **database;
    quint32 length;
    float val;
    bool ok;
    quint64 value;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    val = arg1.toFloat(&ok);
    value = (quint64)(val*10);
    if (ok)
    {
        for (quint32 i =0; i<4; i++)
        {
            database[ptFloorLimit][3+i] = (quint8)((value &(0xFF << (3-i)*8)) >> (3-i)*8);
        }

        storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
        m_lengthOfEachItemListWidgetProcessing[index] = length;
    }

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_checkBox_DefaultDDOL_clicked()
{
    quint8 **database;
    quint32 length;
    quint8 index = ui->listWidgetProcessing->currentRow();

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptDefDDOL][3] = (ui->checkBox_DefaultDDOL->checkState())?true:false;
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);

}


void MainWindow::on_checkBox_DefaultTDOL_clicked()
{
    quint8 **database;
    quint32 length;
    quint8 index = ui->listWidgetProcessing->currentRow();

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptDefTDOL][3] = (ui->checkBox_DefaultTDOL->checkState())?true:false;
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_checkBox_RTSProcess_clicked()
{
    quint8 **database;
    quint32 length;
    quint8 index = ui->listWidgetProcessing->currentRow();

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptRTS][3] = (ui->checkBox_RTSProcess->checkState())?true:false;
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_checkBox_VelocityProcess_clicked()
{
    quint8 **database;
    quint32 length;
    quint8 index = ui->listWidgetProcessing->currentRow();

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptVelChecSupp][3] = (ui->checkBox_VelocityProcess->checkState())?true:false;
    qDebug() << "vel check " << database[ptVelChecSupp][3];
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_spinBox_TargetProcess_editingFinished()
{
    quint32 length;
    quint8 **database;
    quint8 index = ui->listWidgetProcessing->currentRow();

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptTargPer][3] = ui->spinBox_TargetProcess->value();
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_spinBox_MaxTargetProcess_editingFinished()
{
    quint32 length;
    quint8 **database;
    quint8 index = ui->listWidgetProcessing->currentRow();

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    database[ptMaxTargPer][3] = ui->spinBox_MaxTargetProcess->value();
    storeTagToDatabaseProcess(m_databaseArray, index,database, &length);
    m_lengthOfEachItemListWidgetProcessing[index] = length;

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);

}

void MainWindow::on_lineEditProcessingTCC_textEdited(const QString &arg1)
{
    quint32 lengthTLV;
    quint8 lengthString;
    quint8 index = ui->listWidgetProcessing->currentRow();
    quint8 **database;
    QString stringTmp;
    bool ok;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    lengthString = arg1.length();
    stringTmp = arg1;
    for (quint32 i =0; i< 4-lengthString; i++)
    {
        stringTmp.prepend("0");
    }

    for (quint32 i =0; i< 2; i++)
    {
        database[ptTranCurCode][3+i] = (quint8)(arg1.mid(2*i,2).toInt(&ok, 16));
    }
    storeTagToDatabaseProcess(m_databaseArray, index,database, &lengthTLV);
    m_lengthOfEachItemListWidgetProcessing[index] = lengthTLV;

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_lineEditProcessingExpo_textEdited(const QString &arg1)
{
    quint32 lengthTLV;
    quint8 valueTmp;
    quint8 **database;
    quint8 index = ui->listWidgetProcessing->currentRow();
    bool ok;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    valueTmp = (quint8)ui->lineEditProcessingExpo->text().toInt(&ok,10);

    if (ok)
    {
        database[ptTranCurExpo][3] = valueTmp;
        storeTagToDatabaseProcess(m_databaseArray, index,database, &lengthTLV);
        m_lengthOfEachItemListWidgetProcessing[index] = lengthTLV;
    }

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_lineEditProcessingThresh_textEdited(const QString &arg1)
{
    quint32 lengthTLV;
    quint8 valueTmp;
    quint8 **database;
    quint8 index = ui->listWidgetProcessing->currentRow();
    float val;
    bool ok;

    database = new quint8*[MAX_ROW];
    for (quint32 i =0; i< MAX_ROW; i++)
    {
        database[i] = new quint8[MAX_COL];
    }

    initializeBufferDatabase(database);
    checkLengthEachTagProcess(m_databaseArray[index],m_lengthOfEachItemListWidgetProcessing[index],
                              database,QString());

    val = ui->lineEditProcessingThresh->text().toFloat(&ok);
    if (ok)
    {
        valueTmp = (quint8)(val*10);
        qDebug() << "value temp " << valueTmp << val;
        for (quint32 i =0; i< 3; i++)
        {
            database[ptThreshVal][3+i] = (quint8)((valueTmp & (0xFF << 8*(2-i))) >> 8*(2-i));
        }
        storeTagToDatabaseProcess(m_databaseArray, index,database, &lengthTLV);
        qDebug() << "length tlv " << lengthTLV;
        m_lengthOfEachItemListWidgetProcessing[index] = lengthTLV;
    }

    for (quint32 i =0; i< MAX_ROW; i++)
    {
        delete[] database[i];
    }
    delete[] database;
    ui->ButtonSaveProcessing->setEnabled(true);
}

void MainWindow::on_spinBox_MaxTargetProcess_valueChanged(int arg1)
{

}



void MainWindow::on_tabWidget_2_tabBarClicked(int index)
{


}

void MainWindow::on_textEdit_DDOL_textChanged()
{
   emit sendStringToCheck(ui->textEdit_DDOL->toPlainText());
}

void MainWindow::on_textEdit_TDOL_textChanged()
{
   emit sendStringToCheck(ui->textEdit_TDOL->toPlainText());
}

/**
 * @brief MainWindow::on_ButtonACEProcessing_clicked
 * Show ACE dialog
 */
void MainWindow::on_ButtonACEProcessing_clicked()
{
  emit selectACE(ts_aceFromProcessing);
  m_getACE->show();
}

/**
 * @brief MainWindow::on_ButtonEntryACE_clicked
 * show ACE dialog
 */
void MainWindow::on_ButtonEntryACE_clicked()
{
    emit selectACE(ts_aceFromEntryPoint);
    m_getACE->show();
}

/**
 * @brief MainWindow::on_ButtonSend_clicked
 * according to the state of all widgets, save value to buffer.
 * get sha key of data
 * convert all value needed to QByteArray to send to server.
 */
void MainWindow::on_ButtonSend_clicked()
{
    quint8 buffer[200] = {0};
    quint8 outSHA[20]  = {0};
    quint32 lengthTotal;
    tSHA  ct;
    QString stringNeededToConvertToByte;
    QByteArray finalByteArray, byteTmp;

    /* save the value of all widget to buffer */
    updateBufferToSaveTer(buffer, 23, &lengthTotal);
    qDebug() << "length total " << lengthTotal;

    /* get sha key of data in buffer */
    ShaInit(&ct);
    ShaUpdate(&ct, buffer, lengthTotal);
    ShaFinal(&ct, outSHA);

    //string test
    QString stringTest;
    for (quint32 i =0; i < 20; i++)
    {
        if (outSHA[i] <= 0x0f)
        {
            stringTest += "0";
        }
        stringTest += QString::number(outSHA[i],16);
    }
    qDebug() << "send " << stringTest;

    quint32 lengthData;
    quint8  lengthByte049hAnd0aah;

    /* according to the document, add 0x49h and 0x00 into data before sending to server */
    lengthByte049hAnd0aah = 2;
    lengthData = lengthTotal + sizeof(outSHA)/sizeof(quint8) + lengthByte049hAnd0aah;

    /* get all value and save it to a string to convert into byteArray */
    stringNeededToConvertToByte += "00";
    if (lengthData <= 0x0F)
    {
        stringNeededToConvertToByte += "0";
    }

    stringNeededToConvertToByte += QString::number(lengthData, 16);

    stringNeededToConvertToByte += "49aa";

    /* save sha key to string */
    for (int i =0; i< sizeof(outSHA)/sizeof(quint8) ; i++)
    {
        if (outSHA[i] <=0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(outSHA[i],16);
    }
   /* save data to string */
    for (quint32 i =0; i< lengthTotal; i++)
    {
        if (buffer[i] <= 0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(buffer[i],16);
    }

    byteTmp = stringNeededToConvertToByte.toLatin1();
    finalByteArray = QByteArray::fromHex(byteTmp);
    QString ipAdd  = ui->lineEdit_AddressSystem->text();
    QString portNum = ui->lineEdit_PortSystem->text();
    /* send data to server */
    m_socket->getData(finalByteArray,ipAdd,portNum.toInt(),tsTabTerminal);
    emit startToSendTCP();

}

/**
 * @brief MainWindow::on_ButtonSendProcessing_clicked
 */
void MainWindow::on_ButtonSendProcessing_clicked()
{
    quint8 outSHA[20]  = {0};
    tSHA  ct;
    quint32 lengthBuf =0;
    quint32 valTmp =0;
    quint32 lengthTotalByteTags =0;
    QString stringNeededToConvertToByte;
    QByteArray finalByteArray, byteTmp;

    quint8 *numberOfBytes = new quint8[m_numItemsProcess];

    for(quint32 i =0; i< m_numItemsProcess; i++)
    {
        /* update the lengthBuf */
         lengthBuf += m_lengthOfEachItemListWidgetProcessing[i];

         /* check if there are subsequence in tag, length */
         if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
         {
             numberOfBytes[i] = 4;
         }
         else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
         {
             numberOfBytes[i] = 5;
         }
         else
         {
             numberOfBytes[i] = 3;
         }
         lengthTotalByteTags += numberOfBytes[i];
    }

    /* create a buffer to contain all the TLV */
    quint8 *buffer = new quint8[lengthBuf + lengthTotalByteTags];

    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        /* there are m_numItemsProcess ff33 in file */
        buffer[valTmp++] = (quint8)0xff;
        buffer[valTmp++] = (quint8)0x33;

        if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
        {
            buffer[valTmp++] = (quint8)0x81;
            buffer[valTmp++] = (quint8)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
        {
            buffer[valTmp++] = (quint8)0x82;
            buffer[valTmp++] = (quint8)((m_lengthOfEachItemListWidgetProcessing[i] & 0xff00) >> 8);
            buffer[valTmp++] = (quint8)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else
        {
            buffer[valTmp++] = (quint8)m_lengthOfEachItemListWidgetProcessing[i];
        }

        for (quint32 j =0; j< m_lengthOfEachItemListWidgetProcessing[i]; j++)
        {
          buffer[valTmp++] = (quint8)m_databaseArray[i][j];
        }
    }
    /* get the sha key of buffer */
    ShaInit(&ct);
    ShaUpdate(&ct, buffer, lengthBuf+lengthTotalByteTags);
    ShaFinal(&ct, outSHA);

    quint32 lengthData;
    quint8  lengthByte043hAnd0aah;

    /*according to the table in the document, add 0x43h */
    lengthByte043hAnd0aah = 2;
    lengthData = lengthBuf + lengthTotalByteTags + sizeof(outSHA)/sizeof(quint8) + lengthByte043hAnd0aah;
    qDebug() << "lengthData " << lengthData << sizeof(outSHA)/sizeof(quint8);

    /*add all value into a string to convert to ByteArray, the format of data to send to server is in
     the document */
    if (((lengthData >> 8) & 0xff) <= 0x0f)
    {
        stringNeededToConvertToByte += "0";
    }
    stringNeededToConvertToByte += QString::number((lengthData >> 8) & 0xff, 16);

    if ((quint8)(lengthData & 0xff) <= 0x0f)
    {
        stringNeededToConvertToByte += "0";
    }
    stringNeededToConvertToByte += QString::number((quint8)(lengthData &0xff), 16);

    stringNeededToConvertToByte += "43aa";

    for (int i =0; i< sizeof(outSHA)/sizeof(quint8) ; i++)
    {
        if (outSHA[i] <=0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(outSHA[i],16);
    }

    for (quint32 i =0; i< lengthBuf + lengthTotalByteTags; i++)
    {
        if (buffer[i] <= 0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(buffer[i],16);
    }

    byteTmp = stringNeededToConvertToByte.toLatin1();
    finalByteArray = QByteArray::fromHex(byteTmp);
    delete[] buffer;
    delete[] numberOfBytes;
    QString ipAdd  = ui->lineEdit_AddressSystem->text();
    QString portNum = ui->lineEdit_PortSystem->text();
    m_socket->getData(finalByteArray,ipAdd,portNum.toInt(),tsTabProcessing);
    emit startToSendTCP();

}

void MainWindow::on_listWidgetProcessing_clicked(const QModelIndex &index)
{

}

/**
 * @brief MainWindow::on_ButtonEntrySend_clicked
 */
void MainWindow::on_ButtonEntrySend_clicked()
{
    quint16 numberOfRows;
    quint8  valueBytes;
    quint16 lengthLine;
    quint8 outSHA[20]  = {0};
    tSHA  ct;
    QString stringNeededToConvertToByte;
    QByteArray finalByteArray, byteTmp;

    quint16 *lengthList = new quint16[numberOfRows];
   /* create two dimenson array to get data of each row in entryPoint tab */
    quint8 **tmpBuffer;
    tmpBuffer = new quint8*[numberOfRows];
    for (quint32 i =0; i< numberOfRows; i++)
    {
        tmpBuffer[i] = new quint8[MAX_COL];
    }

    numberOfRows = ui->tableWidget->rowCount();
    /*gather all data of each row into one buffer */
    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint32 index =0;
        QComboBox *comboKernel = (QComboBox*)ui->tableWidget->cellWidget(i, 0);
        QComboBox *comboID = (QComboBox*)ui->tableWidget->cellWidget(i,1);
        QComboBox *comboTrans = (QComboBox*)ui->tableWidget->cellWidget(i,2);
        QLineEdit *line = (QLineEdit*)ui->tableWidget->cellWidget(i,3);

        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0e;
        tmpBuffer[i][index++] = (quint8)0x03;
        valueBytes = comboKernel->currentIndex();
        tmpBuffer[i][index++] = (quint8)(valueBytes+1);

        valueBytes = comboID->currentIndex();
        if (comboID->count() ==0)
        {
            tmpBuffer[i][index++] = (quint8)0x01;
        }
        else
        {
            tmpBuffer[i][index++] = (quint8)(valueBytes+1);
        }

        valueBytes = comboTrans->currentIndex();
        tmpBuffer[i][index++] = (quint8)valueBytes;
        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0f;

        lengthLine = line->text().length()/2;
        if ((lengthLine > 0x7f) && (lengthLine < 0xff))
        {
            tmpBuffer[i][index++] = (quint8)0x81;
            tmpBuffer[i][index++] = lengthLine;
        }
        else if (lengthLine >0xff)
        {
            tmpBuffer[i][index++] = (quint8)0x82;
            tmpBuffer[i][index++] = (lengthLine &0xff00) >> 8;
            tmpBuffer[i][index++] = (quint8)lengthLine;
        }
        else
        {
            tmpBuffer[i][index++] = lengthLine;
        }

        for (quint32 j =0; j< lengthLine; j++)
        {
            tmpBuffer[i][index] = (quint8)line->text().mid(2*j,2).toInt(NULL, 16);
            index += 1;
        }
        /* save length of each row (ff35) */
        lengthList[i] = index;
    }

    quint32 lengthTotal =0;
    /* check lengthList to take the number of bytes of tag */
    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint8 numberOfBytes;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
           numberOfBytes = 4;
        }
        else if (lengthList[i] > 0xff)
        {
            numberOfBytes = 5;
        }
        else
        {
            numberOfBytes = 3;
        }
        lengthTotal += lengthList[i] + numberOfBytes;
    }

    /* bufferFin gets all data needed */
    quint8 *bufferFin = new quint8[lengthTotal];
    quint32 index =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        bufferFin[index++] = (quint8)0xff;
        bufferFin[index++] = (quint8)0x35;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
            bufferFin[index++] = (quint8)0x81;
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else if (lengthList[i] > 0xff)
        {
            bufferFin[index++] = (quint8)0x82;
            bufferFin[index++] = (quint8)((lengthList[i] & 0xff00) >>8);
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else
        {
            bufferFin[index++] = (quint8)lengthList[i];
        }
        for (quint32 j =0; j< lengthList[i]; j++)
        {
            bufferFin[index++] = tmpBuffer[i][j];
        }
    }

    for (quint32 i =0; i< numberOfRows; i++)
    {
        delete[] tmpBuffer[i];
    }
    delete[] tmpBuffer;
    delete[] lengthList;

   /* get sha key of bufferFin */
    ShaInit(&ct);
    ShaUpdate(&ct, bufferFin,lengthTotal);
    ShaFinal(&ct, outSHA);

    quint32 lengthData;
    quint8  lengthByte065hAnd0aah;

    lengthByte065hAnd0aah = 2;
    lengthData = lengthTotal + sizeof(outSHA)/sizeof(quint8) + lengthByte065hAnd0aah;
    qDebug() << "lengthData " << lengthData << sizeof(outSHA)/sizeof(quint8);

    if (((lengthData >> 8) & 0xff) <= 0x0f)
    {
        stringNeededToConvertToByte += "0";
    }
    stringNeededToConvertToByte += QString::number((lengthData >> 8) & 0xff, 16);

    if ((quint8)(lengthData & 0xff) <= 0x0f)
    {
        stringNeededToConvertToByte += "0";
    }
    stringNeededToConvertToByte += QString::number((quint8)(lengthData &0xff), 16);

    stringNeededToConvertToByte += "65aa";

    for (int i =0; i< sizeof(outSHA)/sizeof(quint8) ; i++)
    {
        if (outSHA[i] <=0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(outSHA[i],16);
    }

    for (quint32 i =0; i< lengthTotal; i++)
    {
        if (bufferFin[i] <= 0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(bufferFin[i],16);
    }

    qDebug() << "string " << stringNeededToConvertToByte;
    byteTmp = stringNeededToConvertToByte.toLatin1();
    finalByteArray = QByteArray::fromHex(byteTmp);
    delete[] bufferFin;
    QString ipAdd  = ui->lineEdit_AddressSystem->text();
    QString portNum = ui->lineEdit_PortSystem->text();
    m_socket->getData(finalByteArray,ipAdd,portNum.toInt(),tsTabEntryPoint);
    emit startToSendTCP();

}

void MainWindow::on_actionSendTerminal_triggered()
{
    quint8 buffer[200] = {0};
    quint8 outSHA[20]  = {0};
    quint32 lengthTotal;
    tSHA  ct;
    QString stringNeededToConvertToByte;
    QByteArray finalByteArray, byteTmp;

    updateBufferToSaveTer(buffer, 23, &lengthTotal);

    ShaInit(&ct);
    ShaUpdate(&ct, buffer, lengthTotal);
    ShaFinal(&ct, outSHA);

    quint32 lengthData;
    quint8  lengthByte049hAnd0aah;

    lengthByte049hAnd0aah = 2;
    lengthData = lengthTotal + sizeof(outSHA)/sizeof(quint8) + lengthByte049hAnd0aah;

    stringNeededToConvertToByte += "00";
    if (lengthData <= 0x0F)
    {
        stringNeededToConvertToByte += "0";
    }

    stringNeededToConvertToByte += QString::number(lengthData, 16);

    stringNeededToConvertToByte += "49aa";

    for (int i =0; i< sizeof(outSHA)/sizeof(quint8) ; i++)
    {
        if (outSHA[i] <=0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(outSHA[i],16);
    }

    for (quint32 i =0; i< lengthTotal; i++)
    {
        if (buffer[i] <= 0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(buffer[i],16);
    }

    byteTmp = stringNeededToConvertToByte.toLatin1();
    finalByteArray = QByteArray::fromHex(byteTmp);
    QString ipAdd  = ui->lineEdit_AddressSystem->text();
    QString portNum = ui->lineEdit_PortSystem->text();
    m_socket->getData(finalByteArray,ipAdd,portNum.toInt(),tsTabTerminal);
    emit startToSendTCP();
}

void MainWindow::on_actionSendProcessing_triggered()
{
    quint8 outSHA[20]  = {0};
    tSHA  ct;
    quint32 lengthBuf =0;
    quint32 valTmp =0;
    quint32 lengthTotalByteTags =0;
    QString stringNeededToConvertToByte;
    QByteArray finalByteArray, byteTmp;

    quint8 *numberOfBytes = new quint8[m_numItemsProcess];

    for(quint32 i =0; i< m_numItemsProcess; i++)
    {
         lengthBuf += m_lengthOfEachItemListWidgetProcessing[i];
         if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
         {
             numberOfBytes[i] = 4;
         }
         else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
         {
             numberOfBytes[i] = 5;
         }
         else
         {
             numberOfBytes[i] = 3;
         }
         lengthTotalByteTags += numberOfBytes[i];
    }

    quint8 *buffer = new quint8[lengthBuf + lengthTotalByteTags];

    for (quint32 i =0; i< m_numItemsProcess; i++)
    {
        buffer[valTmp++] = (quint8)0xff;
        buffer[valTmp++] = (quint8)0x33;

        if ((m_lengthOfEachItemListWidgetProcessing[i] > 127) && (m_lengthOfEachItemListWidgetProcessing[i] < 256))
        {
            buffer[valTmp++] = (quint8)0x81;
            buffer[valTmp++] = (quint8)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else if (m_lengthOfEachItemListWidgetProcessing[i] > 256)
        {
            buffer[valTmp++] = (quint8)0x82;
            buffer[valTmp++] = (quint8)((m_lengthOfEachItemListWidgetProcessing[i] & 0xff00) >> 8);
            buffer[valTmp++] = (quint8)m_lengthOfEachItemListWidgetProcessing[i];
        }
        else
        {
            buffer[valTmp++] = (quint8)m_lengthOfEachItemListWidgetProcessing[i];
        }

        for (quint32 j =0; j< m_lengthOfEachItemListWidgetProcessing[i]; j++)
        {
          buffer[valTmp++] = (quint8)m_databaseArray[i][j];
        }
    }

    ShaInit(&ct);
    ShaUpdate(&ct, buffer, lengthBuf+lengthTotalByteTags);
    ShaFinal(&ct, outSHA);

    quint32 lengthData;
    quint8  lengthByte043hAnd0aah;

    lengthByte043hAnd0aah = 2;
    lengthData = lengthBuf + lengthTotalByteTags + sizeof(outSHA)/sizeof(quint8) + lengthByte043hAnd0aah;
    qDebug() << "lengthData " << lengthData << sizeof(outSHA)/sizeof(quint8);

    if (((lengthData >> 8) & 0xff) <= 0x0f)
    {
        stringNeededToConvertToByte += "0";
    }
    stringNeededToConvertToByte += QString::number((lengthData >> 8) & 0xff, 16);

    if ((quint8)(lengthData & 0xff) <= 0x0f)
    {
        stringNeededToConvertToByte += "0";
    }
    stringNeededToConvertToByte += QString::number((quint8)(lengthData &0xff), 16);

    stringNeededToConvertToByte += "43aa";

    for (int i =0; i< sizeof(outSHA)/sizeof(quint8) ; i++)
    {
        if (outSHA[i] <=0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(outSHA[i],16);
    }

    for (quint32 i =0; i< lengthBuf + lengthTotalByteTags; i++)
    {
        if (buffer[i] <= 0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(buffer[i],16);
    }

    qDebug() << "string " << stringNeededToConvertToByte;
    byteTmp = stringNeededToConvertToByte.toLatin1();
    finalByteArray = QByteArray::fromHex(byteTmp);
    delete[] buffer;
    delete[] numberOfBytes;
    QString ipAdd  = ui->lineEdit_AddressSystem->text();
    QString portNum = ui->lineEdit_PortSystem->text();
    m_socket->getData(finalByteArray,ipAdd,portNum.toInt(),tsTabProcessing);
    emit startToSendTCP();
}

void MainWindow::on_actionSendEntryPoint_triggered()
{
    quint16 numberOfRows;
    quint8  valueBytes;
    quint16 lengthLine;
    quint8 outSHA[20]  = {0};
    tSHA  ct;
    QString stringNeededToConvertToByte;
    QByteArray finalByteArray, byteTmp;

    quint16 *lengthList = new quint16[numberOfRows];
    quint8 **tmpBuffer;
    tmpBuffer = new quint8*[numberOfRows];
    for (quint32 i =0; i< numberOfRows; i++)
    {
        tmpBuffer[i] = new quint8[MAX_COL];
    }

    numberOfRows = ui->tableWidget->rowCount();

    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint32 index =0;
        QComboBox *comboKernel = (QComboBox*)ui->tableWidget->cellWidget(i, 0);
        QComboBox *comboID = (QComboBox*)ui->tableWidget->cellWidget(i,1);
        QComboBox *comboTrans = (QComboBox*)ui->tableWidget->cellWidget(i,2);
        QLineEdit *line = (QLineEdit*)ui->tableWidget->cellWidget(i,3);

        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0e;
        tmpBuffer[i][index++] = (quint8)0x03;
        valueBytes = comboKernel->currentIndex();
        tmpBuffer[i][index++] = (quint8)(valueBytes+1);

        valueBytes = comboID->currentIndex();
        if (comboID->count() ==0)
        {
            tmpBuffer[i][index++] = (quint8)0x01;
        }
        else
        {
            tmpBuffer[i][index++] = (quint8)(valueBytes+1);
        }

        valueBytes = comboTrans->currentIndex();
        tmpBuffer[i][index++] = (quint8)valueBytes;
        tmpBuffer[i][index++] = (quint8)0xdf;
        tmpBuffer[i][index++] = (quint8)0x0f;

        lengthLine = line->text().length()/2;
        if ((lengthLine > 0x7f) && (lengthLine < 0xff))
        {
            tmpBuffer[i][index++] = (quint8)0x81;
            tmpBuffer[i][index++] = lengthLine;
        }
        else if (lengthLine >0xff)
        {
            tmpBuffer[i][index++] = (quint8)0x82;
            tmpBuffer[i][index++] = (lengthLine &0xff00) >> 8;
            tmpBuffer[i][index++] = (quint8)lengthLine;
        }
        else
        {
            tmpBuffer[i][index++] = lengthLine;
        }

        for (quint32 j =0; j< lengthLine; j++)
        {
            tmpBuffer[i][index] = (quint8)line->text().mid(2*j,2).toInt(NULL, 16);
            index += 1;
        }
        lengthList[i] = index;
    }

    quint32 lengthTotal =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        quint8 numberOfBytes;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
           numberOfBytes = 4;
        }
        else if (lengthList[i] > 0xff)
        {
            numberOfBytes = 5;
        }
        else
        {
            numberOfBytes = 3;
        }
        lengthTotal += lengthList[i] + numberOfBytes;
    }

    quint8 *bufferFin = new quint8[lengthTotal];
    quint32 index =0;
    for (quint32 i =0; i< numberOfRows; i++)
    {
        bufferFin[index++] = (quint8)0xff;
        bufferFin[index++] = (quint8)0x35;
        if ((lengthList[i]>0x7f) && (lengthList[i] < 0xff))
        {
            bufferFin[index++] = (quint8)0x81;
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else if (lengthList[i] > 0xff)
        {
            bufferFin[index++] = (quint8)0x82;
            bufferFin[index++] = (quint8)((lengthList[i] & 0xff00) >>8);
            bufferFin[index++] = (quint8)lengthList[i];
        }
        else
        {
            bufferFin[index++] = (quint8)lengthList[i];
        }
        for (quint32 j =0; j< lengthList[i]; j++)
        {
            bufferFin[index++] = tmpBuffer[i][j];
        }
    }

    for (quint32 i =0; i< numberOfRows; i++)
    {
        delete[] tmpBuffer[i];
    }
    delete[] tmpBuffer;
    delete[] lengthList;

    ShaInit(&ct);
    ShaUpdate(&ct, bufferFin,lengthTotal);
    ShaFinal(&ct, outSHA);

    quint32 lengthData;
    quint8  lengthByte065hAnd0aah;

    lengthByte065hAnd0aah = 2;
    lengthData = lengthTotal + sizeof(outSHA)/sizeof(quint8) + lengthByte065hAnd0aah;
    qDebug() << "lengthData " << lengthData << sizeof(outSHA)/sizeof(quint8);

    if (((lengthData >> 8) & 0xff) <= 0x0f)
    {
        stringNeededToConvertToByte += "0";
    }
    stringNeededToConvertToByte += QString::number((lengthData >> 8) & 0xff, 16);

    if ((quint8)(lengthData & 0xff) <= 0x0f)
    {
        stringNeededToConvertToByte += "0";
    }
    stringNeededToConvertToByte += QString::number((quint8)(lengthData &0xff), 16);

    stringNeededToConvertToByte += "65aa";

    for (int i =0; i< sizeof(outSHA)/sizeof(quint8) ; i++)
    {
        if (outSHA[i] <=0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(outSHA[i],16);
    }

    for (quint32 i =0; i< lengthTotal; i++)
    {
        if (bufferFin[i] <= 0x0f)
        {
            stringNeededToConvertToByte += "0";
        }
        stringNeededToConvertToByte += QString::number(bufferFin[i],16);
    }

    qDebug() << "string " << stringNeededToConvertToByte;
    byteTmp = stringNeededToConvertToByte.toLatin1();
    finalByteArray = QByteArray::fromHex(byteTmp);
    delete[] bufferFin;
    QString ipAdd  = ui->lineEdit_AddressSystem->text();
    QString portNum = ui->lineEdit_PortSystem->text();
    m_socket->getData(finalByteArray,ipAdd,portNum.toInt(),tsTabEntryPoint);
    emit startToSendTCP();

}
