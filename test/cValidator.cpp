#include "cValidator.h"

cValidator::cValidator()
{

}

QValidator::State cValidator::validate(QString &input, int &pos) const
{
    if (input.isEmpty())
           return Acceptable;

        bool b = false;
        for (quint32 i =0; i< input.length()/2;i++)
        {
            QString tmpString;
            tmpString = input.mid(2*i,2);
            quint8 val = tmpString.toInt(&b,16);
            if (!b)
            {
                return Invalid;
            }
        }
        if (input.length() %2 ==1)
        {
            QString stringTmp ;
            stringTmp = input.mid(input.length()-1,1);
            quint8 val = stringTmp.toInt(&b,16);
            if (!b)
            {
                return Invalid;
            }
        }
        return Acceptable;


}

/**
 * @brief cValidator::getStringToCheck
 * @param stringInp
 */
void cValidator::getStringToCheck(QString &stringInp)
{
    int pos=0;
    State check;
    check = validate(stringInp, pos);
    if (check == Acceptable)
    {
        emit result(true);
    }
    else
    {
        emit result(false);
    }
}
