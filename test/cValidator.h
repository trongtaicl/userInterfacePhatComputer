#ifndef CVALIDATOR_H
#define CVALIDATOR_H
#include <QValidator>

class cValidator: public QValidator
{
    Q_OBJECT
public:
    cValidator();
    virtual State validate(QString &input, int &pos) const;

public slots:
    void getStringToCheck(QString &stringInp);
signals:
    void result(bool a);
};

#endif // CVALIDATOR_H
