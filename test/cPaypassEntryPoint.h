#ifndef cPaypassEntryPoint_H
#define cPaypassEntryPoint_H

#include <QDialog>
#include "cSplitTag.h"
#include <QtCore>
#include <QtGui>
#include <QColor>
#include "cAdditionalTerCapabilities.h"
#include "cTerminalActionCode.h"
#include "cCardDataInputCapabilities.h"
#include "cSecurityCapabilities.h"
#include "cCvmCapabilities.h"
#include "cKernelConfig.h"
#include <QSignalMapper>
#include <QLabel>
#include <QToolButton>
#include <QLineEdit>
#include <QRegExp>
#include <QValidator>
#include <QRegExpValidator>
#include "cTestOtherTlv.h"
#include <QWidget>
#include <QList>

namespace Ui {
class cPaypassEntryPoint;
}

#define LINEEDIT   0
#define COMBOBOX   1
#define SPINBOX    2

typedef enum{
    DefUDOLMastripe                 = 0x00,
    KerId                           = 0x01,
    AppVerNumMastripe               = 0x02,
    CVMCapRequiredMastripe          = 0x03,
    CVMCapNoRequiredMastripe        = 0x04,
    MesHoldTime                     = 0x15,
    RCTLNo                          = 0x06,
    RCTL                            = 0x07,
    ReadCVMLimit                    = 0x08,
    TerCounCode                     = 0x09,
    MobSupInd                       = 0x0A,
    AddTerCap                       = 0x0B,
    AppVerNumEMV                    = 0x0C,
    CardDataEMV                     = 0x0D,
    CVMCapRequiredEMV               = 0x0E,
    CVMCapNoRequiredEMVp            = 0x0F,
    KerCon                          = 0x10,
    MaxLifeTimeEMV                  = 0x11,
    MaxNumTornTrans                 = 0x12,
    ReadConFloor                    = 0x13,
    SecCapEMV                       = 0x14,
    TACDefEMV                       = 0x15,
    TACDenialEMV                    = 0x16,
    TACOnlEMV                       = 0x17,
    TerType                         = 0x18,
    BalReadBefACEMV                 = 0x19,
    BalReadAftACEMV                 = 0x1A
}
IndexCheckBox;



class cPaypassEntryPoint : public QDialog
{
    Q_OBJECT

public:
    explicit cPaypassEntryPoint(QWidget *parent = 0);
    ~cPaypassEntryPoint();

     void InitializePaypass();

     void InitializeToolButton();

     void CheckTagAvai(uint8_t *Buffertam, uint16_t LengTotal);

     void UpdateData(uint8_t Index, uint8_t Sobyte, uint8_t *Buffertam, uint8_t Length);

     int ChangeStyleSheetLineEdit(int index, QString text,
                                  int length, mlsBoolWidgetSplitTag_t widgetType,
                                  QWidget *wid, QList<QCheckBox*> listCheck, QString &stringFin);

     void UpdateTextFinal(int length, uint8_t *Buffertam, QString text, uint8_t Index);

     void updateBuffer(uint16_t length, uint8_t bufferTemp[]);



private:
    Ui::cPaypassEntryPoint         *ui;

    cAdditionalTerCapabilities     *AddTer;
    cTerminalActionCode            *TACWin;
    cCardDataInputCapabilities     *winCardData;
    cSecurityCapabilities          *winSecCap;
    cCvmCapabilities               *winCVMCap;
    cKernelConfig                  *winKerConfig;
    cSplitTag                       m_TLV;
    cTestOtherTLV                   m_testTLV;

    quint8                         m_lineEditChoseTAC ;
    quint8                         m_lineEditChoseCVM ;

    QStringList                     m_comboTerType;   /**/
    QStringList                     m_comboKerID;     /**/

    QList <QCheckBox*>               m_listCheckBox;   /**/
    QList <QLineEdit*>               m_listLineEditCheck;  /**/
    QList <QCheckBox*>               m_listCheckBoxLine;
    QList <QCheckBox*>               m_listCheckBoxCombo;
    QList <QCheckBox*>               m_listCheckBoxSpin;
    QList <QComboBox*>               m_listComBoBox;
    QList <QSpinBox*>                m_listSpin;

    QList<QWidget*>                 m_listWidget;
    QList <QCheckBox*>              m_listCheckboxWid;

    QSignalMapper                   *sigMappLineEdit;

    QString                         textFinal ;
    QString                         textFixForDefUDOL;

    qint32                          m_indexWidget;

    quint8                          m_bufferTemp[200];
    quint8                          m_length;

    mlsBoolWidgetSplitTag_t         m_widgetType[27];


public slots:
    void UpdateData(qint32 indexUseForOkButton);

    void UpdateAddTerCap(const QString &text);

    void UpdateTACData(const QString &text);

    void Update1ByteLineEdit();

    void UpdateCardData(const QString &text);

    void UpdateSecCap(const QString &text);

    void UpdateCVMCap(const QString &text);

    void UpdateKerConfig(const QString &text);

    void checkbox_update_lineEdit(int index);

    void button_ok_clicked();

private slots:
    void on_toolButtonAddTerCap_clicked();

    void on_toolButtonTACDefault_clicked();

    void on_toolButtonTACDenial_clicked();

    void on_toolButtonTACOnline_clicked();

    void on_toolButtonSecCap_clicked();

    void on_toolButtonCardData_clicked();

    void on_toolButtonCVMCap_clicked();

    void on_toolButtonCVMCapNo_clicked();

    void on_toolButtonKerCon_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_textEdit_textChanged();

    void on_checkBox_TerType_clicked();

    void on_checkBox_KerID_clicked();

    void on_checkBox_MaxNumTorn_clicked();

signals:
    void ToolButtonAddTerCap_clicked(const QString &text, mlsWinAdd_t winselected);

    void ToolButtonTACDefault_clicked(const QString &text, mlsWindowDef_t winSelected);

    void ToolButtonTACDenial_clicked(const QString &text, mlsWindowDef_t winSelected);

    void ToolButtonTACOnline_clicked(const QString &text, mlsWindowDef_t winSelected);

    void ToolButtonSecCap_clicked(const QString &text);

    void ToolButtonCardData_clicked(const QString &text);

    void ToolButtonnCVMCap_clicked(const QString &text);

    void ToolButtonKerConfig_clicked(const QString &text);

    void buttonOk_clicked(qint32 index, QString textFinal);
};

#endif // cPaypassEntryPoint_H
