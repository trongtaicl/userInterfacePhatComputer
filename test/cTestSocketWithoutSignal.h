#ifndef CTESTSOCKETWITHOUTSIGNAL_H
#define CTESTSOCKETWITHOUTSIGNAL_H

#include <QObject>
#include <QTcpSocket>
#include <QDebug>
#include <QByteArray>
#include <QTime>

typedef enum{
    tsTabTerminal          = 0x00,
    tsTabProcessing        = 0x01,
    tsTabEntryPoint        = 0x02,
    tsWindowTabCA          = 0x03,
    tsWindowTabRevocatedCA = 0x04
}mls_tabSend_t;

class cTestSocketWithoutSignal: public QObject
{
    Q_OBJECT
public:
    cTestSocketWithoutSignal();

    void getData(QByteArray byteWrite, QString add, quint16 port, mls_tabSend_t ithTab);
private:
    QTcpSocket *socket;

    QByteArray m_dataToSend;

    mls_tabSend_t   m_ithTab;

    QString         m_addr;
    quint16         m_port;

    bool            m_connected;
    QTime           m_timer;

signals:

    void writeSuccess(mls_tabSend_t ithTab);

    void errorProcess(QString &text, mls_tabSend_t ithTab);

public slots:
    void startConnecting();

    void connectedServer();

};

#endif // CTESTSOCKETWITHOUTSIGNAL_H
