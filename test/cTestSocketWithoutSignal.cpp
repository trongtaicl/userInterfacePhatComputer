#include "cTestSocketWithoutSignal.h"
#include <QTime>

cTestSocketWithoutSignal::cTestSocketWithoutSignal()
{
      socket = new QTcpSocket();
      connect(socket, SIGNAL(connected()), this, SLOT(connectedServer()));
      m_connected = false;
}

void cTestSocketWithoutSignal::getData(QByteArray byteWrite, QString address, quint16 port,
                                       mls_tabSend_t ithTab)
{
   m_addr = address;
   m_port = port;
   m_ithTab = ithTab;
   m_dataToSend = byteWrite;
}

void cTestSocketWithoutSignal::startConnecting()
{
    quint64 timeNotConnected = 0;
    quint64 timeTmp =0;
    QString result ;

    m_timer.start();
    do
    {
       socket->connectToHost(m_addr, m_port);
       if (!socket->waitForConnected(30000))
       {
           timeTmp = m_timer.elapsed();
           timeNotConnected += timeTmp;
           qDebug() << "time " << timeNotConnected;
       }
    }
    while((!m_connected) && (timeNotConnected <30000));
    m_connected = false;
    if (timeNotConnected >=30000)
    {
        result = "error ";

        emit errorProcess(result,m_ithTab);
    }
}


void cTestSocketWithoutSignal::connectedServer()
{
    m_connected = true;
    socket->write(m_dataToSend);
    socket->waitForBytesWritten(1000);
    emit writeSuccess(m_ithTab);
    socket->close();
}

