#ifndef cAdditionalTerCapabilities_H
#define cAdditionalTerCapabilities_H

#include <QDialog>
#include <QString>
#include <QCheckBox>
#include <QSignalMapper>

namespace Ui {
class cAdditionalTerCapabilities;
}

typedef enum{
    waMainWindow       = 0x00,
    waPayPass          = 0x01
}mlsWinAdd_t;

class cAdditionalTerCapabilities : public QDialog
{
    Q_OBJECT
signals:

    void ButtonOk_MainWindow_Clicked(const QString &text);  /* update lineedit in cAdditionalTerCapabilities.cpp into Lineedit in Mainwindow.cpp*/
    void ButtonOk_PayPass_Clicked(const QString &text);   /* update lineedit in cAdditionalTerCapabilities.cpp into Lineedit in Mainwindow.cpp*/

public:
    explicit cAdditionalTerCapabilities(QWidget *parent = 0);
    ~cAdditionalTerCapabilities();

    void InitializeCheckbox();

    void updateCheckBoxBasedOnLineEdit(const QString &text, QList<QCheckBox*> *list);

public slots:

    void change_text_edit(const QString &text, mlsWinAdd_t win);

    void set_text_MSB();

    void set_text_LSB();

private slots:
    void on_pushButtonOK_clicked();

    void on_pushButtonCancel_clicked();

    void on_lineEdit_cursorPositionChanged(int arg1, int arg2);

    void on_lineEdit_textEdited(const QString &arg1);

    void on_lineEdit_textChanged(const QString &arg1);

private:
    Ui::cAdditionalTerCapabilities *ui;

    QCheckBox* m_checkBoxMSB[24];  /* an array of checkbox that represents for 3 MSB (byte)*/
    QCheckBox* m_checkBoxLSB[16];  /* an array of checkbox that represents for 2 LSB (byte)*/

    QList<QCheckBox*> m_listCheckBoxMSB;  /* a list of checkbox that represents for 3 MSB (byte)*/
    QList<QCheckBox*> m_listCheckBoxLSB;  /* a list of checkbox that represents for 3 MSB (byte)*/

    QSignalMapper *m_sigMappMSB; /*using QSignalMapper map each checkbox signal */
    QSignalMapper *m_sigMappLSB;

    /* headNum is represented for 3 MSB (byte)*/
    /* tailNum is represented for 2 LSB (byte)*/
    unsigned int m_headNum =0, m_tailNum =0;

    QString m_addterTextLineEdit; /*text for updating in main lineEdit */
    mlsWinAdd_t  m_winSelected;  /* signal to open this dialog from which window,
                                    mainwindow or paypass window */

    int ConvertStringtoNum(const QString &text, int pos, int num);
};

#endif // cAdditionalTerCapabilities_H
