#include "cReadXmlFile.h"

cReadXmlFile::cReadXmlFile()
{

}

void cReadXmlFile::writeXMLFile(QString &fileName, QString &terminalPath, QString &processingPath,
                                QString &entryPointPath)
{

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);

    QXmlStreamWriter xmlWrite(&file);
    xmlWrite.setAutoFormatting(true);
    xmlWrite.writeStartDocument();

    xmlWrite.writeStartElement("PATH");

    xmlWrite.writeTextElement("TERMINAL",terminalPath);
    xmlWrite.writeTextElement("PROCESSING", processingPath);
    xmlWrite.writeTextElement("ENTRYPOINT", entryPointPath);

    xmlWrite.writeEndElement();
    file.close();
}


bool cReadXmlFile::readXMLFile(QString &fileName, QString &terminal, QString &processing,
                         QString &entrypoint)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        return false;
    }

    QXmlStreamReader xml;

    xml.setDevice(&file);

    while (!xml.atEnd() && !xml.hasError())
    {
        QXmlStreamReader::TokenType token = xml.readNext();
        if (token== QXmlStreamReader::StartDocument)
        {
            continue;
        }
        if (token == QXmlStreamReader::StartElement)
        {
            if (xml.name() == "PATH")
            {
                continue;
            }
            if (xml.name() == "TERMINAL")
            {
                terminal = xml.readElementText();
            }
            if (xml.name() == "PROCESSING")
            {
                processing = xml.readElementText();
            }
            if (xml.name() == "ENTRYPOINT")
            {
                entrypoint = xml.readElementText();
            }
        }

    }

    if(xml.hasError())
    {
            return false;
    }
    return true;
}
